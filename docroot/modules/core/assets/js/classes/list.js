List = {
		
	/**
	 *  Set selected list element as active.
	 *
	 *  @return void
	 */
	setActive : function(){
		$('.list ul li').parent().find('li').addClass('inactive');
		$('.list ul li').off('click.set_active').on('click.set_active',function(){
			if($(this).hasClass('active')){
				is_active = true; 
			}else{
				is_active = false;
			}
			$(this).parent().find('li').removeClass('active').addClass('inactive');
			if(!is_active) $(this).removeClass('inactive').addClass('active');
		});
	},
	
	/**
	 * Clear list selection
	 * 
	 * @param string list selector
	 * 
	 * @return void
	 */
	clear : function(list){
		$(list+' ul li').removeClass('active');
		$(list+' ul li').addClass('inactive');
	},
};

