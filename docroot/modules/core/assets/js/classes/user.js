/**
 * User class.
 */
var User = {

	config : {
		module : 'app',
		data : [
		        'id',
		        'is_active',
		        'is_deleted',
		        'is_deletable',
		        'birth_date',
		        'birth_place',
		        'email',
		        'username',
		        'firstname',
		        'secondname',
		        'lastname',
		        'password',
		        'pesel',
		        'phone_number',
		        'place',
		        'street',
		        'zip_code',
		        'role',
		        'logins',
		        'last_login',
		],
		'requests' : {
			'user' : {
				'users' : {
					'management' : {},
				},
			},
		},
	},
		
	init : function(){
		this.requests.app.init();
	},
	
	requests : {
		app : {
			init : function(){
				User.requests.app.config.add('default','user.users.management','ajax-users-list','ajax-users-list-trigger',['firstname','lastname'],'ajax-user-form','ajax-user-form-filters');
				User.requests.user.users.management.displayList();
				User.requests.user.users.management.edit();
				User.requests.user.users.management.create();
				User.requests.user.users.management.remove();
			},
			assign_config : function(request,config){
				switch(request){
				case 'user.users.management':
					User['config']['requests']['user']['users']['management'][name] = config;
					break;
				}					
			},
			config : {
				'add' : function(name,request,list,list_trigger,list_columns,form,form_filters){
					var config = {};
					config.list = list;
					config.list_trigger = list_trigger;
					config.list_columns = list_columns;
					config.form = form;
					config.form_filters = form_filters;
					User['requests']['app']['assign_config'](request,config);
				},
			},
		},
		'user' : {
			'users' : {
				'management' : {
					'displayList' : function(){
						for(var config in User.config.requests.user.users.management){
							if(User.config.requests.user.users.management.hasOwnProperty(config)){
								config = User['config']['requests']['user']['users']['management'][config];
								if($('.'+config.list).length > 0){
									var url_load_list = url_base+'users/management/displayList';
									var edit_load = 'ajax-users-management-edit-load';
									var form_id = 'ajax-user-id';
									var url_load_form = url_base+'users/management/edit/';
									if($('.'+config.list_trigger).length > 0){
										$('.'+config.list_trigger).on('click',function(){
											jQuery.ajax({
												url:url_load_list,
												type:'post',
												data:$('.'+config.form_filters).serialize(),
												success:function(result){
													var result = JSON.parse(result);
													var data = result['data'];
													$('.'+config.list).html('<ul></ul>');
													for(var i in data){
														var columns = '';
														for(var j in config.list_columns){
															columns = columns+data[i][config.list_columns[j]]+' ';
														}
														if(columns.replace(/\s/g, '') == ''){
															columns = '&nbsp;';
														}
														$('.'+config.list+' ul').append(
															'<li class="'+edit_load+'">'
															+'<input type="hidden" class="'+form_id+'" value="'+data[i]['id']+'"/>'
															+columns+'</li>'
														);
													}
													$('.'+edit_load).on('click',function(e){
														id = $(this).find("input."+form_id).val();
														jQuery.ajax({
															url:url_load_form+id,
															type:'get',
															data:$('.'+config.form).serialize(),
															success:function(result){
																var result = JSON.parse(result);
																var data = result['data'];
																for(var i in User.config.data){
																	if('data_'+User['config']['data'][i] == $('.'+config.form+' #data_'+User['config']['data'][i]).attr('id')){
																		$('.'+config.form+' #data_'+User['config']['data'][i]).val(data[User['config']['data'][i]]);	
																	}
																}
																$('.'+config.form+' #data_password').val('');
															}
														});					
													});
												}
											});
										});
									}
									$(document).ready(function(){
										jQuery.ajax({
											url:url_load_list,
											success:function(result){
												var result = JSON.parse(result);
												var data = result['data'];
												$('.'+config.list).html('<ul></ul>');
												for(var i in data){
													var columns = '';
													for(var j in config.list_columns){
														columns = columns+data[i][config.list_columns[j]]+' ';
													}
													if(columns.replace(/\s/g, '') == ''){
														columns = '&nbsp;';
													}
													$('.'+config.list+' ul').append(
														'<li class="'+edit_load+'">'
														+'<input type="hidden" class="'+form_id+'" value="'+data[i]['id']+'"/>'
														+columns+'</li>'
													);
												}
												List.setActive();
												$('.'+edit_load).off('click.user_load').on('click.user_load',function(e){
													id = $(this).find("input."+form_id).val();
													var user_id = $('#data_id').val();
													if(id == user_id){
														$('#data_id').val('');
														for(var i in User.config.data){
															$('.'+config.form+' #data_'+User['config']['data'][i]).val('');
														}
													}else{
														jQuery.ajax({
															url:url_load_form+id,
															type:'get',
															data:$('.'+config.form).serialize(),
															success:function(result){
																var result = JSON.parse(result);
																var data = result['data'];
																$('.ajax-users-management-remove').attr('data-id',data['id']);
																for(var i in User.config.data){
																	if('data_'+User['config']['data'][i] == $('.'+config.form+' #data_'+User['config']['data'][i]).attr('id')){
																		$('.'+config.form+' #data_'+User['config']['data'][i]).val(data[User['config']['data'][i]]);	
																	}
																}
																$('.'+config.form+' #data_password').val('');
															}
														});		
													}		
												});
											}
										});								
									});	
								};
							}
						}
					},
					'edit' : function(){
						for(var config in User.config.requests.user.users.management){
							if(User.config.requests.user.users.management.hasOwnProperty(config)){
								config = User['config']['requests']['user']['users']['management'][config];
								var edit_save = 'ajax-users-management-edit-save';
								var url_edit = url_base+'users/management/edit/';
								var url_create = url_base+'users/management/create/';
								$(document).ready(function(){
									$('.'+edit_save).off('click.user_save').on('click.user_save',function(e){
										id = $('.'+config.form+' #data_id').val();
										if(id){
											jQuery.ajax({
												url:url_edit,
												type:'post',
												data:$('.'+config.form).serialize(),
												success:function(result){
													var result = JSON.parse(result);
													var data = result['data'];
													Message.display(result);
													User.requests.user.users.management.displayList();
												}
											});
										}else{
											jQuery.ajax({
												url:url_create,
												type:'post',
												data:$('.'+config.form).serialize(),
												success:function(result){
													var result = JSON.parse(result);
													var data = result['data'];
													Message.display(result);
													User.requests.user.users.management.displayList();
												}
											});
										}
									});
								});
							}
						}
					},
					'create' : function(){
						for(var config in User.config.requests.user.users.management){
							if(User.config.requests.user.users.management.hasOwnProperty(config)){
								config = User['config']['requests']['user']['users']['management'][config];
								var create_button = 'ajax-users-management-create';
								$(document).ready(function(){
									$('.'+create_button).on('click',function(){
										for(var i in User.config.data){
											$('.'+config.form+' #data_'+User['config']['data'][i]).val('');
										}
									});
								});
							}
						}
					},
					'remove' : function(){
						$('.ajax-users-management-remove').off('click.user_remove').on('click.user_remove',function(){
							id = $(this).attr('data-id');
							if(id==''){
								alert('Należy wybrać użytkownika');
							}else{
								var decision = confirm("Czy na pewno usunąć użytkownika?"); 
								if (decision) {
									jQuery.ajax({
										url:url_base+'users/management/remove/'+id,
										type:'post',
										success:function(result){
											var result = JSON.parse(result);
											Message.display(result);
											User.requests.user.users.management.displayList();
										}
									});
								}
							}
						});
					},
				},
			},
		},
	},
};
