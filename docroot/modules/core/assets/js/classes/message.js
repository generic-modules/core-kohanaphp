var Message = {
	/**
	 * Display messages.
	 * 
	 * Display errors and regular messages.
	 * 
	 * @param array messages data with messages
	 * 
	 * @return void
	 */
	display : function(data){
		$('#ajax-error-messages').html('');
		$('#ajax-messages').html('');
		if(typeof data['errors']!=='undefined'){
			var errors = data['errors'];
			for(var i in errors){
				$('#ajax-error-messages').append('<div class="error message"><a href="#">'+errors[i]+'</a></div>');
			}
				$(document).ready(function(){
					$('.error').on('click', function(e){
						e.preventDefault();
						$(this).attr('style','display:none');
					});
			});
		}
		if(typeof data['messages']!=='undefined'){
			var messages = data['messages'];
			for(var i in messages){
				$('#ajax-messages').append('<div class="warning message"><a href="#">'+messages[i]+'</a></div>');
			}
				$(document).ready(function(){
					$('.message').on('click', function(e){
						e.preventDefault();
						$(this).attr('style','display:none');
					});
			
				});
		}
	},
};
