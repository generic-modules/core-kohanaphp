/**
 * Scripts for view cms/contents/management/edit.
 */
$(document).ready(function(){
	tinymce.init({
    	selector: "textarea#data_content",
    	plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste jbimages"
		],
    	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
    	relative_urls: false,
    	image_advtab: true,
    	height: 600,
	});
	tinymce.init({
    	selector: "textarea#data_lang_pl_content",
    	plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste jbimages"
		],
    	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
    	relative_urls: false,
    	image_advtab: true
	});
	tinymce.init({
    	selector: "textarea#data_lang_en_content",
    	plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste jbimages"
		],
    	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
    	relative_urls: false,
    	image_advtab: true
	});
});
