/**
 * script for view users/management/edit.
 */
$(document).ready(function(){
	if($('#data_is_company').prop('checked')){
		$('#data_company_name').parent().parent().show();
	}else{
		$('#data_company_name').parent().parent().hide();
	}
	$('#data_is_company').on('change',function(){
		if($(this).prop('checked')){
			$('#data_company_name').parent().parent().show();
		}else{
			$('#data_company_name').parent().parent().hide();
		}
	});
});
