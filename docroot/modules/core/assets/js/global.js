/**
 * Global script.
 */
var url_base = $('#url_base').val();

//User.init();

//List.setActive();

//$(document).ready(function(){
//	$('#ajax_load').hide();
//	$(document).ajaxStart(function(){
//		$('#ajax_load').show();
//	});
//	$(document).ajaxStop(function(){
//		$('#ajax_load').hide();
//	});	
//})


$(document).ready(function(){
	$("input.filter").keypress(function(event) {
	    if (event.which == 13) {
	    	event.preventDefault();
	    	$(this).closest("form").submit();
	    }
	});
	$("select.filter").on('change',function(event) {
		event.preventDefault();
		$(this).closest("form").submit();
	});
});
