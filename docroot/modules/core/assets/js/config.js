/**
 * Javascript config.
 */
var Config = {
	
	/**
	 * @var string baseUrl
	 * 
	 * Application base url.
	 */
	baseUrl : '/'
};
