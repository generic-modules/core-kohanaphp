<?php defined('SYSPATH') or die('No direct script access.');

class Model_App_Email_Type extends Model_Base
{

	/**
	 * @var $_table
	 *
	 * Database table name for this model
	 */
	protected $_table_name = 'emails_types';


	protected $_has_many = array(
		'emails' => array('model' => 'Email', 'foregin_key' => 'type_id'),
	);
}
