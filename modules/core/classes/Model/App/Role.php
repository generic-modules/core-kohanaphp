<?php defined('SYSPATH') or die('No direct script access.');

class Model_App_Role extends Model_Base
{
	/**
	 * @var $_table
	 *
	 * Database table name for this model
	 */
	protected $_table = 'roles';

	/**
	 * @var $_has_many
	 *
	 * Has many relationships
	 */
	protected $_has_many = array(
		'users'		=> array('model' => 'User', 'through' => 'roles_users'),
		'functions'	=> array('model' => 'Function', 'through' => 'roles_functions'),
	);

	/**
	 * Change functions assignement to particular role
	 *
	 *
	 */
	public function changeFunctions()
	{

	}

	/**
	 * Create new role.
	 *
	 * @param array $data data to save
	 *
	 * @return void
	 */
	public function createOne($data)
	{
		$this->setData($data);
		try{
			$this->save();
			return false;
		} catch(ORM_Validation_Exception $e) {
			Error::set($e);
			return false;
		}
	}

	/**
	 * Get roles list based on id numbers.
	 *
	 * @param array[int] $ids id numbers
	 *
	 * @return array[Model_Role] roles collection or null if no results
	 */
	public function getListByIds($ids)
	{
		if($ids) return $this->where('id','in',$ids)->find_all();
		return false;
	}

	/**
	 * Get only visible roles.
	 *
	 * @return array or Model_Role
	 */
	public function getVisible($options = array())
	{
		$roles = $this->where('is_visible','=',true)->find_all();
		if(count($roles) > 1){
			if(isset($options['array'])){
				$data = array();
				foreach($roles as $row){
					if(isset($options['columns'])){
						if(isset($options['empty_option'])){
							$data[0] = __('user.global.select.choose');
						}
						if(is_array($options['columns'])){
							$values = array();
							foreach($options['columns'] as $column){
								$values[] = $column;
							}
						}else{
							$values = $row->_object[$options['columns']];
						}
					}else{
						$values = $row->_object;
					}
					$data[$row->_object['id']] = $values;
				}
				return $data;
			}else{
				return $roles;
			}
		}else{
			return $roles[0];
		}
	}

	/**
	 * Remove particular role.
	 *
	 * @param int $id role id
	 *
	 * @return void
	 */
	public function removeOne($id)
	{
		$this->where('id','=',$id)->find();
		$this->delete();
	}

	/**
	 * @inherit
	 */
	public function setData($data)
	{
		if(isset($data['name'])) $this->name = $data['name'];
	}

	/**
	 * @inherit
	 */
	public function setFormsRules()
	{

	}

	/**
	 * Get relations with other models.
	 *
	 * @return array relations array
	 */
	public function setRelations()
	{
		$this->setRelation('has_many','emails','Email','user_id');
		$this->setRelation('has_many','roles','Role','user_id','role_id','roles_users');
	}
}
