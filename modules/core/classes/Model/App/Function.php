<?php defined('SYSPATH') or die('No direct script access.');

class Model_App_Function extends Model_Base
{
	/**
	 * @var $_table
	 *
	 * Database table name for this model
	 */	
	protected $_table = 'functions';

	/**
	 * @var $_has_many
	 * 
	 * Has many relationships
	 */
	protected $_has_many = array(
		'roles' => array('model' => 'Role', 'through' => 'Role_Function'),
	);
	
	/**
	 * Get functions list.
	 * 
	 * @return functions collection
	 */
	public function getList()
	{
		return $this->find_all();
	}

}
