<?php defined('SYSPATH') or die('No direct script access.');

class Model_App_Address extends Model_Base
{
	/**
	 * Set relations with other models.
	 *
	 * @return array relations array
	 */
	public function setRelations()
	{
		$this->setRelation('belongs_to','user','User','user_id');
		parent::setRelations();
	}

	/**
	 * Get email by type
	 *
	 * @param string $type
	 *
	 * @return Model_Email
	 */
	public function getByType($type)
	{
		$type = ORM::factory('Address_Type')->where('system_name','=',$type)->find();
		$address = ORM::factory('Address')->where('type_id','=',$type->id)->find();
		return $address;
	}

	public function getDefault()
	{
		return $this->where('is_default','=',true);
	}
}
