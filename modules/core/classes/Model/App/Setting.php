<?php defined('SYSPATH') or die('No direct script access.');

class Model_App_Setting extends Model_Base
{
	/**
	 * @var $_table
	 *
	 * Database table name for this model
	 */
	protected $_table = 'settings';

	/**
	 * @inherit
	 */
	public function init($post, $options = null)
	{
		if(isset($post['edit_settings_submit'])){
			$this->form = 'edit_settings';
			unset($post['edit_settings_submit']);
		}
		parent::init($post);
	}

	/**
	 * Save all settings.
	 *
	 * @param array $post settings to save
	 *
	 * @return true if success, otherwise false
	 */
	public function saveAll($post)
	{
		if(isset($post['edit_settings_submit'])){
			unset($post['edit_settings_submit']);
		}
		if(!$post['data']) {
			$data = $post;
			unset($post);
			$post['data'] = $data;
		}
		$data = $post['data'];
		$this->init($post);
		if(!$this->data) return true;
		$this->begin();
		foreach($data as $system_name=>$value){
			$setting = ORM::factory('Setting')->where('system_name','=',$system_name)->find();
			$setting->value = $value;
			try{
				$setting->save($this->getFormRules('edit_settings'));
			}catch(ORM_Validation_Exception $e){
				$this->rollback();
				Error::set($e);
				return false;
			}
		}
		$this->commit();
		return true;
	}

	/**
	 * @inherit
	 */
	public function setFormsRules()
	{
		$data = $this->data;		
		parent::setFormsRules();
		switch($this->form){
			case 'edit_settings':
				$this->formRules['edit_settings'] = Validation::factory($this->data);
				if(isset($data['company_name'])) $this->setFormRule('edit_settings','company_name','max_length',array(':value',40));
				if(isset($data['company_name'])) $this->setFormRule('edit_settings','company_name','not_empty');
				if(isset($data['company_name'])) $this->setFormRule('edit_settings','company_name','regex',array(':value',Kohana::$config->load('validation.alpha_numeric_pl')));
				if(isset($data['company_address'])) $this->setFormRule('edit_settings','company_address','max_length',array(':value',100));
				if(isset($data['company_address'])) $this->setFormRule('edit_settings','company_address','not_empty');
				if(isset($data['company_address'])) $this->setFormRule('edit_settings','company_address','regex',array(':value',Kohana::$config->load('validation.alpha_numeric_pl')));
				if(isset($data['company_place'])) $this->setFormRule('edit_settings','company_place','max_length',array(':value',40));
				if(isset($data['company_place'])) $this->setFormRule('edit_settings','company_place','not_empty');
				if(isset($data['company_place'])) $this->setFormRule('edit_settings','company_place','regex',array(':value',Kohana::$config->load('validation.alpha_pl')));
				if(isset($data['company_nip'])) $this->setFormRule('edit_settings','company_nip','max_length',array(':value',10));
				if(isset($data['company_nip'])) $this->setFormRule('edit_settings','company_nip','not_empty');
				if(isset($data['company_nip'])) $this->setFormRule('edit_settings','company_nip','regex',array(':value',Kohana::$config->load('validation.nip')));
				if(isset($data['company_regon'])) $this->setFormRule('edit_settings','company_regon','max_length',array(':value',9));
				if(isset($data['company_regon'])) $this->setFormRule('edit_settings','company_regon','not_empty');
				if(isset($data['company_regon'])) $this->setFormRule('edit_settings','company_regon','regex',array(':value',Kohana::$config->load('validation.regon')));
				if(isset($data['company_bank_account_number'])) $this->setFormRule('edit_settings','company_bank_account_number','exact_length',array(':value',26));
				if(isset($data['company_bank_account_number'])) $this->setFormRule('edit_settings','company_bank_account_number','not_empty');
				if(isset($data['company_bank_account_number'])) $this->setFormRule('edit_settings','company_bank_account_number','regex',array(':value',Kohana::$config->load('validation.regon')));
				if(isset($data['company_bank_account_bank_name'])) $this->setFormRule('edit_settings','company_bank_account_bank_name','max_length',array(':value',100));
				if(isset($data['company_bank_account_bank_name'])) $this->setFormRule('edit_settings','company_bank_account_bank_name','not_empty');
				if(isset($data['company_bank_account_bank_name'])) $this->setFormRule('edit_settings','company_bank_account_bank_name','regex',array(':value',Kohana::$config->load('validation.alpha_numeric_pl')));
				if(isset($data['company_phone_number'])) $this->setFormRule('edit_settings','company_phone_number','not_empty');
				if(isset($data['company_phone_number'])) $this->setFormRule('edit_settings','company_phone_number','phone');
				if(isset($data['company_fax'])) $this->setFormRule('edit_settings','company_fax','not_empty');
				if(isset($data['company_fax'])) $this->setFormRule('edit_settings','company_fax','phone');
				if(isset($data['company_email'])) $this->setFormRule('edit_settings','company_email','email');
				if(isset($data['company_email'])) $this->setFormRule('edit_settings','company_email','not_empty');
				if(isset($data['company_www'])) $this->setFormRule('edit_settings','company_www','not_empty');
				if(isset($data['company_www'])) $this->setFormRule('edit_settings','company_www','url');
				break;
		}
	}

	/**
	 * Set parameter value.
	 *
	 * @param string $key parameter to set
	 * @param string $value value of parameter to set
	 *
	 * @return void
	 */
	public function setOne($key, $value)
	{
		$this->where('key','=',$key)->find();
		$this->value = $value;
		$this->save();
	}

}
