<?php defined('SYSPATH') or die('No direct script access.');

class Model_App_Email extends Model_Base
{
	/**
	 * @var $_table
	 *
	 * Database table name for this model
	 */
	protected $_table_name = 'emails';

	/**
	 * @var $_belongs_to
	 *
	 * Belongs to relationships
	 */
	protected $_belongs_to = array(
		'user' => array('model' => 'User', 'foreign_key' => 'user_id'),
		'type' => array('model' => 'Email_Type', 'foreign_key' => 'type_id'),
	);

	/**
	 * Get email by type
	 *
	 * @param string $type
	 *
	 * @return Model_Email
	 */
	public function getByType($type)
	{
		$type = ORM::factory('Email_Type')->where('system_name','=',$type)->find();
		$email = ORM::factory('Email')->where('type_id','=',$type->id)->find();
		return $email;
	}

	public function getDefault()
	{
		return $this->where('is_default','=',true);
	}

	/**
	 * Send email
	 *
	 * @param array $data email data
	 * @param array $files attachements files
	 *
	 * @return true/false
	 */
	public function send($data,$files = array())
	{
		if(isset($data['data']['contact'])){
			$data = $data['data']['contact'];
		}
		if(isset($data['to'])){
			$to = $data['to'];
		}else{
			$to = Kohana::$config->load('emails.default.to');
		}
		if(isset($data['subject'])){
			$subject = $data['subject'];
		}
		if(isset($data['message'])){
			$message = $data['message'];
		}
		if($files){
			$_files = array();
			foreach($files as $file){
				if($file['size'] > 0){
					$_file['type'] = $file['type'];
					$_file['name'] = $file['name'];
					$_file['file'] = chunk_split(base64_encode(file_get_contents($file['tmp_name'])));
					$_files[] = $_file;
				}
			}
			$files = $_files;
		}
		$random_hash = md5(date('r', time()));
		$type = Kohana::$config->load('emails.default.type');
		$charset = Kohana::$config->load('emails.default.charset');
		$from = Kohana::$config->load('emails.default.sender_email');
		$reply_to = Kohana::$config->load('emails.default.sender_email');
		$headers = array();
		$headers[] = "Content-type: $type; boundary=\"PHP-mixed-$random_hash\"";
		$headers[] = "From: $from";
		$headers[] = "Reply-To: $reply_to";
		$template = 'emails/templates/default';
		$view = View::factory($template);
		$view->message = $message;
		$view->random_hash = $random_hash;
		if(count($files > 0)){
			$view->files = $files;
		}
		return mail($to,$subject,$view,implode("\r\n", $headers));
	}
}
