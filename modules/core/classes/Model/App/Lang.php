<?php defined('SYSPATH') or die('No direct script access.');

class Model_App_Lang extends Model_Base
{
	/**
	 * @var array $data
	 *
	 * Translated data
	 */
	protected $data = array();
	
	/**
	 * Class constructor
	 *
	 * @param string $lang language symbol
	 * @param string $table_name table name
	 * @param int $id object id
	 *
	 * @return void
	 */
	public function __construct($table_name = null, $lang = null, $id = null)
	{
		if($table_name){
			$this->_table_name = $table_name.'_lang';
			parent::__construct();
			if($lang and $id){
				$this->object_id = $id;
				$this->where('object_id','=',$this->object_id)->where('lang','=',$lang)->find();
			}
			return $this;
		}
	}
	
	/**
	 * Save translated data
	 *
	 * @param Validation $validation validation rules
	 *
	 * @return true/false
	 */
	public function save(Validation $validation = NULL)
	{
		$object_id = $this->object_id;
		foreach($this->data as $lang => $values){
			$this->clear();
			$this->where('object_id','=',$object_id)->where('lang','=',$lang)->find();
			foreach($values as $property => $value){
				$this->$property = $value;
			}
			if(!parent::save($validation)){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Remove translated objects.
	 *
	 * @param string $table
	 * @param int $id
	 *
	 * @return bool
	 */
	public function removeLang($table,$id)
	{
		$query = DB::query(Database::SELECT, "delete from :table where object_id=:id");
		$query->param(':table',DB::expr($table.'_lang'));
		$query->param(':id',DB::expr($id));
		$query->execute();
		return true;
	}
	
	/**
	 * Save lang object
	 *
	 * @param int object_id related object id
	 * @param string lang language string
	 * @param array data translated data
	 *
	 * @return boolean
	 */
	public function saveLang($table,$id,$lang,$data)
	{
		$query_check_lang = DB::query(Database::SELECT, "select count(*) as number from :table where object_id=:id and lang=':lang'");
		$query_check_lang->param(':table',DB::expr($table.'_lang'));
		$query_check_lang->param(':lang',DB::expr($lang));
		$query_check_lang->param(':id',DB::expr($id));
		$result = $query_check_lang->execute();
		$number = (int)$result[0]['number'];
		if($number > 0){
			$query = DB::query(Database::SELECT, "update :table :data where object_id=:id and lang=':lang'");
			if(count($data) > 0){
				$update = "set";
				foreach($data as $name => $value){
					$update .= " ".$name."='".$value."',";
				}
				$update = rtrim($update,",");
			}else{
				return false;
			}
			$query->param(':table',DB::expr($table.'_lang'));
			$query->param(':lang',DB::expr($lang));
			$query->param(':id',DB::expr($id));
			$query->param(':data',DB::expr($update));
			$query->execute();
			return true;
		}else{
			$query = DB::query(Database::SELECT, "insert into :table (object_id,lang,:columns) values(:id,':lang',:data)");
			if(count($data) > 0){
				$columns = "";
				$values = "";
				foreach($data as $name => $value){
					$columns.= $name.",";
					$values .= "'".$value."',";
				}
				$columns= rtrim($columns,",");
				$values= rtrim($values,",");
			}else{
				return false;
			}
			$query->param(':table',DB::expr($table.'_lang'));
			$query->param(':lang',DB::expr($lang));
			$query->param(':id',DB::expr($id));
			$query->param(':columns',DB::expr($columns));
			$query->param(':data',DB::expr($values));
			$query->execute();
			return true;
		}
	}
	
	/**
	 * Set translated data
	 *
	 * @param array $data
	 *
	 * @return void
	 */
	public function setData($data)
	{
		$this->data = $data;
	}
}