<?php defined('SYSPATH') or die('No direct script access.');

class Model_App_Address_Type extends Model_Base
{
	/**
	 * @var $_table
	 *
	 * Database table name for this model
	 */	
	protected $_table = 'addresses_types';

}
