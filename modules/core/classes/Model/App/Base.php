<?php defined('SYSPATH') or die('No direct script access.');

abstract class Model_App_Base extends Model_Auth_User
{
	/**
	 * @var $data
	 *
	 * Data from submited form.
	 */
	protected $data = array();

	/**
	 * @var $form
	 *
	 * Submited form.
	 */
	protected $form = null;

	/**
	 * @var array $formRules
	 *
	 * Validation rules for particular form
	 */
	protected $formRules = array();

	/**
	 * @var bool $ignoreRules
	 *
	 * Flag if rules should be used during save.
	 */
	protected $ignoreRules = false;
	
	/**
	 * @var bool $is_translated
	 * 
	 * Flag if content is translated
	 */
	protected $is_translated = false;
	
	/**
	 * @var Model_Lang
	 */
	protected $lang = null;
	
	/**
	 * @var $parameters
	 *
	 * Parameters for this model.
	 */
	protected $parameters = array();

	/**
	 * @var array $post
	 *
	 * POST
	 */
	protected $post = array();

	/**
	 * @var mixed $options
	 *
	 * Various options passed to model
	 */
	protected $options = array();

	/**
	 * @var array $related
	 *
	 * Related models data and parameters.
	 */
	protected $related = array();

	/**
	 * @var array $relations
	 *
	 * Model realtions with another models
	 */
	protected $relations = array();

	/**
	 * @var array $rules
	 *
	 * Validation rules for the object
	 */
	protected $rules = array();

	/**
	 * @var array $translated_rules
	 *
	 * Validation rules for translated properties of the object
	 */
	protected $translated_rules = array();

	
	protected $session_data_index = null;
	
	/**
	 * @var int $transaction_level
	 *
	 * Transactions counter needed to decide
	 * when transaction should be commited.
	 */
	protected static $transaction_level = 0;

	/**
	 * @inherit
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
		$this->setRelations();
	}

	/**
	 * Begin transaction.
	 *
	 * @return void
	 */
	public function begin()
	{
		if(self::$transaction_level===0) Database::instance()->query('query','SET AUTOCOMMIT=0');
		self::$transaction_level++;
	}

	/**
	 * Returns 'on' for true in checkbox.
	 *
	 * @param string $column column name
	 *
	 * @return true/false
	 */
	public function bool($column){
		var_dump($this->$column); die;
		if($this->$column == 1) return 'on';
	}

	/**
	 * Check if removing is possible
	 *
	 * @return true if possible or false
	 */
	public function isRemovingPossible()
	{
		return true;
	}

	/**
	 * Commit transaction.
	 *
	 * @return void
	 */
	public function commit()
	{
		self::$transaction_level--;
		if(self::$transaction_level===0){
			Database::instance()->query('query','COMMIT');
			Database::instance()->query('query','SET AUTOCOMMIT=1');
		}
	}

	/**
	 * Count pages number.
	 * 
	 * @return int
	 */
	public function countPages($items_per_page = null)
	{
		return ceil($this->where('is_deleted','=',false)->count_all()/$items_per_page);
	}
	
	/**
	 * Format property as date string.
	 * 
	 * @param string $value value to format
	 * @param string $format default Y-m-d
	 * 
	 * @return string
	 */
	public function date($value, $format = 'Y-m-d')
	{
		$date = new Datetime($this->$value);
		$date = $date->format($format);
		return $date;
	}
	
	/**
	 * @inherit
	 */
	public function delete()
	{
		if($this->is_translated){
			$lang = new Model_Lang();
			$lang->removeLang($this->_table_name,$this->id);
		}
		
		
		
		parent::delete();
	}
	
	/**
	 * Get full address.
	 *
	 * @return string
	 */
	public function getAddress()
	{
		$address = '';
		if($this->street_name or $this->street_number or $this->flat_number or $this->zip_code or $this->place){
			$address .= $this->street_name.' '.$this->street_number;
			if($this->flat_number) $address .= '/'.$this->flat_number;
			$address .= ', '.$this->zip_code.' '.$this->place;
		}
		return $address;
	}
	
	/**
	 * Active elements list.
	 *
	 * @param array $options
	 *
	 * @return array[Model_Object]
	 */
	public function getActive($options = array())
	{
		return $this->where('is_active','=',1)->getList($options);
	}

	/**
	 * Get object by id.
	 *
	 * @param int $id
	 *
	 * return Model_Object
	 */
	public function getById($id)
	{
		return $this->where('id','=',$id)->find();
	}

	/**
	 * Get object by system name.
	 *
	 * @param string $system_name system name
	 *
	 * @return Model_Object
	 */
	public function getBySystemName($system_name)
	{
		return $this->where('system_name','=',$system_name)->find();
	}

	/**
	 * Get data for particular form format.
	 *
	 * @param string $form form name
	 *
	 * @return array data in selected form format
	 */
	public function getData($form)
	{
		return array();
	}

	/**
	 * Get form rules.
	 *
	 * @param string $form form name
	 *
	 * @return $formRules field
	 */
	protected function getFormRules($form)
	{
		if(isset($this->formRules[$form])){
			return $this->formRules[$form];
		}
		return null;
	}

	/**
	 * Get single record
	 *
	 * @param mixed $id id or system name
	 * @param array $options
	 * - where: where clause
	 *
	 * @return Model_Object
	 */
	public function getOne($id=null,$options = array())
	{
		if($id){
			if(is_array($id)){
				$this->where($id[0],'=',$id[1]);
			} else if(is_numeric($id)){
				$this->where('id','=',$id);
			}else{
				$this->where('system_name','=',$id);
			}
		}else{
			return null;
		}
		if($this->find()){
			if(isset($options['array'])){
				return $this->toArrayRow();
			}
			return $this;
		}else{
			return false;
		}
	}

	/**
	 * Get last item
	 *
	 * @return Model_Object
	 */
	public function getLast()
	{
		return $this->order_by('id','desc')->limit(1)->find();
	}

	/**
	 * Get list.
	 *
	 * @param bool $array get results as array
	 *
	 * @return array options:
	 * - array: get list as array
	 * - pagination: pagination options
	 * - columns: select columns - works only with array option
	 * - empty_option: empty option value
	 * - filters: filters for select
	 */
	public function getList($options = array())
	{
		if(isset($options['where'])){
			if(is_array($options['where'])){
				foreach($options['where'] as $where){
					if(is_array($where)){
						switch(count($where)){
							case 2: $this->where($where[0],'=',$where[1]); break;
							case 3: $this->where($where[0],$where[1],$where[2]); break;
							default: die('Option where should have 2 or 3 elements');
						}
					}else{
						die('Option where needs to be array');
					}
				}
			}else{
				die('Option where needs to be array');
			}
		}
		if(isset($options['limit'])){
			$this->limit($options['limit']);
		}
		if(isset($options['order_by'])){
			$this->order_by($options['order_by']['column'],$options['order_by']['direction']);
		}
		if(isset($options['filters'])){
			foreach($options['filters'] as $name => $filter){
				if($related = $this->hasRelation($name)){
					switch($related['model']){
						case 'Role':
							if($filter > 0){
								$role = ORM::factory('Role')->where('id','=',$filter)->find();
								$id = array();
								foreach($role->users->find_all() as $user){
									$id[] = $user->id;
								}
								$this->where('id','in',$id);
							}
							break;
						default:
							if($filter) $this->where($name.'_id','=',$filter);
					}
				}else{
					if($filter) $this->where($name,'like','%'.$filter.'%');
				}
			}
		}
		if(isset($options['array'])){
			$data = array();
			foreach($this->find_all() as $row){
				if(isset($options['columns'])){
					$columns = $options['columns'];
					if(isset($options['empty_option'])){
						$data[0] = __('user.global.select.choose');
					}
					if(is_array($columns)){
						$values = array();
						foreach($columns as $column){
							$values[] = $column;
						}
					}else{
						$values = $row->_object[$options['columns']];
					}
				}else{
					$values = $row->toArrayRow();
				}
				$data[$row->_object['id']] = $values;
			}
			return $data;
		}
		if(isset($options['select'])){
			$data = array();
			foreach($this->find_all() as $row){
				if(isset($options['columns'])){
					$columns = $options['columns'];
					if(isset($options['empty_option'])){
						$data[0] = __('user.global.select.choose');
					}
					if(is_array($columns)){
						$values = array();
						foreach($columns as $column){
							if(is_array($column)){
								$_columns = $column;
								foreach($_columns as $_column){
									if($row->_object[$_column]){
										$values[] = $_column;
										break;
									}
								}
							}else{
								$values[] = $row->$column;
							}

						}
					}else{
						$values = $row->_object[$options['columns']];
					}
				}else{
					$values = $row->toArrayRow();
				}
				$data[$row->_object['id']] = implode(' ',$values);
			}
			return $data;
		}else{
			$results = $this->find_all();
			if(count($results) > 0){
				return $results;
			}
			return null;
		}
	}

	/**
	 * Get data for select
	 *
	 * @param array $columns columns list
	 * @param array $options additional options array
	 *
	 * @return array
	 */
	public function getSelectList($columns = array('name'),$separator = ' ',$options = array())
	{
		$list = $this->getList($options);
		$_list = array();
		foreach($list as $item){
			$_item = array();
			foreach($columns as $column){
				$_item[] = $item->$column;
			}
			$id = $item->id;
			$value = implode($separator,$_item);
			$_list[$id] = $value;
		}
		$list = $_list;
		return $list;
	}


	/**
	 * Init and return relations with other models.
	 *
	 * Every module is supposed to init it's own relations
	 * with another models on each level.
	 *
	 * @return array relations array
	 */
	protected static function getRelations()
	{
		return array();
	}

	/**
	 * Get object rules.
	 *
	 * @return $this->rules field
	 */
	protected function getRules()
	{
		return $this->rules;
	}

	/**
	 * Check if model has relation and return related object.
	 *
	 * @param string $relation relation name
	 *
	 * @return Model_Object
	 */
	public function hasRelation($relation)
	{
		$related = null;
		if(isset($relation)){
			if(isset($this->_has_many[$relation])) $related = $this->_has_many[$relation];
			else if(isset($this->_has_one[$relation])) $related = $this->_has_one[$relation];
			else if(isset($this->_belongs_to[$relation])) $related = $this->_belongs_to[$relation];
		}
		return $related;
	}

	/**
	 * Init class based on received data.
	 *
	 * This method should be used at the very beginning of every method
	 * that uses any data sent from a form.
	 *
	 * It should always execute parent at the very end of init method.
	 *
	 * @param array $post POST
	 *
	 * @return void
	 */
	protected function init($post)
	{
		if(!isset($post)) return;
		if(!$this->loaded()){
			if(isset($post['data']['id'])) {
				$this->where('id','=',$post['data']['id'])->find();
			}else if(isset($post['id'])){
				$this->where('id','=',$post['id'])->find();
			}
		}
		$this->post = $post;
		if(isset($post['data'])) $data = $post['data']; else $data = $post;
		if(isset($data)) $this->setData($data);
		if(isset($post['parameters'])) $this->setParameters($post['parameters']);
		if(isset($post['options'])) $this->options = $post['options'];
		$this->setRules();
		$this->setFormsRules();
	}

	/**
	 * Get translated version of the property
	 *
	 * @param string $lang language symbol
	 *
	 * @return StdClass
	 */
	public function lang($lang)
	{
		$query = DB::query(Database::SELECT, "select * from :table where object_id = :id and lang = ':lang'")
			->param(':table',DB::expr($this->_table_name.'_lang'))
			->param(':lang',DB::expr($lang))
			->param(':id',DB::expr($this->id))
		;
		$data = $query->execute();
		
		$data = $this->getPlaceholders($data[0]);
		
		
// 		echo '<pre>'; var_dump($data); die;
		
		return (object)$data;
	}

	/**
	 * Return number as PLN format.
	 *
	 * It works only on number format values.
	 *
	 * @param string $column column name
	 * @param bool $display_currency flag if add currency name (default true)
	 *
	 * @return string column name as PLN
	 */
	public function pln($column,$display_currency = true)
	{
		return Core::pln($this->$column,$display_currency);
	}

	/**
	 * Remove object
	 *
	 * @param int $id object id
	 *
	 * @return true if success or false
	 */
	public function removeOne($id)
	{
		if(is_array($id)){
			if(isset($id['data_id'])) $id = $id['data_id'];
		}
		$this->where('id','=',$id)->find();
		if($this->isRemovingPossible()){
			switch($this->removeType()){
				case 'remove':
					$this->removeRelated();
					$this->delete();
					break;
				case 'flag':
					$this->is_deleted = true;
					$this->remove('roles',Kohana::$config->load('models.role.login_id'));
					$this->save();
					break;
			}
			return true;
		}else{
			Message::set('remove_failed_relations');
			return false;
		}
	}

	/**
	 * Remove related objects
	 *
	 * @return true if success or false
	 */
	public function removeRelated()
	{
		return true;
	}

	/**
	 * Get the way of removing model object
	 *
	 * @return string
	 */
	protected function removeType()
	{
		return 'remove';
	}

	/**
	 * Refreash related data after unsuccessull save.
	 * 
	 * @return void
	 */
	protected function refreashRelatedData()
	{
		foreach($this->related as $relation => $data){
			if(isset($data['data'])){
				$data = $data['data'];
				foreach($data as $name => $value){
					$this->$relation->$name = $value;
				}
			}
		}
	}
	
	/**
	 * Rollback transaction.
	 *
	 * @return void
	 */
	public function rollback()
	{
		Database::instance()->query('query','ROLLBACK');
		Database::instance()->query('query','SET AUTOCOMMIT=1');
	}

	/**
	 * Validation rules.
	 *
	 * @return array validation rules
	 */
	public function rules()
	{
		if($this->ignoreRules) return array();
		return $this->getRules();
	}

	/**
	 * @inherit
	 */
	public function save(Validation $validation = null)
	{
		if(parent::save($validation)){
			if(isset($this->data['lang'])){
				if(Kohana::$config->load('global.languages.multilanguage')){
					
				}else{
					$lang = new Model_Lang();
					$lang->saveLang($this->_table_name,$this->id,Lang::get(),$this->data['lang']);
				}
			}
			if(Kohana::$config->load('global.languages.multilanguage') and isset($this->data['lang'])){
				if($this->saveTranslated($validation)){
					return true;
				}
			}else{
				return true;
			}
		}
		return false;
	}

	/**
	 * Save single object
	 *
	 * @param array $post
	 * @param array $options
	 *
	 * @return true if success, otherwise false
	 */
	public function saveOne($post,$options = array())
	{
		$this->init ( $post );
		try {
			$this->begin();
			if ($this->save ()) {
				if($this->saveRelated()){
					$this->commit();
					return true;
				}
			}
			return false;
		} catch ( ORM_Validation_Exception $e ) {
			$this->refreashRelatedData();
			$this->rollback();
			Error::set ( $e );
			return false;
		}
	}

	/**
	 * Save related data.
	 *
	 * @return true/false
	 */
	public function saveRelated()
	{
		return true;
	}

	/**
	 * Save translated data.
	 *
	 * @return true/false
	 */
	public function saveTranslated(Validation $validation = null)
	{
		$lang = new Model_Lang($this->_table_name);
		$lang->setData($this->data['lang']);
		$lang->object_id = $this->id;
		$lang->rules = $this->translated_rules;
		return $lang->save($validation);
	}

	/**
	 * Handle session data.
	 * 
	 * Set provided data and get session data associated with model.
	 * If no data provided then only get model data.
	 * 
	 * @param array $data session data
	 * @param bool $clear clear session data
	 * 
	 * @return array
	 */
	public function session($data = array(),$clear = false)
	{
		if(!$this->session_data_index) die('Property session_data_index is not set');
		$_data = Session::instance()->get($this->session_data_index);
		if($_data){
			$data = array_merge($_data,$data);
		}
		if($clear) {
			Session::instance()->set($this->session_data_index,null);
		}else{
			Session::instance()->set($this->session_data_index,$data);
		}
		return $data;
	}
	
	/**
	 * Set model data.
	 *
	 * In inheriting models, first should be done
	 * customisation for this models and inherited method
	 * should be executed at the end
	 *
	 * @param array $data data to set
	 *
	 * @return void
	 */
	protected function setData($data)
	{
		$this->data = $data;
		$this->setValues($data);
	}

	/**
	 * Set form rule.
	 *
	 * Set single rule for particular form.
	 *
	 * @param string $form form name
	 * @param string $field field name
	 * @param string $rule rule name
	 * @param array rule definition
	 *
	 * @return void
	 */
	protected function setFormRule($form, $field, $rule, $definition = null)
	{
		if($definition){
			$this->formRules[$form]->rule($field, $rule, $definition);
		}else{
			$this->formRules[$form]->rule($field, $rule);
		}
	}

	/**
	 * Set rules for all forms interacting with this object.
	 *
	 * In this class should be defined validation rules for this object
	 *
	 * Inheriting classes, except the first one, should always execute
	 * this class for parent class, to make sure all the rules are defined.
	 *
	 * @param string $data form data to validate
	 *
	 * @return void
	 */
	protected function setFormsRules(){}

	/**
	 * Set model parameters.
	 *
	 * It sets parameters as model attribute.
	 * If one of parameters is submited form name
	 * (with _submit at the end) it sets this name
	 * without _submit part as form name (attribute form).
	 *
	 * @param array $parameters parameters array
	 *
	 * @return void
	 */
	protected function setParameters($parameters)
	{
		if($parameters){
			if(is_array($parameters)){
				foreach($parameters as $index => $parameter){
					$parts = explode('_',$index);
					if(in_array('submit',$parts)){
						$form = '';
						foreach($parts as $name){
							if($name != 'submit'){
								$form .= $name.'_';
							}
						}
						$this->form = rtrim($form,'_');
					}else{
						$this->parameters[$index] = $parameter;
					}
				}
			}
		}
	}

	/**
	 * Set related models data and parameters.
	 *
	 * @param string $model model name
	 * @param array $post post data from the form
	 * @param bool $multiple flag if multiple related elements, defaults to false
	 *
	 * @return array adjusted post data
	 */
	protected function setRelated($model, $post, $multiple = false)
	{
		if(!isset($post['parameters'])) $post['parameters'] = array();
		if(isset($post['data'])){
			$data = $post['data'];
			$parameters = $post['parameters'];
			$return = 'post';
		}else{
			$data = $post;
			$parameters = array();
			$return = 'data';
		}
		
		if(isset($data)){
			if(is_array($model)){
				$this->related[$model[0]]['data'] = $data[$model[1]];
				$this->related[$model[0]]['parameters'] = $parameters;
				unset($data[$model[1]]);
			}else{
				foreach($data as $index => $value){
					if($index == $model){
						if($multiple){
							foreach($value as $_index => $_value){
								$this->related[$model][$_index]['data'] = $_value;
								$this->related[$model][$_index]['parameters'] = $parameters;
							}
						}else{
							$this->related[$model]['data'] = $value;
							$this->related[$model]['parameters'] = $parameters;
						}
						break;
					}
				}
				unset($data[$index]);
			}
		}
		if($return == 'data') return $data;
		$post['data'] = $data;
		return $post;
	}

	/**
	 * Set single relation definition.
	 *
	 * @param string $type relation type
	 * @param string $name relation name
	 * @param string $model model name
	 * @param string $foreign_key foreign key
	 * @param string $far_key far_key
	 * @param string $through through
	 *
	 * @return array
	 */
	public function setRelation($type, $name, $model, $foreign_key, $far_key = null, $through = null)
	{
		$relation[$name]['model'] = $model;
		$relation[$name]['foreign_key'] = $foreign_key;
		if($through) $relation[$name]['through'] = $through;
		if($far_key) $relation[$name]['far_key'] = $far_key;
		$this->relations[$type][] = $relation;
	}

	/**
	 * Set model relations
	 *
	 * @return void
	 */
	public function setRelations()
	{
		foreach($this->relations as $type => $relations){
			$type = '_'.$type;
			foreach($relations as $relation){
				$this->$type = array_merge($this->$type,$relation);
			}
		}
	}


	/**
	 * Set single rule.
	 *
	 * This method add single rule to $rules field.
	 * Rules definitions have to be proper for KohanaPHP Validation object
	 *
	 * @param string $field field name
	 * @param string $rule rule name
	 * @param array $definition rule definition
	 *
	 * @return void
	 */
	protected function setRule($field, $rule, $translated = false)
	{
		if(is_array($rule)){
			if($translated and Kohana::$config->load('global.languages.multilanguage')){
				$this->translated_rules[$field][] = array($rule['name'], $rule['definition']);
			}else{
				$this->rules[$field][] = array($rule['name'], $rule['definition']);
			}
		}else{
			if($translated and Kohana::$config->load('global.languages.multilanguage')){
				$this->translated_rules[$field][] = array($rule);
			}else{
				$this->rules[$field][] = array($rule);
			}

		}
	}

	/**
	 * Set object rules.
	 *
	 * In this class should be defined validation rules for this object
	 *
	 * Inheriting classes, except the first one, should always execute
	 * this class for parent class, to make sure all the rules are defined.
	 *
	 * @return void
	 */
	protected function setRules(){}

	/**
	 * Set model values
	 *
	 * Set model values - both stored in data store
	 * and those that are not stored in data store.
	 *
	 * @param $data
	 *
	 * @return void
	 */
	public function setValues($data = null)
	{
		$_data = $data;
		foreach($data as $item => $value){
			if($value==="" or $value==='') unset($_data[$item]);
		}
		unset($value);
		if($data) foreach($data as $name=>$value) try{$this->$name = $value;} catch(Exception $e){}
		if(isset($this->id)) if($this->id == '') unset($this->id);
	}

	/**
	 * Convert objects to array.
	 *
	 * @param $data object or collection of objects
	 *
	 * @return array
	 */
	public function toArray($data = null)
	{
		if($data){
			$_data = array();
			if($data instanceof Database_MySQLi_Result){
				foreach($data as $row){
					if($row instanceof stdClass){
						$_data[] = (array) $row;
					}else{
						$_data[] = $row->toArrayRow();
					}
				}
				$data = $_data;
				return $data;
			}else{
				$this->toArrayRow();
			}
		}else{
			return $this->toArrayRow();
		}
	}

	/**
	 * Get single row as Array.
	 *
	 * @return array
	 */
	public function toArrayRow()
	{
		return $this->_object;
	}

	/**
	 * Display number verbally.
	 *
	 * Works for numbers to 9999999.
	 *
	 * @param string $column column name
	 * @param string $mode currency | number
	 *
	 * @return string verbal version of number
	 */
	public function verbally($column, $mode = 'currency')
	{
		$parts = array(
			'units' => array(
				'zero',
				'jeden',
				'dwa',
				'trzy',
				'cztery',
				'pięć',
				'sześć',
				'siedem',
				'osiem',
				'dziewięć',
				'dziesięć',
				'jedenaście',
				'dwanaście',
				'trzynaście',
				'czternaście',
				'piętnaście',
				'szesnaście',
				'siedemnaście',
				'osiemnaście',
				'dziewiętnaście'
			),
			'tens' => array(
				'',
				'dziesięć',
				'dwadzieścia',
				'trzydzieści',
				'czterdzieści',
				'pięćdziesiąt',
				'sześćdziesiąt',
				'siedemdziesiąt',
				'osiemdziesiąt',
				'dziewięćdziesiąt'
			),
			'hundreds' => array(
				'',
				'sto',
				'dwieście',
				'trzysta',
				'czterysta',
				'pięćset',
				'sześćset',
				'siedemset',
				'osiemset',
				'dziewięćset'
			),
			'thousands' => array(
				'tysiąc',
				'tysiące',
				'tysięcy'
			),
			'millions' => array(
				'milion',
				'miliony',
				'milionów'
			),
		);
		$number = $this->$column;
		if($number>9999999) return 'number to high';
		$integer = (string)floor($number);
		$decimal = (string)number_format(($number - floor($number)) * 100);
		$integer_verbally = '';
		for($i=0;$i<strlen($integer);$i++){
			if($i==strlen($integer)-7){
				if($integer[$i-1]==0){
					if($integer[$i-1]==0)
						if($integer[$i]==1) $integer_verbally .= $parts['units'][$integer[$i]].' '.$parts['millions'][0].' ';
						if($integer[$i]>=2 and $integer[$i]<=4) $integer_verbally .= $parts['units'][$integer[$i]].' '.$parts['millions'][1].' ';
						if($integer[$i]>=5) $integer_verbally .= $parts['units'][$integer[$i]].' '.$parts['millions'][2].' ';
				}
			}
			if($i==strlen($integer)-6) $integer_verbally .= $parts['hundreds'][$integer[$i]].' ';
			if($i==strlen($integer)-5){
				if($integer[$i]==1) $integer_verbally .= $parts['units'][$integer[$i].$integer[$i+1]].' '.$parts['thousands'][2].' ';
				if($integer[$i]>1){
					if($integer[$i+1]==0) $integer_verbally .= $parts['tens'][$integer[$i]].' '.$parts['thousands'][2].' ';
					if($integer[$i+1]==1 or $integer[$i+1]>=5) $integer_verbally .= $parts['tens'][$integer[$i]].' '.$parts['units'][$integer[$i+1]].' '.$parts['thousands'][2].' ';
					if($integer[$i+1]>=2 and $integer[$i+1]<=4) $integer_verbally .= $parts['tens'][$integer[$i]].' '.$parts['units'][$integer[$i+1]].' '.$parts['thousands'][1].' ';
				}
			}
			if($i==strlen($integer)-4){
				if($integer[$i-1]==0 and $integer[$i-2]==0){
					if($integer[$i]==1) $integer_verbally .= $parts['thousands'][0].' ';
					if($integer[$i]>=2 and $integer[$i]<=4) $integer_verbally .= $parts['units'][$integer[$i]].' '.$parts['thousands'][1].' ';
					if($integer[$i]>=5 and $integer[$i]<=9) $integer_verbally .= $parts['units'][$integer[$i]].' '.$parts['thousands'][2].' ';
				}
			}
			if($i==strlen($integer)-3) $integer_verbally .= $parts['hundreds'][$integer[$i]].' ';
			if($i==strlen($integer)-2){
				if($integer[$i]==1) $integer_verbally .= $parts['units'][$integer[$i].$integer[$i+1]];
				if($integer[$i]>=2) $integer_verbally .= $parts['tens'][$integer[$i]].' ';
			}
			if($i==strlen($integer)-1 and ($integer[strlen($integer)-2]==0 or $integer[strlen($integer)-2]>=2)){
				if($integer[$i]>0) $integer_verbally .= $parts['units'][$integer[$i]];
				if($number===0) $integer_verbally = $parts['units'][0];
			}
		}
		$decimal_verbally = '';
		for($i=0;$i<strlen($decimal);$i++){
			if($i==strlen($decimal)-7){
				if($decimal[$i-1]==0){
					if($decimal[$i-1]==0)
						if($decimal[$i]==1) $decimal_verbally .= $parts['units'][$decimal[$i]].' '.$parts['millions'][0].' ';
						if($decimal[$i]>=2 and $decimal[$i]<=4) $decimal_verbally .= $parts['units'][$decimal[$i]].' '.$parts['millions'][1].' ';
						if($decimal[$i]>=5) $decimal_verbally .= $parts['units'][$decimal[$i]].' '.$parts['millions'][2].' ';
				}
			}
			if($i==strlen($decimal)-6) $decimal_verbally .= $parts['hundreds'][$decimal[$i]].' ';
			if($i==strlen($decimal)-5){
				if($decimal[$i]==1) $decimal_verbally .= $parts['units'][$decimal[$i].$decimal[$i+1]].' '.$parts['thousands'][2].' ';
				if($decimal[$i]>1){
					if($decimal[$i+1]==0) $decimal_verbally .= $parts['tens'][$decimal[$i]].' '.$parts['thousands'][2].' ';
					if($decimal[$i+1]==1 or $decimal[$i+1]>=5) $decimal_verbally .= $parts['tens'][$decimal[$i]].' '.$parts['units'][$decimal[$i+1]].' '.$parts['thousands'][2].' ';
					if($decimal[$i+1]>=2 and $decimal[$i+1]<=4) $decimal_verbally .= $parts['tens'][$decimal[$i]].' '.$parts['units'][$decimal[$i+1]].' '.$parts['thousands'][1].' ';
				}
			}
			if($i==strlen($decimal)-4){
				if($decimal[$i-1]==0 and $decimal[$i-2]==0){
					if($decimal[$i]==1) $decimal_verbally .= $parts['thousands'][0].' ';
					if($decimal[$i]>=2 and $decimal[$i]<=4) $decimal_verbally .= $parts['units'][$decimal[$i]].' '.$parts['thousands'][1].' ';
					if($decimal[$i]>=5 and $decimal[$i]<=9) $decimal_verbally .= $parts['units'][$decimal[$i]].' '.$parts['thousands'][2].' ';
				}
			}
			if($i==strlen($decimal)-3) $decimal_verbally .= $parts['hundreds'][$decimal[$i]].' ';
			if($i==strlen($decimal)-2){
				if($decimal[$i]==1) $decimal_verbally .= $parts['units'][$decimal[$i].$decimal[$i+1]];
				if($decimal[$i]>=2) $decimal_verbally .= $parts['tens'][$decimal[$i]].' ';
			}
			if($i==strlen($decimal)-1 and ($decimal[strlen($decimal)-2]==0 or $decimal[strlen($decimal)-2]>=2)){
				if($decimal[$i]>0) $decimal_verbally .= $parts['units'][$decimal[$i]];
				if($number===0) $decimal_verbally = $parts['units'][0];
			}
		}
		switch($mode){
			case 'currency':
				$integer_verbally .= ' zł';
				if($decimal_verbally) $decimal_verbally .= ' gr';
				if($decimal_verbally) $verbally = $integer_verbally.' '.$decimal_verbally;
				else $verbally = $integer_verbally;
				return $verbally;
			case 'number':
				$decimal_verbally .= ' / 100';
				$verbally = $integer_verbally . $decimal_verbally;
				return $verbally;
			default: die('wrong mode in Model_BaseOrm::verbally');
		}
	}
	
	/**
	 * Change url base to placeholder.
	 *
	 * Change url base (for example "/") to {{url_base}}.
	 *
	 * @param string $data data to convert
	 *
	 * @return string converted data
	 */
	protected function setBaseUrlPlaceholder($data)
	{
		return str_replace('<img src="'.URL::base(),'<img src="'.'{{url_base}}',$data);
	}
	
	/**
	 * Change placeholder to url base.
	 *
	 * Change {{url_base}} placeholder to url base (for example "/").
	 *
	 * @param string $data data to convert
	 *
	 * @return string converted data
	 */
	protected function getBaseUrlPlaceholder($data)
	{
		return str_replace('<img src="'.'{{url_base}}','<img src="'.URL::base(),$data);
	}
	
	/**
	 * Change placeholders to data
	 * 
	 * @param array $data data
	 * 
	 * @return array converted data
	 */
	protected function getPlaceholders($data = null)
	{
		if($data){
			$_data = array();
			if(count($data)){
				foreach($data as $name => $value){
					$value= $this->getBaseUrlPlaceholder($value);
					$_data[$name] = $value;
				}
			}
			return $_data;
		}
	}
}
