<?php defined('SYSPATH') OR die('No direct script access.');

class Model_App_Content_Category extends Model_Base
{
	/**
	 * @inherit
	 */
	protected $_table_name = 'contents_categories';

	/**
	 * @inherit
	 */
	public function setRelations()
	{
		$this->setRelation('has_many','children','Content_Category','parent_id');
		$this->setRelation('has_many','contents','Content','category_id');
		$this->setRelation('belongs_to','parent','Content_Category','parent_id');
		parent::setRelations();
	}

	/**
	 * Edit one category object
	 *
	 * @param array $post data for content
	 *
	 * @return true if save success, otherwise false
	 */
	public function editOne($post)
	{
		$this->init($post);
		try{
			$this->save($this->getFormRules('edit_category'));
			return true;
		}catch(ORM_Validation_Exception $e){
			Error::set($e);
			return false;
		}
	}

	/**
	 * Get categories list.
	 *
	 * If no parent id provided, get root categories.
	 *
	 * @param int $parent_id parent category id
	 *
	 * @return array[Model_Content_Category] tree structured array of categories
	 */
	public function getList($parent_id = null)
	{
		return $this->where('parent_id','=',$parent_id)->find_all();
	}

	/**
	 * @inherit
	 */
	protected function init($post)
	{
		$data = $post['data'];
		$this->setData($data['category']);
		isset($post['parameters']) ? $parameters = $post['parameters'] : $parameters = array();
		if($parameters){
			if(isset($parameters['edit_category_submit'])) $this->form = 'edit_category';
		}
		parent::init($post);
	}
}
