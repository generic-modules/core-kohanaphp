<?php defined('SYSPATH') or die('No direct script access.');

class Model_App_Role_Function extends Model_Base
{
	/**
	 * @var $_table
	 *
	 * Database table name for this model
	 */	
	protected $_table = 'roles_functions';

	/**
	 * @var $_belongs_to
	 * 
	 * Belongs to relationships
	 */
	protected $_belongs_to = array(
		'role'		=> array('model' => 'Role', 'foreign_key' => 'role_id'),
		'function'	=> array('model' => 'Function', 'foreign_key' => 'function_id'),
	);

}
