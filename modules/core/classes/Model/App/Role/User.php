<?php defined('SYSPATH') or die('No direct script access.');

class Model_App_Role_User extends Model_Base
{
				
	/**
	 * @var $_table
	 *
	 * Database table name for this model
	 */	
	protected $_table = 'roles_users';

	/**
	 * @var $_has_many
	 * 
	 * Has many relationships
	 */
	protected $_belongs_to = array(
		'user' => array('model' => 'User', 'foreign_key' => 'user_id'),
		'role' => array('model' => 'Role', 'foreign_key' => 'user_id'),
	);	
}
