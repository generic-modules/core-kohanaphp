<?php defined('SYSPATH') OR die('No direct script access.');

class Model_App_Content extends Model_Base
{
	/**
	 * @inherit
	 */
	protected $_table_name = 'contents';
	
	
	public function setRelations()
	{	
		$this->is_translated = true;
		$this->setRelation('belongs_to','category','Content_Category','category_id');
		parent::setRelations();
	}
	
	/**
	 * Create one content.
	 *
	 * @param array $post content data
	 * @param Model_Content_Category $category category object
	 *
	 * @return true if success, otherwise false
	 */
	public function createOne($post, $category)
	{
		$this->init($post);
		$this->category_id = $category->id;
		try{
			$this->save($this->getFormRules('create_content'));
			return true;
		}catch(ORM_Validation_Exception $e){
			Error::set($e);
			return false;
		}
	}

	/**
	 * Edit one content object
	 *
	 * @param array $post data for content
	 *
	 * @return true if save success, otherwise false
	 */
	public function editOne($post)
	{
		$this->init($post);
		if($this->category->id == Kohana::$config->load('models.content.email_templates_category')){
			if(!strstr(str_replace(' ','',$this->data['content']),"{{activation_link}}")){
				Error::set(['element'=>'content','errors'=>'activation_link_placeholder_required']);
				return false;
			}
		}
		try{
			$this->begin();
			$this->save($this->getFormRules('edit_content'));
			$this->commit();
			return true;
		}catch(ORM_Validation_Exception $e){
			$this->rollback();
			Error::set($e);
			return false;
		}
	}

	/**
	 * Get news list.
	 * 
	 * @param int number news number, default null means all
	 * @param array $options select options
	 * 
	 * @return array[Model_Content]
	 */
	public function getNews($number = null, $options = array())
	{
		if($number) $options['limit'] = $number;
		$options['order_by'] = ['column'=>'update_date','direction'=>'desc'];
		$options['where'][] = ['category_id',ORM::factory('Content_Category')->getOne('news')->id];
		return parent::getList($options);
	}
	
	/**
	 * Display contents list.
	 *
	 * @pparam mixed $category_id contents category id or array of categories id numbers
	 *
	 * @return array[Model_Content] contents collection
	 */
	public function getPublished($category_id)
	{
		if(is_array($category_id)){
			return $this->where('category_id','in',$category_id)->where('is_published','=',true)->find_all();
		}else{
			return $this->where('category_id','=',$category_id)->where('is_published','=',true)->find_all();
		}
	}
	
	/**
	 * @inherit
	 */
	protected function init($post)
	{
		$data = $post['data'];
		if(isset($data['content'])){
			$data['content'] = $this->setBaseUrlPlaceholder($data['content']);
		}
		if(Kohana::$config->load('global.languages.multilanguage')){
			
		}else{
			$data['lang']['title'] = $data['title'];
			$data['lang']['content'] = $data['content'];
		}		
		isset($post['parameters']) ? $parameters = $post['parameters'] : $parameters = array();
		if($parameters){
			if(isset($parameters['create_content_submit'])) $this->form = 'create_content';
		}
		if(!$this->id) $this->create_date = date('Y-m-d');
		$this->update_date = date('Y-m-d');
		
		
		$post['data'] = $data;
		parent::init($post);
	}
	
	/**
	 * Remove one content.
	 *
	 * @param int $id content id
	 *
	 * @return void
	 */
	public function removeOne($id)
	{
		$this->where('id','=',$id)->find();
		$this->delete();
	}

	/**
	 * @inherit
	 */
	protected function setData($post)
	{
		parent::setData($post);
		isset($this->data['is_published']) ? $this->is_published = true : $this->is_published = false;
		if(Kohana::$config->load('global.languages.multilanguage')){
			foreach($this->data['lang'] as $lang => $value){
				if(isset($value['editor'])){
					$this->data['lang'][$lang]['content'] = $value['editor'];
					unset($this->data['lang'][$lang]['editor']);
				}
			}
		}else{
			if(isset($value['editor'])){
				$this->data['lang'][Lang::get()]['content'] = $value['editor'];
				unset($this->data['lang'][Lang::get()]['editor']);
			}
		}
	}

	/**
	 * @inherit
	 */
	protected function setRules()
	{
		parent::setRules();
		$this->setRule('title','not_empty',true);
	}
}
