<?php defined('SYSPATH') or die('No direct script access.');

class Model_App_User extends Model_Base
{
	/**
	 * @inherit
	 */
	protected $_table_name = 'users';

	/**
	 * @inherit
	 */
	protected $data = array();

	/**
	 * @var Model_Address $livingAddress
	 *
	 * Shortcut for relation with address type living address
	 */
	public $living_address = null;

	/**
	 * @var Model_Role
	 */
	public $role = null;

	/**
	 * Set relations with other models.
	 *
	 * @return array relations array
	 */
	public function setRelations()
	{
		$this->setRelation('has_many','addresses','Address','user_id');
		$this->setRelation('has_many','emails','Email','user_id');
		$this->setRelation('has_many','roles','Role','user_id','role_id','roles_users');
		$this->setRelation('has_many','phones','Phone','user_id');
		$this->setRelation('has_one','contact_person','ContactPerson','user_id');
		$this->living_address = ORM::factory('Address');
		parent::setRelations();
	}

	/**
	 * @inherit
	 */
	public function init($post, $options = null)
	{
	    $this->post = $post;
	    
	    if(isset($post['data'])) $data = $post['data'];
	    else $data = $post;
	    
	    if(isset($data['is_company'])){
	        $data['is_company'] = 1;
	    }else{
	        $data['is_company'] = 0;
	        $data['company_name'] = '';
	    }
	    
	    if(isset($data['role'])){
	        $data = $this->setRelated('roles',$data);
	    }
	    
	    if(isset($data['contact_person'])) {
	        $data = $this->setRelated('contact_person',$data);
	    }
	    if(Kohana::$config->load('models.address.multiple')){
	        if(isset($data['living_address'])) $data = $this->setRelated('living_address',$data);
	    }else{
	        if(isset($data['address'])) $data = $this->setRelated('address',$data);
	    }
	    if(Kohana::$config->load('models.email.multiple')){
	        if(isset($data['emails'])){
	            $data = $this->setRelated('emails',$data,true);
	        }
	    }
	    if(!$this->create_date) $data['create_date'] = date('Y-m-d H:i:s');
	    $data['update_date'] = date('Y-m-d H:i:s');
	    /**
	     * @TODO: Move that to module Driving School
	     */
	    if(isset($data['type'])){
	        switch($data['type']){
	            case 'lecturer':
	                $this->is_instructor = false;
	                $this->is_lecturer = true;
	                break;
	            case 'instructor':
	                $this->is_instructor = true;
	                $this->is_lecturer = false;
	        }
	        unset($data['type']);
	    }
	    /**
	     * @TODO: Move that to module Driving School
	     */
	    if(isset($data['is_working'])){
	        switch($data['is_working']){
	            case 'yes':
	                $this->is_working = true;
	                break;
	            case 'no':
	                $this->is_working = false;
	                break;
	        }
	        unset($data['is_working']);
	    }
	    if(isset($data['is_active'])){
	        switch($data['is_active']){
	            case 'yes':
	                $this->is_active = true;
	                break;
	            case 'no':
	                $this->is_active = false;
	                break;
	        }
	        unset($data['is_active']);
	    }
	    $post['data'] = $data;
	    parent::init($post);
	}
	
	/**
	 * @inherit
	 */
	protected function removeType()
	{
		return Kohana::$config->load('models.user.remove');
	}

	/**
	 * @inherit
	 */
	protected function refreashRelatedData()
	{
		if(isset($this->living_address)){
			$living_address = ORM::factory('Address');
			foreach($this->living_address as $name => $value){
				$living_address->$name = $value;
			}
			$this->living_address = $living_address;
		}
		parent::refreashRelatedData();
	}
	
	/**
	 * @inherit
	 */
	public function removeRelated()
	{
		if($this->addresses->count_all() > 0){
			$addresses = $this->addresses->getList();
			foreach($addresses as $address){
				$address->delete();
			}
		}
		parent::removeRelated();
	}

	/**
	 * Assign roles to user.
	 *
	 * @param Model_Role collection $roles roles to assign
	 */
	public function assignRoles($roles)
	{
		if($this->roles){
			foreach($this->roles->find_all() as $role){
				$this->remove('roles',$role);
			}
		}
		if($roles){
			foreach($roles as $role){
				$this->add('roles',$role);
			}
		}
	}

	/**
	 * Change password.
	 *
	 * If there are any errors, it is stored in session.
	 *
	 * @param array $post data to store
	 *
	 * @return true if success, otherwise false
	 */
	public function changePassword($post)
	{
		$this->init($post);
		try{
			$this->save($this->getFormRules('change_password'));
			return true;
		} catch(ORM_Validation_Exception $e) {
			Error::set($e);
			return false;
		}
	}

	/**
	 * Check email
	 *
	 * Check if provided email is valid.
	 * If is valid, get user by email.
	 *
	 * If there are any errors, it is stored in session.
	 *
	 * @param array $post data containing email
	 *
	 * @return true if success, otherwise false
	 */
	public function checkEmail($post)
	{
		$this->where('is_deleted','=',false)->where('email','=',$post['data']['email'])->find();
		if($this->email===$post['data']['email']){
			return true;
		}else{
			Error::set(array('element'=>'email','errors'=>'not_match'));
			return false;
		}
	}

	/**
	 * @inherit
	 */
	public function countPages($items_per_page = null)
	{
		$items_per_page = Kohana::$config->load('controllers.admin.users_management.users_list.records_per_page');
		return parent::countPages($items_per_page);
	}
	
	/**
	 * Create user account.
	 *
	 * If there are any errors, it is stored in session.
	 *
	 * @param array $post data to store for user account
	 * @param array $options
	 * - role: role for created user
	 *
	 * @return true if success, otherwise false
	 */
	public function createOne($post,$options = array())
	{
		$this->init($post,$options);
		$errors = false;
		if(isset($this->data['username'])){
			$check = ORM::factory('User')->where('username','=',$this->data['username'])->find();
			if($check->username===$this->data['username']){
				Error::set(array('element'=>'username','errors'=>'unique'));
				$errors = true;
			}
		}
		if(Kohana::$config->load('models.email.multiple')){
			foreach($this->related['email']['data'] as $email){
				$check = ORM::factory('User')->where('email','=',$email)->find();
				if($check->email===$email){
					Error::set(array('element'=>'email','errors'=>'unique'));
					$errors = true;
				}
			}
		}else{
			$check = ORM::factory('User')->where('email','=',$this->data['email'])->find();
			if($check->email===$this->data['email']){
				Error::set(array('element'=>'email','errors'=>'unique'));
				$errors = true;
			}
		}
		
		
		if(isset($this->data['password']) and isset($this->data['password_repeat'])){
			if($this->data['password'] != $this->data['password_repeat']){
				Error::set(['element'=>'password_repeat','errors'=>'matches']);
				$errors = true;
			}
		}
		
		if($errors){
			return false;
		}
		if(isset($options['role'])){
			$role = ORM::factory('Role')->where('system_name','=',$options['role'])->find();
		}else{
			if(isset($post['data']['role'])){
				$role = ORM::factory('Role')->where('id','=',$post['data']['role'])->find();
			}
		}
		try{
			$this->begin();
			if($this->data['user_id'] = $this->saveOne(null,$this->getFormRules('create_account'))){
				if($this->add('roles',ORM::factory('Role')->where('system_name','=','login')->find())){
					if($this->saveRelated()){
						$this->commit();
						return true;
					}
				}
			}
			$this->rollback();
			return false;
		} catch(ORM_Validation_Exception $e) {
			$this->rollback();
			Error::set($e);
			return false;
		}
	}

	/**
	 * Edit user account.
	 *
	 * If there are any errors, it is stored in session.
	 *
	 * @param array $post data to store for user account
	 *
	 * @return true if success, otherwise false
	 */
	public function edit($post)
	{
		$this->init($post);

		if(isset($this->data['password']) and isset($this->data['password_repeat'])){
			if($this->data['password'] != $this->data['password_repeat']){
				Error::set(['element'=>'password_repeat','errors'=>'matches']);
				$errors = true;
			}
		}
		
		if($errors){
			return false;
		}

		//echo '<pre>'; var_dump($this->related); die;

		/**
		 * @todo: fix this to work only if email is set as login and only for email used for login
		 */
// 		if(Kohana::$config->load('models.email.multiple')){
// 			foreach($this->related['emails']['data'] as $email){
// 				$check = ORM::factory('User')->where('id','<>',$this->id)->where('email','=',$email)->find();
// 				if($check->email===$email){
// 					Error::set(array('element'=>'email','errors'=>'unique'));
// 					return false;
// 				}
// 			}
// 		}else{
// 			$check = ORM::factory('User')->where('id','<>',$this->id)->where('email','=',$this->data['email'])->find();
// 			if($check->email===$this->data['email']){
// 				Error::set(array('element'=>'email','errors'=>'unique'));
// 				return false;
// 			}
// 		}
		try{
			$this->begin();
			if($this->save($this->getFormRules('edit_account'))){
				if($this->saveRelated()){
					$this->commit();
					return true;
				}
			}
			$this->rollback();
			return false;
		} catch(ORM_Validation_Exception $e) {
			$this->rollback();
			Error::set($e);
			return false;
		}
	}

	/**
	 * Get active users list.
	 *
	 * @return active users collection
	 */
	public function getActive($options = array())
	{
		return $this->where('is_active','=',true)->where('is_deleted','=','false')->find_all();
	}

	/**
	 * Get all users list (including deleted).
	 *
	 * @return all users collection (including deleted)
	 */
	public function getAll()
	{
		return $this->find_all();
	}

	/**
	 * Get users by role name.
	 *
	 * @param string $role role name
	 * @param array $options
	 * - array: get data as array
	 *
	 * @return array[Model_User]
	 */
	public function getByRole($role,$options = array())
	{
		$role = ORM::factory('Role')->where('system_name','=',$role)->find();
		if(isset($options['order_by'])){
			$role->order_by($options['order_by']['column'],$options['order_by']['direction']);
		}
		if(isset($options['filters'])){
			$filters = $options['filters'];
		}else{
			$filters = null;
		}
		
		if(isset($options['columns'])){
			return $role->users->getList($options);
		}
		
		if(isset($options['array'])){
			$data = array();
			foreach($role->users->getList(['filters'=>$filters,'order_by'=>$options['order_by']]) as $row){
				$data[] = $row->_object;
			}
			return $data;
		}else{
			return $role->users->getList(array('filters'=>$filters,'order_by'=>$options['order_by']));
		}
	}

	/**
	 * Get email by type system name.
	 *
	 * @param string $type system name
	 */
	public function getEmailByType($type)
	{
		$type = ORM::factory('Email_Type')->where('system_name','=',$type);
		return $this->emails->where('type_id','=',$type->id)->find();
	}

	/**
	 * Get user's living address.
	 *
	 * @return Model_Address address object
	 */
	public function getLivingAddress()
	{
		$this->addresses->where('type_id','=',Kohana::$config->load('models.address.living_address_id'))->find();
	}

	/**
	 * Get users list.
	 *
	 * @param int $page page number
	 * @param int $pageCount items per page
	 * @param bool $count only counts and returns pages number
	 * @param bool $array get results as array
	 *
	 * @return all not deleted users collection or pages number
	 */
	public function getList($options = array())
	{
		
		if(isset($options['pager'])){
			
		}
		
		return parent::getList($options);
// 		if($array){
// 			return parent::getList($array);
// 		}else{
// 			if($page){
// 				if($count) return ceil($this->where('is_deleted','=',false)->count_all()/$pageCount);
// 				$offset = ($page-1)*$pageCount;
// 				return $this->where('is_deleted','=',false)->limit($pageCount)->offset($offset)->find_all();
// 			}else{
// 				return $this->where('is_deleted','=',false)->find_all();
// 			}
// 		}
	}

	/**
	 * Get one user.
	 *
	 * @param int $id users id
	 *
	 * @return Model_User
	 */
	public function getOne($id = null, $options = array())
	{
		if($id) $this->where('id','=',$id);
		return $this->find();
	}

	/**
	 * Check if user has role.
	 *
	 * @param string $role role name.
	 * @param bool $hierarchy take into account hierarchical roles
	 *
	 * @return bool
	 */
	public function hasRole($role, $hierarchy = false)
	{
		if($hierarchy){
			$role = ORM::factory('Role')->where('system_name','=',$role)->find();
			foreach($this->roles->find_all() as $user_role){
				if($user_role->hierarchy <= $role->hierarchy) return true;
			}
		}else{
			$role = ORM::factory('Role')->where('system_name','=',$role)->find();
			if($this->has('roles',$role)) return true;
		}
		return false;
	}

	/**
	 * Login user.
	 *
	 * @param array $post data to validate user
	 * @param string $username username|email - what should be used for authorization
	 *
	 * @return true, if login success, otherwise false
	 */
	public function login($post)
	{
		if(isset($post['data']['username'])){
			$username = $post['data']['username'];
		}else if($post['data']['email']){
			$username = $post['data']['email'];
		}else{
			die('no username');
		}
		$password = $post['data']['password'];
		if(Auth::instance()->login($username,$password)){
			return true;
		}
		Error::set(array('element'=>'username_password','errors'=>'not_valid'));
		return false;
	}

	/**
	 * Logout user
	 *
	 * @return void
	 */
	public function logout()
	{
		Auth::instance()->logout();
	}

	/**
	 * Reset password.
	 *
	 * Generate random password and set it for user.
	 *
	 * @return generated password or false if failed to save
	 */
	public function resetPassword()
	{
		$password = substr(sha1(rand()), 0, Kohana::$config->load('models.user.reset_password_length'));
		$this->password = Auth::instance()->hash_password($password);
		if($this->save()){
			return $password;
		}
		return false;
	}

	/**
	 * @inherit
	 */
	public function saveRelated()
	{
		if(parent::saveRelated()){
			
			if(isset($this->related['roles'])){
				if($this->roles->count_all() > 0){
					foreach($this->roles->find_all() as $role){
						$this->remove('roles',$role);
					}
				}
			}
			
			if(Kohana::$config->load('models.address.multiple')){

			}else{
				if(isset($this->related['address'])){
					if($this->addresses->count_all() == 0){
						$type = ORM::factory('Address_Type')->where('system_name','=','general')->find();
						$address = ORM::factory('Address');
						$address->user_id = $this->id;
						$address->type_id = $type->id;
					}else{
						foreach($this->addresses->find_all() as $_address){
							$address = $_address;
						}
					}
					$address->saveOne($this->related['address']);
				}
				
			}
			if(Kohana::$config->load('models.email.multiple')){
				foreach($this->related['emails'] as $type => $value){
					$value = $value['data'];
					$type = ORM::factory('Email_Type')->where('system_name','=',$type)->find();
					$email = ORM::factory('Email')->where('user_id','=',$this->id)->where('type_id','=',$type->id)->find();
					if(!$email->id){
						$email->user_id = $this->id;
						$email->type_id = $type->id;
						$email->is_default = false;
					}
					$email->address = $value;
					$email->save();
				}
			}
			if(isset($this->options['role'])){
				$role = ORM::factory('Role')->where('system_name','=',$this->options['role'])->find();
			}else if(isset($this->data['role'])){
				if($this->data['role'] > 0){
					$role = ORM::factory('Role')->where('id','=',$this->data['role'])->find();
				}
			}
			if(isset($role)){
				$this->remove('roles',$this->roles->getVisible());
				if(!$this->has('roles',ORM::factory('Role')->where('system_name','=','login')->find())){
					$this->add('roles',ORM::factory('Role')->where('system_name','=','login')->find());
				}
				$this->add('roles',ORM::factory('Role')->where('id','=',$role->id)->find());
			}
		}
		return true;
	}

	/**
	 * Send password.
	 *
	 * Send password to provided email address.
	 *
	 * @param string $email email to send password
	 * @param string $password password to send
	 *
	 * @return void
	 */
	public function sendPassword($email,$password)
	{
		$from = ORM::factory('Setting')->getOne('company_email');
		$subject = __('user.users.management.reset_password.email.subject');
		$content = __('user.users.management.reset_password.email.content', array(':password' => $password));
		$headers = '';
		$headers .= 'From: '.$from;
		$headers .= '\r\n';
		$headers .= 'Reply-to: '.$from;
		mail($email,$subject,$content,'From: '.$headers);
	}

	/**
	 * @inherit
	 */
	protected function setData($data)
	{
		if(isset($data)){
			if(isset($data['id'])){
				if($data['id']=='') unset($data['id']);
			}
		}
		if(isset($this->parameters['password_change_check'])){
			if($this->parameters['password_change_check']!=='on') unset($data['password']);
		}
		parent::setData($data);
	}

	/**
	 * @inherit
	 */
	protected function setFormsRules()
	{
		parent::setFormsRules();
		switch($this->form){
			case 'create_user':
				$this->formRules['create_account'] = Validation::factory($this->data);
				$this->setFormRule('create_account','password_repeat','matches',array(':validation',':field','password'));
				//$this->setFormRule('create_account','password','max_length',array(':value',40));
				//$this->setFormRule('create_account','password','min_length',array(':value',8));
				$this->setFormRule('create_account','password','not_empty');
				break;
			case 'edit_user':
				$this->formRules['edit_account'] = Validation::factory($this->data);
				if(isset($this->data['change_password_check'])){
					$this->setFormRule('edit_account','password_repeat','matches',array(':validation',':field','password'));
					$this->setFormRule('edit_account','password','max_length',array(':value',40));
					$this->setFormRule('edit_account','password','min_length',array(':value',8));
					$this->setFormRule('edit_account','password','not_empty');
				}
				break;
			case 'change_password':
				$this->formRules['change_password'] = Validation::factory($this->data);
				$this->setFormRule('change_password','password_repeat','matches',array(':validation',':field','password'));
				$this->setFormRule('change_password','password','max_length', array(':value', 40));
				$this->setFormRule('change_password','password','min_length', array(':value', 8));
				$this->setFormRule('change_password','password','not_empty');
				break;
		}
	}

	/**
	 * @inherit
	 */
	protected function setRules()
	{
		parent::setRules();
		switch($this->form){
			case 'create_user':
				if(isset($this->data['email'])){
					$this->setRule('email', 'email');
					$this->setRule('email', array('name'=>'max_length','definition'=>array(':value', 40)));
					$this->setRule('email', array('name'=>'min_length','definition'=>array(':value', 5)));
					$this->setRule('email', 'not_empty');
				}
				if(isset($this->data['name'])){
					$this->setRule('name', array('name'=>'regex','definition'=>array(':value',Kohana::$config->load('validation.alpha_pl'))));
					$this->setRule('name', array('name'=>'max_length','definition'=>array(':value', 40)));
					$this->setRule('name', array('name'=>'min_length','definition'=>array(':value', 3)));
				}
				if(isset($this->data['pesel'])){
					$this->setRule('pesel', 'digit');
					$this->setRule('pesel', array('name'=>'max_length','definition'=>array(':value', 11)));
				}
				if(isset($this->data['surname'])){
					$this->setRule('surname', array('name'=>'regex','definition'=>array(':value',Kohana::$config->load('validation.alpha_pl'))));
					$this->setRule('surname', array('name'=>'max_length','definition'=>array(':value', 40)));
					$this->setRule('surname', array('name'=>'min_length','definition'=>array(':value', 3)));
				}
				if(isset($this->data['username'])){
					$this->setRule('username', 'alpha_numeric');
					$this->setRule('username', array('name'=>'max_length','definition'=>array(':value', 20)));
					$this->setRule('username', 'not_empty');
				}
				if(isset($this->data['phone_number'])){
					$this->setRule('phone_number', 'digit');
				}
				break;
			case 'edit_user':
				if(isset($this->data['email'])){
					$this->setRule('email', 'email');
					$this->setRule('email', array('name'=>'max_length','definition'=>array(':value', 40)));
					$this->setRule('email', array('name'=>'min_length','definition'=>array(':value', 5)));
					$this->setRule('email', 'not_empty');
				}
				if(isset($this->data['name'])){
					$this->setRule('name', array('name'=>'regex','definition'=>array(':value',Kohana::$config->load('validation.alpha_pl'))));
					$this->setRule('name', array('name'=>'max_length','definition'=>array(':value', 40)));
					$this->setRule('name', array('name'=>'min_length','definition'=>array(':value', 3)));
				}
				if(isset($this->data['pesel'])){
					$this->setRule('pesel', 'digit');
					$this->setRule('pesel', array('name'=>'max_length','definition'=>array(':value', 11)));
				}
				if(isset($this->data['surname'])){
					$this->setRule('surname', array('name'=>'regex','definition'=>array(':value',Kohana::$config->load('validation.alpha_pl'))));
					$this->setRule('surname', array('name'=>'max_length','definition'=>array(':value', 40)));
					$this->setRule('surname', array('name'=>'min_length','definition'=>array(':value', 3)));
				}
				if(isset($this->data['phone_number'])){
					$this->setRule('phone_number', 'digit');
				}
				break;
		}
	}

	/**
	 * @inherit
	 */
	public function toArrayRow()
	{
		$data = parent::toArrayRow();
		$data['role'] = $this->roles->getVisible()->id;
		if(!Kohana::$config->load('models.address.multiple')){
			if($this->addresses->count_all() > 0){
				foreach($this->addresses->find_all() as $address){
					$data['address'] = $address->toArrayRow();
				}
			}else{
				$data['address'] = array();
			}
		}
		return $data;
	}
}
