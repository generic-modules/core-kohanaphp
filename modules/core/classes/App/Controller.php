<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

class App_Controller extends Kohana_Controller
{
	/**
	 * @inherit
	 */
	public static function redirect($uri = '', $code = 302)
	{
		if(is_array($uri)){
			$parameters = $uri[1];
			$uri = $uri[0];
			$first = true;
			foreach($parameters as $name => $value){
				if($first){
					$first = false;
					
					// shipments/costam/?name=value&name=value&name=value...
					
					
					$uri .= '?'.$name.'='.$value;
					
					
				}else{
					$uri .= '&'.$name.'='.$value;
				}
			}
		}
		parent::redirect($uri,$code);
	}
}
