<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * Errors messages.
 * 
 * Methods:
 * - set
 */
class App_Error
{
	/**
	 * Set errors messages.
	 *
	 * Get errors from session and merges with now errors
	 * and stores in session again.
	 * 
	 * @param mixed $errors errors messages: ORM_Validation_Exception | array[element][array[errors]] | array[element][error]
	 * 
	 * @return void
	 */
	public static function set($errors)
	{
		$e = Session::instance()->get('errors');
		if(is_object($errors)){
			if(get_class($errors)==='ORM_Validation_Exception'){
				foreach($errors->errors() as $k1 => $v1){
					if($k1==='_external'){
						foreach($v1 as $k2 => $v2){
							if($k2==='repeat_password'){
								$e['password'][] = 'matches';
							}else{
								$e[$k2][] = $v2[0];
							}
						}
					}else{
						if($v1[0]==='email') $v1[0] = 'not_valid';
						$e[$k1][] = $v1[0];
					}
				}
			}
		}else if(is_array($errors)){
			if(is_array($errors['errors'])){
				$e[$errors['element']] = $errors['errors'];
			}else{
				$e[$errors['element']][] = $errors['errors'];
			}
		}
		Session::instance()->set('errors',$e);
	}
}
