<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * Messages.
 * 
 * Methods:
 * - set
 */
class App_Message
{
	/**
	 * Set message.
	 * 
	 * @param string $message message
	 * @param array $parameters message parameters
	 * 
	 * @return void
	 */
	public static function set($message, $parameters = array())
	{
		$messages = Session::instance()->get('messages');
		$msg['message'] = $message;
		$msg['parameters'] = $parameters;
		$messages[] = $msg;
		Session::instance()->set('messages',$messages);
	}
}
