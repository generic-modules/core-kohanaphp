<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * Bootstrap framework helpers class.
 *
 * Methods:
 * - glyphicon
 */
class App_Bootstrap
{
	/**
	 * Generate glyphicon.
	 * 
	 * Generates glyphicons based on name.
	 * @see https://www.w3schools.com/bootstrap/bootstrap_ref_comp_glyphs.asp
	 * 
	 * @param string $name glyphicon name - only unique pars like "pencil", "star"...
	 * 
	 * @return html string
	 */
	public static function glyphicon($name)
	{
		return '<span class="glyphicon glyphicon-'.$name.'"></span>';
	}
}