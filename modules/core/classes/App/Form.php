<?php defined('SYSPATH') OR die('No direct script access.');
/**
 * @inherit
 */
class App_Form extends Kohana_Form
{
	
	public static $arrays = array();
	
	
	public static $hidden_values = array();
	
	/**
	 * Add hidden values to form.
	 *
	 * @param string $form form name
	 * @param array $values hidden values
	 * @param string $type values type, default data
	 */
	public static function addHidden($form,$values,$type = 'data')
	{
		foreach($values as $name => $value){
			self::$hidden_values[$form][$type][$name] = $value;
		}
	}

	/**
	 * Creates a button form input. Note that the body of a button is NOT escaped,
	 * to allow images and other HTML to be used.
	 *
	 *     echo Form::button('save', 'Save Profile', array('type' => 'submit'));
	 *
	 * @param   string  $name       input name and id
	 * @param   string  $body       input value
	 * @param   array   $attributes html attributes
	 * @return  string
	 * @uses    HTML::attributes
	 */
	public static function button($name, $body, array $attributes = NULL)
	{
		$id = $name;
		$id = str_replace('[', '_', $id);
		$id = str_replace(']', '', $id);
		$attributes['id'] = $id;
		if(isset($attributes['disabled'])){
			$attributes['name'] = 'disabled';
		}else{
			$attributes['name'] = $name;
		}
		return '<button'.HTML::attributes($attributes).'>'.$body.'</button>';
	}
	
	/**
	 * Create filter input.
	 *
	 * @param string $filter identifier
	 * @param mixed $values filter value or values
	 * @param array $attributes filter attributes
	 *
	 * @return string
	 */
	public static function filter($identifier,$values = null,$attributes = array())
	{
		$html = '';
		if(!isset($values)) $values= null;
		if(is_array($identifier)){
			$component = $identifier[0];
			$name = $identifier[1];
			$id = $name;
		}
		if($component) {
			$component = rtrim($component,'.');
			$prefix = str_replace('_','.',$component);
			$component= str_replace('.','_',$component);
		}
		$type = Kohana::$config->load('forms.components.'.$component.'.filters.'.$name.'.type');
		if(!$type){
			$type = 'input';
		}
		$css = Kohana::$config->load('forms.components.'.$component.'.filters.'.$name.'.css');
		switch($type){
			case 'select':
				$attributes['input']['class'] = 'filter '.$css;
				if($values){
					if(is_array($values)){
						if(isset($values[1])) $selected = $values[1];
						if(isset($values[0])) $values = $values[0];
						if(!isset($values)) $values = null;
						if(!isset($selected)) $selected = null;
					}
				}else{
					$values = null;
					$selected = null;
				}
				if(!isset($values[0])){
					$values = array_merge(['0'=>__('user.global.select.choose')],$values);
				}
				$html .= self::select('filters['.$name.']',$values,$selected,$attributes['input']);
				break;
			default:
				$value = $values;
				$html .= '<input id="filters_'.$id.'" name="filters['.$name.']" class="filter '.$css.'" value="'.$value.'"/>';
		}
		return $html;
	}
	
	/**
	 * Creates a form input. If no type is specified, a "text" type input will
	 * be returned.
	 *
	 *     echo Form::input('username', $username);
	 *
	 * @param   string  $name       input name and id
	 * @param   string  $value      input value
	 * @param   array   $attributes html attributes
	 * @return  string
	 * @uses    HTML::attributes
	 */
	public static function input($name, $value = NULL, array $attributes = NULL)
	{
		if(isset($attributes['id'])){
			$id = $attributes['id'];
		}else{
			$id = $name;
			$id = str_replace('[', '_', $id);
			$id = str_replace(']', '', $id);
		}
		$attributes['id'] = $id;
		if(isset($attributes['disabled'])){
			$attributes['name'] = 'disabled';
		}else{
			$attributes['name'] = $name;
		}
		$attributes['value'] = $value;
		if ( ! isset($attributes['type'])){
			$attributes['type'] = 'text';
		}
		return '<input'.HTML::attributes($attributes).' />';
	}
	
	/**
	 * Generates an opening HTML form tag.
	 *
	 *     // Form will submit back to the current page using POST
	 *     echo Form::open();
	 *
	 *     // Form will submit to 'search' using GET
	 *     echo Form::open('search', array('method' => 'get'));
	 *
	 *     // When "file" inputs are present, you must include the "enctype"
	 *     echo Form::open(NULL, array('enctype' => 'multipart/form-data'));
	 *
	 * @param mixed $parameters form name, name and action, defaults to the current request URI, or [Request] class to use
	 * @param array $attributes html attributes
	 * @return string
	 * @uses Request
	 * @uses URL::site
	 * @uses HTML::attributes
	 */
	public static function open($parameters = NULL, array $attributes = NULL)
	{
		if($parameters){
			if(is_array($parameters)){
				$identifier = $parameters[0];
				$action = $parameters[1];
			}else{
				$identifier = $parameters;
				$action = null;
			}
		}else{
			$action = null;
		}
		if(isset($attributes)){
			if(isset($attributes['class'])){
				$attributes['class'] .= ' form-horizontal';
			}else{
				$attributes['class'] = 'form-horizontal';
			}
		}else{
			$attributes['class'] = 'form-horizontal';
		}
		if(isset($identifier)) $attributes['id'] = $identifier;
		$html = parent::open($action,$attributes);
		if(isset($identifier)){
			isset(self::$hidden_values[$identifier]) ? $values = self::$hidden_values[$identifier] : $values = null;
			if($values){
				foreach($values as $_type => $_values){
					foreach($_values as $_name => $_value){
						$html .= '<input type="hidden" id="'.$_type.'_'.$_name.'" name="'.$_type.'['.$_name.']" value="'.$_value.'"/>';
					}
				}
			}
		}
		return $html;
	}
	
	/**
	 * Create standard form row.
	 *
	 * Single row has label, input and, optionally, info helper.
	 *
	 * @param mixed $input input identifier, required to generate id, name and other parameters
	 * @param mixed $values input values
	 * @param string $attributes row attributes
	 *
	 * @uses Kohana::$config::forms.row.info_helper.image_path
	 *
	 * @return HTML form row string
	 */
	public static function row($input, $values = null,$attributes = null)
	{
		$label_separator = Kohana::$config->load('forms.row.label.separator');
		if(!$label_separator) $label_separator = '';
		$helper_image_path = Kohana::$config->load('forms.row.info_helper.image_path');
		$is_array = false;
		$array_index = null;
		if(is_array($input)){
			$component = $input[0];
			if(isset($input[2])){
				$is_array = true;
				$path = $input[2];
				if(Core::arr(self::$arrays,$path)){
					$_component = Core::arr(self::$arrays,$path);
					if(!isset($_component['components'])) $_component['components'] = array();
					if(in_array($input[1],$_component['components'])){
						$_component['counter']++;
						$_component['components'] = array();
					}
					$_component['components'][] = $input[1];
				}else{
					$_component['counter'] = 1;
					$_component['components'] = array();
					$_component['components'][] = $input[1];
				}
				Core::arr(self::$arrays,$path,$_component);
			}
			$input= $input[1];
		}else{
			$component = null;
		}
		$name = explode('.',$input);
		$name = implode('][',$name);
		if(isset($attributes['data_prefix'])){
			$data_prefix = $attributes['data_prefix'];
		}else{
			$data_prefix = '';
		}
		if($is_array){
			$_path = explode('.',$path);
			$array_index = $_path[0];
			$name = 'data'.$data_prefix.'['.$array_index.']['.$_component["counter"].']['.$name.']';
		}else{
			$name = 'data'.$data_prefix.'['.$name.']';
		}
		if($data_prefix){
			$id_prefix = $data_prefix;
			$id_prefix = str_replace('][','_',$id_prefix);
			$id_prefix = str_replace(']','_',$id_prefix);
			$id_prefix = str_replace('[','',$id_prefix);
		}else{
			$id_prefix = '';
		}
		$id = str_replace('.','_',$input);
		if($is_array){
			$id = 'data_'.$id_prefix.$array_index.'_'.$_component['counter'].'_'.$id;
		}else{
			$id = 'data_'.$id;
		}
		$input= str_replace('.','_',$input);
		if($component) {
			$component = rtrim($component,'.');
			$prefix = str_replace('_','.',$component);
			$component= str_replace('.','_',$component);
		}
		if(substr($prefix,-1) != '.') $prefix .= '.';
		$label = $input.'.label';
		if($prefix) $label = $prefix.$label;
		$info_helper = $input.'.info_helper';
		if($prefix) $info_helper= $prefix.$info_helper;
		if(isset($component)) {
			$input = Kohana::$config->load('forms.components.'.$component.'.inputs.'.$input);
		}else{
			$input = Kohana::$config->load('forms.inputs.'.$input);
		}
		if(!$input) $input = array();
		if(!isset($input['type'])) $input['type'] = 'input';
		if(isset($attributes['input']['label'])) $input['label'] = $attributes['input']['label'];
		if(!isset($input['label'])) $input['label'] = 'default';
		if(!isset($input['attributes'])) $input['attributes'] = array();
		if(!isset($input['attributes']['input'])) $input['attributes']['input'] = array();
		if(!isset($input['attributes']['input']['id'])) $input['attributes']['input']['id'] = $id;
		if(!isset($input['enabled'])) $input['enabled']= false;
		if(!$input['enabled']) return '';
		if(isset($input['separator'])){
			$label_separator = $input['separator'];
		}
		if(isset($attributes)){
			$input['attributes'] = array_merge($input['attributes'],$attributes);
		}
		if(isset($input['attributes']['input']['class'])){
			$input['attributes']['input']['class'] .= ' form-control';
		}else{
			$input['attributes']['input']['class'] = 'form-control';
		}
		
		
		
		switch($input['type']){
			case 'checkbox':
				$col['label'] = 'col-xs-10';
				$col['input'] = 'col-xs-2';
				break;
			default:
				if(isset($attributes['col']['label'])){
					$col['label'] = $attributes['col']['label'];
				}else{
					if(isset($input['attributes']['col']['label'])){
						$col['label'] = $input['attributes']['col']['label'];
					}else{
						$col['label'] = 'col-xs-12 col-sm-4';
					}
				}
				if(isset($attributes['col']['input'])){
					$col['input'] = $attributes['col']['input'];
				}else{
					if($input['label'] == 'placeholder'){
						if(isset($input['attributes']['col']['input'])){
							$col['input'] = $input['attributes']['col']['input'];
						}else{
							$col['input'] = 'col-xs-12';
						}
					}else{
						if(isset($input['attributes']['col']['input'])){
							$col['input'] = $input['attributes']['col']['input'];
						}else{
							$col['input'] = 'col-xs-12 col-sm-8';
						}
					}
				}
		}
		
		
		
		
		
		
		$row = '';
		if(isset($input['attributes']['row']['type'])){
			$row_type = $input['attributes']['row']['type'];
		}else{
			$row_type = 'standard';
		}
		if(isset($attributes['row'])){
			if(isset($attributes['row']['id'])){
				$id = 'id="'.$attributes['row']['id'].'"';
			}
			if(isset($attributes['row']['class'])){
				$row_class = ' '.$attributes['row']['class'];
				unset($attributes['row']['class']);
			}else{
				$row_class = '';
			}
			if(count($attributes['row'])){
				foreach($attributes['row'] as $_name => $attribute){
					$row_attributes .= ' '.$_name.'='.'"'.$attribute.'"';
				}
			}else{
				$row_attributes = '';
			}
		}else{
			$row_attributes = '';
			$row_class = '';
		}
		if($row_type != 'no-wrappers'){
			$row .= '<div '.$id.' class="row form-group'.$row_class.'"'.$row_attributes.'>';
		}
		if($input['label']){
			$label_attributes['class'] = 'control-label';
			switch($input['label']){
				case 'placeholder':
					$input['attributes']['input']['placeholder'] = __($label);
					break;
				default:
					if($row_type != 'no-wrappers'){
						$row .= '<div class="'.$col['label'].'">';
						$row .= self::label($name,__($label),$label_attributes).$label_separator;
						$row .= '</div>';
					}else{
						$input['attributes']['input']['placeholder'] = __($label);
					}
			}
		}
		if(Kohana::$config->load('forms.row.info_helper.display')){
			$row .= '<div class="info_helper">'.HTML::image($helper_image_path,array('title'=>__($helper))).'</div>';
		}
		switch($input['type']){
			case 'plain_text':
				if(is_array($values)){
					if($values[0] == ''){
						$value = $values[1];
					}else{
						$value = $values[0];
					}
				}else{
					$value = $values;
				}
				$row .= $value;
				break;
			case 'select':
				if(!isset($input['choose'])) $input['choose'] = __('user.global.select.choose');
				if($values){
					if(is_array($values)){
						if(isset($values[1])) $selected = $values[1];
						if(isset($values[0])) $values = $values[0];
						if(!isset($values)) $values = null;
						if(!isset($selected)) $selected = null;
					}
				}else{
					$values = null;
					$selected = null;
				}
				if(!isset($values[0])){
					if($input['choose']) $values = array_merge(['0'=>$input['choose']],$values);
				}
				if(is_array($selected)){
					if(isset($input['attributes']['input']['class'])){
						$input['attributes']['input']['class'] += ' multiselect';
					}else{
						$input['attributes']['input']['class'] = 'multiselect';
					}
				}
				if($row_type != 'no-wrappers'){
					$row .= '<div class="'.$col['input'].'">';
				}
				$row .= self::select($name,$values,$selected,$input['attributes']['input']);
				if($row_type != 'no-wrappers'){
					$row .= '</div>';
				}
				break;
			case 'checkbox':
				if(is_array($values)){
					if(isset($values['checked'])) $checked = $values['checked'];
					if(isset($values['value'])) $value = $values['value'];
				}else{
					$value = $values;
					if($value > 0){
						$checked = true;
					}else{
						$checked = false;
					}
				}
				if(isset($input['attributes']['input']['class'])){
					$input['attributes']['input']['class'] .= ' pull-left';
				}else{
					$input['attributes']['input']['class'] = 'pull-left';
				}
				if($row_type != 'no-wrappers'){
					$row .= '<div class="'.$col['input'].'">';
				}
				$row .= self::$input['type']($name,$value,$checked,$input['attributes']['input']);
				if($row_type != 'no-wrappers'){
					$row .= '</div>';
				}
				break;
			default:
				if($row_type != 'no-wrappers'){
					$row .= '<div class="'.$col['input'].'">';
				}
				$row .= self::$input['type']($name,$values,$input['attributes']['input']);
				if($row_type != 'no-wrappers'){
					$row .= '</div>';
				}
		}
		if(isset($attributes['suffix'])){
			$row .= $attributes['suffix'];
		}
		if($row_type != 'no-wrappers'){
			$row .= '</div>';
		}
		return $row;
	}
	
	/**
	 * Creates a textarea form input.
	 *
	 *     echo Form::textarea('about', $about);
	 *
	 * @param   string  $name           textarea name and id
	 * @param   string  $body           textarea body
	 * @param   array   $attributes     html attributes
	 * @param   boolean $double_encode  encode existing HTML characters
	 * @return  string
	 * @uses    HTML::attributes
	 * @uses    HTML::chars
	 */
	public static function textarea($name, $body = '', array $attributes = NULL, $double_encode = TRUE)
	{
		$id = $name;
		$id = str_replace('[', '_', $id);
		$id = str_replace(']', '', $id);
		$attributes['id'] = $id;
		if(isset($attributes['disabled'])){
			$attributes['name'] = 'disabled';
		}else{
			$attributes['name'] = $name;
		}
		$attributes += array();
		return '<textarea'.HTML::attributes($attributes).'>'.HTML::chars($body, $double_encode).'</textarea>';
	}
	
	/**
	 * Creates a select form input.
	 *
	 *     echo Form::select('country', $countries, $country);
	 *
	 * [!!] Support for multiple selected options was added in v3.0.7.
	 *
	 * @param   string  $name       input name and id
	 * @param   array   $options    available options
	 * @param   mixed   $selected   selected option string, or an array of selected options
	 * @param   array   $attributes html attributes
	 * @return  string
	 * @uses    HTML::attributes
	 */
	public static function select($name, array $options = NULL, $selected = NULL, array $attributes = NULL)
	{
		if(is_array($selected)){
			$multiselect = true;
		}else{
			$multiselect = false;
		}
		$id = $name;
		$id = str_replace('[]', '', $id);
		$id = str_replace('[', '_', $id);
		$id = str_replace(']', '', $id);
		$attributes['id'] = $id;
		if(isset($attributes['disabled'])){
			$attributes['name'] = 'disabled';
		}else{
			$attributes['name'] = $name;
		}
		if($multiselect){
			$attributes['multiple'] = 'multiple';
			if(isset($attributes['class'])){
				$attributes['class'] = $attributes['class'].' multiselect';
			}else{
				$attributes['class'] = 'multiselect';
			}
		}
		if (!is_array($selected)){
			if ($selected === NULL){
				$selected = array();
			}else{
				$selected = array( (string) $selected);
			}
		}
		if (empty($options)){
			$options = '';
		}else{
			foreach ($options as $value => $name){
				if (is_array($name)){
					$group = array('label' => $value);
					$_options = array();
					foreach ($name as $_value => $_name){
						$_value = (string) $_value;
						$option = array('value' => $_value);
						if (in_array($_value, $selected)){
							$option[] = 'selected';
						}
						$_options[] = '<option'.HTML::attributes($option).'>'.HTML::chars($_name, FALSE).'</option>';
					}
					$_options = "\n".implode("\n", $_options)."\n";
					$options[$value] = '<optgroup'.HTML::attributes($group).'>'.$_options.'</optgroup>';
				}else{
					$value = (string) $value;
					$option = array('value' => $value);
					if (in_array($value, $selected)){
						$option[] = 'selected';
					}
					$options[$value] = '<option'.HTML::attributes($option).'>'.HTML::chars($name, FALSE).'</option>';
				}
			}
			$options = "\n".implode("\n", $options)."\n";
		}
		if($multiselect){
			return '<select'.HTML::attributes($attributes).'>'.$options.'</select>';
		}else{
			return '<div class="select"><select'.HTML::attributes($attributes).'>'.$options.'</select></div>';
		}
	}
	
	/**
	 * @inherit
	 */
	public static function submit($name, $value, array $attributes = NULL)
	{
		if(isset($attributes['class'])){
			$attributes['class'] += ' btn';
		}else{
			$attributes['class'] = 'btn';
		}
		return parent::submit($name,$value,$attributes);
	}
	
	/**
	 * Update component's counter
	 *
	 * @param string $name component's name
	 * @param integer $value update value
	 */
	public static function setCounter($name,$value)
	{
		Core::arr(self::$arrays,$name.'.counter',$value);
	}

	/**
	 * Get counter
	 * 
	 * @param string $name
	 * 
	 * @return int
	 */
	public static function getCounter($name)
	{
		$counter = Core::arr(self::$arrays,$name.'.counter');
		if($counter) return $counter;
		return 0;
	}
}
