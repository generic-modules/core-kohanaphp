<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * Debugging class.
 */
class App_Debug extends Kohana_Debug
{
	/**
	 * Init debug class
	 *
	 * @return void
	 */
	public static function init(){}

	/**
	 * Dump variable.
	 *
	 * @param mixed $var variable to dump
	 * @param bool $die if after debug should be die
	 * @param string $message die message
	 *
	 * @return void
	 */
	public static function vardump($var, $die = false, $message = '')
	{
		echo '<pre>';
		var_dump($var);
		echo '</pre>';
		if($die) die($message);
	}
}
