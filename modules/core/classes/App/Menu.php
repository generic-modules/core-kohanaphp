<?php defined('SYSPATH') or die('No direct script access.');

class App_Menu
{
	/**
	 * @var array
	 * 
	 * Styles for menu
	 */
	protected $menu_css = array();

	
	protected $name = null;
	
	/**
	 * @var array
	 *
	 * Styles for options
	 */
	protected $rows_css = array();
	
	/**
	 * @var array
	 * 
	 * Styles for options anchors
	 */
	protected $rows_anchor_css = array();
	
	/**
	 * @var string
	 * 
	 * Active group name
	 */
	protected $group = null;
	
	/**
	 * @var array
	 * 
	 * Menu options
	 */
	protected $options = array();
	
	/**
	 * Class constructor
	 * 
	 * @param string $name menu name
	 * @param array $options menu options
	 */
	public function __construct($name,$options = array())
	{
		$this->name = $name;
		if(isset($options['menu_css'])) $this->menu_css = $options['menu_css'];
		if(isset($options['rows_css'])) $this->rows_css = $options['rows_css'];
		if(isset($options['rows_anchor_css'])) $this->rows_anchor_css = $options['rows_anchor_css'];
	}
	
	/**
	 * Set option active.
	 *
	 * @param string $name
	 *
	 * @return void
	 */
	public function activate($name)
	{
		if(is_array($name)){
			$this->options[$name[0]][$name[1]]['is_active'] = true;
		}else{
			$this->options[$name]['is_active'] = true;
		}
	}
	
	/**
	 * Add menu option
	 * 
	 * @param mixed $name option unique name or group name and option name
	 * @param array $definition option definition
	 * - url
	 * - label
	 * - required role (optional)
	 * @param array $options additional options
	 * 
	 * @return void
	 */
	public function add($name,$definition,$options = array())
	{
		if(is_array($name)){
			$group = $name[0];
			$name = $name[1];
			if(!Kohana::$config->load('menu.'.$this->name.'.options.'.$group.'.'.$name.'.enabled')) return;
		}else{
			if(!Kohana::$config->load('menu.'.$this->name.'.options.'.$name.'.enabled')) return;
			$group = null;
		}
		$url = $definition[0];
		$label = $definition[1];
		isset($definition[2]) ? $role = $definition[2] : $role = '';
		$option['url'] = $url;
		$option['label'] = $label;
		$option['role'] = $role;
		$option['is_active'] = false;
		if($group){
			$this->options[$group][$name] = $option;
		}else{
			$this->options[$name] = $option;
		}
	}
	
	/**
	 * Generate menu html
	 * 
	 * @return string generated menu
	 */
	public function generate()
	{
		$menu_id = 'id="'.$this->name.'_menu"';
		if(count($this->menu_css) > 0){
			$menu_css = ' class ="'.implode(' ',$this->menu_css).'"';
		}else{
			$menu_css = '';
		}
		$this->group ? $options = isset($this->options[$this->group]) ? $this->options[$this->group] : $options = $this->options : $options = $this->options;
		if(count($options) > 0){
			$menu = '<ul '.$menu_id.$menu_css.'>';
			foreach($options as $name => $option){
				if(isset($option['role'])){
					if(($option['role'] and Auth::instance()->logged_in($option['role'])) or (!$option['role'])){
						if(is_array($option)){
							$css = $this->rows_css;
							if($option['is_active']) $css[] = 'current';
							if(count($css) > 0){
								$css = ' class ="'.implode(' ',$css).'"';
							}else{
								$css = '';
							}
							$menu .= '<li'.$css.'>';
							$css = $this->rows_anchor_css;
							if(count($css) > 0){
								$css = ' class ="'.implode(' ',$css).'"';
							}else{
								$css = '';
							}
							$menu .= '<a '.$css.'href="'.$option['url'].'">'.$option['label'].'</a>';
							$menu .= '</li>';
						}
					}
				}
			}
			$menu .= '</ul>';
			return $menu;
		}
		return null;
	}
	
	/**
	 * Set options order.
	 * 
	 * @param array $order order
	 * @param string $group group name
	 * 
	 * @return void
	 */
	public function setOrder($order, $group = null)
	{
		$group ? $options = $this->options[$group] : $options = $this->options;
		$_options = array();
		foreach($order as $option){
			$_options[] = $options[$option];
		}
		if($group){
			$this->options[$group] = $_options;
		}else{
			$this->options = $_options;
		}
	}
	
	/**
	 * Select group name.
	 * 
	 * @param string $name group name
	 * 
	 * @return void
	 */
	public function selectGroup($name)
	{
		$this->group = $name;
	}
}
