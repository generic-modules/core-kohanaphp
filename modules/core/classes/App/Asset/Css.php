<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * CSS class.
 * 
 * Defines single CSS file.
 */
class App_Asset_Css
{
	/**
	 * @var string $name
	 * 
	 * Css file name.
	 */
	public $name;
	
	/**
	 * @var string $path
	 * 
	 * Path to Css file.
	 */
	public $path;
	
	/**
	 * Class constructor.
	 * 
	 * @param string $path path to CSS file
	 * @param string $name name of CSS file
	 * 
	 * @return void
	 */
	public function __construct($path, $name)
	{
		$this->path = $path;
		$this->name = $name;
	}
}
