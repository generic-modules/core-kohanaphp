<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * Parameters for frontend (HTML/Javascript)
 *
 * Methods:
 * - count
 * - get
 * - set
 */
class App_Asset_Parameter
{
	/**
	 * @var array $parameters
	 *
	 * Parameters array.
	 */
	protected static $parameters = array();

	/**
	 * Count parameters.
	 *
	 * @return int parameters number
	 */
	public static function count()
	{
		return count(self::$parameters);
	}

	/**
	 * Get parameters.
	 *
	 * @param string $parameter name
	 *
	 * @return array parameters array
	 */
	public static function get($parameter = null)
	{
		if($parameter){
			return self::$parameters[$parameter];
		}else{
			return self::$parameters;
		}
	}

	/**
	 * Set parameters.
	 *
	 * @param array[array[name=>value]] $parameters parameters array
	 *
	 * @return void
	 */
	public static function set($parameters)
	{
		foreach($parameters as $parameter){
			foreach($parameter as $name => $value){
				self::$parameters[] = array('name'=>$name,'value'=>$value);
			}
		}
	}
}
