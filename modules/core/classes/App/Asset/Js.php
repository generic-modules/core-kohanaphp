<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * JS class.
 * 
 * Defines single JS file.
 */
class App_Asset_Js
{
	/**
	 * @var string $name
	 * 
	 * Css file name.
	 */
	public $name;
	
	/**
	 * @var string $path
	 * 
	 * Path to Css file.
	 */
	public $path;
	
	/**
	 * Class constructor.
	 * 
	 * @param string $path path to JS file
	 * @param string $name name of JS file
	 * 
	 * @return void
	 */
	public function __construct($path, $name)
	{
		$this->path = $path;
		$this->name = $name;
	}
}
