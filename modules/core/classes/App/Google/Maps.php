<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * Google Maps.
 */
class App_Google_Maps
{
	
	public static function getGeocode($address)
	{
		$street_name = $address['street_name'];
		$street_number = $address['street_number'];
		$flat_number = $address['flat_number'];
		$zip_code = $address['zip_code'];
		$place = $address['place'];
		$address = 'ul. '.$street_name.' '.$street_number;
		if($flat_number) $address .= '/'.$flat_number;
		$address .= ', '.$zip_code.' '.$place;
		$address .= ', Polska';
		$prepAddr = str_replace(' ','+',$address);
		$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&components=postal_code:'.$zip_code);
		$output= json_decode($geocode);
		$latitude = $output->results[0]->geometry->location->lat;
		$latitude = str_replace(',','.',$latitude);
		$longitude = $output->results[0]->geometry->location->lng;
		$longitude= str_replace(',','.',$longitude);
		return ['lat'=>$latitude,'lng'=>$longitude];
	}
}
