<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * Layouts class.
 *
 * Layouts are defined for particular template.
 * All layouts are stored in directory "layouts" in template directory.
 *
 * For layouts are defined placeholders that are separate views.
 */
class App_Template_Layout extends View
{
	/**
	 * @var array $assets
	 * 
	 * Array of objects class Assets.
	 */
	private $assets;
	
	/**
	 * Class constructor.
	 *
	 * Set class parameters
	 * If not specified, default values are used.
	 *
	 * @param string $templateName name of template to witch layout is assigned
	 * @param string $name layout's name
	 *
	 * @return void
	 */
	public function __construct($templateName = 'default', $name = 'default', $path = null)
	{
		parent::__construct('templates/'.$templateName.'/layouts/'.$name);
		$this->templateName = $templateName;
		$this->name = $name;
		if($path){
			$this->path = $path;
		}else{
			$this->path = 'templates/'.$templateName.'/layouts/'.$name.'/';
		}
		$this->placeholders = array();
		$this->setName($name);
	}
	
	/**
	 * Set layout name.
	 *
	 * @param string $name layout name
	 *
	 * @return void
	 */
	public function setName($name)
	{
		$this->name = $name;
	}
	
	/**
	 * Get layout name.
	 *
	 * @return string layout name
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set template name to witch is assigned layout.
	 *
	 * @param $name template name
	 *
	 * @return void
	 */
	public function setTemplateName($name)
	{
		$this->templateName = $name;
	}
	
	/**
	 * Get template name to witch is assigned layout.
	 *
	 * @return $this->templateName
	 */
	public function getTemplateName()
	{
		return $this->templateName;
	}
		
	/**
	 * Set content view.
	 *
	 * Content view is special type of placeholder.
	 * It is stored in property $content.
	 * 
	 * @param string $path relative path to content view directory
	 *
	 * @return $this->content
	 */
	public function setContent($path)
	{
		$this->content = new Template_Layout_Placeholder($path);
		$this->content->path = $path.'/';
		$this->content->prefix = str_replace('/','.',$this->content->path);
		return $this->content;
	}

	/**
	 * Get content view.
	 *
	 * @return Kohana::View content view
	 */
	public function getContent()
	{
		return $this->content;
	}
			
	/**
	 * Add placeholder to layout.
	 *
	 * If provided name of existing placeholder,
	 * this placeholder is replaced with the new one.
	 *
	 * @param string $name nazwa placeholdera
	 * @param bool $type placeholder type (layout|global), defaults to 'layout'
	 *
	 * @return void
	 */
	public function addPlaceholder($name, $type = 'layout')
	{
		$path = $this->path.'placeholders/'.Core::removeInitialSlash($name);
		switch($type){
			case 'layout': $this->placeholders[$name] = new Template_Layout_Placeholder($this->path.'placeholders/'.$name,$this->path.'placeholders/'.$name); break;
			case 'global': $this->placeholders[$name] = new Template_Layout_Placeholder('placeholders/'); break;
		}
	}
	
	/**
	 * Get selected placeholder
	 *
	 * @param string $name placeholder name
	 *
	 * @return Template_Layout_Placeholder placeholder
	 */
	public function getPlaceholder($name)
	{
		return $this->placeholders[$name];
	}

	/**
	 * Remove selected placeholder
	 *
	 * @param string $name placeholder name
	 *
	 * @return void
	 */
	public function removePlaceholder($name)
	{
		unset($this->placeholders[$name]);
	}
}
