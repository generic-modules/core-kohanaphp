<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * Placeholder class.
 *
 * Placeholders are assigned to layouts.
 */
class App_Template_Layout_Placeholder extends View
{
	/**
	 * @var string $name
	 *
	 * Placeholder's name.
	 */
	public $name;

	/**
	 * @var string $path
	 *
	 * Relative path to placeholder's file.
	 */
	public $path;
	
	/**
	 * @var string $prefix
	 * 
	 * Prefix for translation files.
	 */
	public $prefix;
	
	/**
	 * Class constructor.
	 *
	 * Set class parameters:
	 * - path
	 * - prefix
	 *
	 * If parameters are not set, default values are used.
	 *
	 * @param string $name placeholder view name
	 *
	 * @return void
	 */
	public function __construct($path)
	{
		$this->placeholders = array();
		$this->name = Core::getFilenameFromPath($name);
		$this->path = $this->setPath($path);
		$this->prefix = $this->setPrefix($this->path);
		$data['prefix'] = $this->prefix;
		parent::__construct($path,$data);
	}
	
	
	/**
	 * Add placeholder to layout.
	 *
	 * If provided name of existing placeholder,
	 * this placeholder is replaced with the new one.
	 *
	 * @param string $name nazwa placeholdera
	 *
	 * @return void
	 */
	public function addPlaceholder($name)
	{
		$this->placeholders[$name] = new Template_Layout_Placeholder($this->path.Core::removeInitialSlash($name));
	}

	/**
	 * Ustawia nazwę placeholdera.
	 *
	 * @param string $name nazwa placeholdera
	 *
	 * @return void
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * Pobiera nazwę placeholdera.
	 *
	 * @return string nazwa placeholdera
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Pobiera ścieżkę do placeholdera.
	 *
	 * @return string ścieżka do placeholdera
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * Pobiera ścieżkę i nazwę pliku
	 *
	 * @return string ścieżka i nazwa pliku
	 */
	public function getFile()
	{
		return $this->_file;
	}
	
	/**
	 * Set proper path string.
	 * 
	 * @param string $path
	 * 
	 * @return string
	 */
	protected function setPath($path)
	{
		$path = $path.'/';
		return $path;
	}
	
	/**
	 * Set prefix by path
	 * 
	 * @param string $path
	 * 
	 * @return $string
	 */
	protected function setPrefix($path)
	{
		$prefix = str_replace('/','.',$path);
		$prefix= str_replace('templates.','',$prefix);
		$prefix= str_replace('layouts.default.placeholders.content.','',$prefix);
		return $prefix;
	}
}
