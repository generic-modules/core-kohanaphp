<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * Application core methods.
 *
 * Methods:
 * - float
 */
class App_Core
{
	/**
	 * Get and/or set array key value.
	 */
	public static function arr(&$arr, $path, $value = null, $separator = '.')
	{
		$keys = explode($separator, $path);
		foreach ($keys as $key) {
			if(!isset($arr[$key])) $arr[$key] = array();
			$arr = &$arr[$key];
		}
		if($value) $arr = $value;
		return $arr;
	}
	
	/**
	 * Add trailing slash to string if not exists.
	 *
	 * @param string $string string to change
	 *
	 * @return string with trailing slash
	 */
	public static function addTrailingSlash($string)
	{
		return rtrim($string, '/') . '/';
	}
	
	/**
	 * Converts bool value to number or number to bool.
	 *
	 * @param mixed $value
	 */
	public static function bool($value)
	{
		if(is_string($value) and (strtoupper($value)=='TRUE') or strtoupper($value)=='FALSE'){
			if(strtoupper($value)==strtoupper('TRUE')){
				return 1;
			}else{
				return 0;
			}
		}else{
			if($value==1){
				return 'true';
			}else{
				return 'false';
			}
		}
		die('Parameter value must be one of: true, false, 0, 1');
	}

	/**
	 * Find file HMVC style.
	 * 
	 * Search for file in modules first (with modules hierarchy).
	 * If not found, search in application path.
	 * 
	 * @param string $path to file
	 * 
	 * @return string path to file, false otherwise
	 */
	public static function findFile($path)
	{
		$modules = Kohana::$config->load('modules');
		$_modules = array();foreach($modules as $module)$_modules[] = $module;
		foreach(array_reverse($_modules) as $module){
			if(isset($module['type'])){
				if($module['type']=='magnetar'){
					if(file_exists(DOCROOT.'modules/'.$module['directory'].'/'.$path)){
						return URL::base().'modules/'.$module['directory'].'/'.$path;
					}
				}
			}
		}
		if(file_exists(DOCROOT.'application/'.$path)){
			return URL::base().'application/'.$path;
		}
	}
	
	/**
	 * Convert variable to float value.
	 *
	 * Conversion sets precision (default 2)
	 * and change ',' to '.'.
	 *
	 * @param mixed $number number to format
	 * @param int precission precission (default 2)
	 *
	 * @return float converted number
	 */
	public static function float($number, $precission = 2)
	{
		if($number)
			if(is_float($number)) return str_replace(',','',number_format($number,$precission));
			elseif(is_string($number)){
				$number = str_replace(',','.',$number);
				if(preg_match('/^[0-9]{0,}\.[0-9]{0,}$/',$number) or preg_match('/^[0-9]{0,}$/',$number)){
					$number = number_format($number,$precission,'.','');
					$number = str_replace(',','.',$number);
				}
				return $number;
			}
			else return $number;
		else return 0.00;
	}

	/**
	 * Format integer value as hour.
	 *
	 * @param int $number number to format
	 * @param string $format 12h or 24h
	 *
	 * @return string formated hour string
	 */
	public static function formatIntAsHour($number, $format = '24h')
	{
		$hour = $number.':00';
		if($number < 10) $hour = '0'.$hour;
		return $hour;
	}

	/**
	 * Get address from string.
	 * 
	 * @param string $string string with address
	 * 
	 * @return mixed return type - string/array, default array
	 */
	public static function getAddress($string,$return = 'array')
	{
		$start = strpos($string,'ul.');
		if($start) $start = $start + 3;
		if(!$start){
			$characters = str_split($string);
			$start = 0;
			$length = 0;
			foreach($characters as $character){
				if((int)$character == 0){
					$length++;
				}else{
					break;
				}
			}
			if($length < strlen($string)){
				$_start = $start;
				while($_start < $length){
					$_start = strpos($string,'<p>',$start+1);
					if(!$_start) break;
					if($_start < $length) $start = $_start;
				}
				if($start){
					$start = $start + 2;
				}else{
					while($_start < $length){
						$_start = strpos($string,'.',$start+1);
						if(!$_start) break;
						if($_start < $length) $start = $_start;
					}
				}
				if($start) $start++;
			}
		}
		$string = substr($string,$start);
		$string = ltrim($string);
		$characters = str_split($string);
		$start = 0;
		$length = 0;
		foreach($characters as $character){
			if((int)$character == 0){
				$length++;
			}else{
				break;
			}
		}
		$street_name = substr($string,$start,$length);
		$street_name = str_replace('<p>','',$street_name);
		$street_name = str_replace('</p>','',$street_name);
		$street_name = str_replace(',','',$street_name);
		$street_name = trim($street_name);
		$start = $length;
		$string = substr($string,$start);
		$start = 0;
		$length= strpos($string,'/');
		if($length and ($length <= 4 and ($length < strpos($string,'</p>') or !strpos($string,'</p>')))){
			$flat_number = true;
		}else{
			$length = strpos($string,',');
			if(!$length or $length > 4) $length = strpos($string,'</p>');
			$flat_number = false;
		}
		$street_number = substr($string,$start,$length);
		$street_number = str_replace('<p>','',$street_number);
		$street_number = str_replace('</p>','',$street_number);
		$street_number = str_replace(',','',$street_number);
		$street_number = trim($street_number);
		$street_number= (int)$street_number;
		if($flat_number){
			$start = strpos($string,'/');			
			$string = substr($string,$start+1);
			$characters = str_split($string);
			$start = 0;
			$length = 0;
			foreach($characters as $character){
				if((int)$character > 0){
					$length++;
				}else{
					break;
				}
			}
			$flat_number = substr($string,$start,$length);
			$flat_number = str_replace('<p>','',$flat_number);
			$flat_number = str_replace('</p>','',$flat_number);
			$flat_number = str_replace(',','',$flat_number);
			$flat_number = trim($flat_number);
			$flat_number = (int)$flat_number;
			$start = $length;
			$string = substr($string,$start);
			$string = trim($string);
			$characters = str_split($string);
			$start = 0;
			foreach($characters as $character){
				if((int)$character == 0){
					$start++;
				}else{
					break;
				}
			}
			$string = substr($string,$start);
			$string = trim($string);
		}else{
			$start = $length;
			$string = substr($string,$start);
			$string = trim($string);
			$characters = str_split($string);
			$start = 0;
			foreach($characters as $character){
				if((int)$character == 0){
					$start++;
				}else{
					break;
				}
			}
			$string = substr($string,$start);
			$string = trim($string);
		}
		$start = 0;
		$length = 6;
		$zip_code = substr($string,$start,$length);
		$zip_code = str_replace('<p>','',$zip_code);
		$zip_code = str_replace('</p>','',$zip_code);
		$zip_code = str_replace(',','',$zip_code);
		$zip_code = trim($zip_code);
		$start = $length;		
		$string = substr($string,$start);
		$string = trim($string);
		$start = 0;
		$length= strpos($string,'</p>');
		if(!$length or $length > 20) $length = strpos($string,'.');
		
		if($length){
			$place = substr($string,$start,$length);
		}else{
			$place = substr($string,$start);
		}
		$place = str_replace('<p>','',$place);
		$place = str_replace('</p>','',$place);
		$place = str_replace('.','',$place);
		$place = trim($place);
		
		switch($return){
			case 'string': 
				$address = 'ul. '.$street_name.' '.$street_number;
				if($flat_number) $address .= '/'.$flat_number;
				$address .= ', '.$zip_code.' '.$place;
				$address .= ', Polska';
				return $address;
			case 'array':
				$address = array();
				$address['street_name'] = $street_name;
				$address['street_number'] = $street_number;
				$address['flat_number'] = $flat_number;
				$address['zip_code'] = $zip_code;
				$address['place'] = $place;
				$address['country'] = 'Polska';
				return $address;
			default: die('Illegal return value: '.$return);
		}
	}

	/**
	 * Change array key value.
	 *
	 * @param array $array
	 * @param mixed $old_key old key value
	 * @param mixed $new_key new key value
	 *
	 * @return array
	 */
	public static function chagneArrayKey($array,$old_key,$new_key)
	{
		$array[$new_key] = $array[$old_key];
		unset($array[$old_key]);
		return $array;
	}

	/**
	 * Get file name from path.
	 * 
	 * @param string $path
	 * 
	 * @return string
	 */
	public static function getFilenameFromPath($path)
	{
		$components = explode('/',$path);
		if(count($components) > 1){
			return $components[count($components)-1];
		}else{
			return $path;
		}
	}
	
	/**
	 * Remove initial slash from string.
	 *
	 * @param string $string string to change
	 *
	 * @return string without initial slash
	 */
	public static function removeInitialSlash($string)
	{
		return ltrim($string, '/');
	}

	/**
	 * Replace initial slash with string.
	 *
	 * @param string $string string to change
	 * @param string $replace string replacement
	 * @param int $count number of changes
	 *
	 * @return string with replaced slash
	 */
	public static function replaceInitialSlash($string,$replace = null,$count=1)
	{
		if(!$replace) $replace = self::addTrailingSlash(URL::base());
		return preg_replace('/\//',$replace,$string,1);
	}

	/**
	 * Replace initial slash in css with string.
	 *
	 * @param string $string string to change
	 * @param string $replace string replacement
	 * @param int $count number of changes
	 *
	 * @return string with replaced slash
	 */
	public static function replaceInitialSlashCss($string,$replace = null,$count=1)
	{
		if(!$replace) $replace = self::addTrailingSlash(URL::base());
		$replacement = preg_replace('/url\(\//','url('.$replace,$string);
		return $replacement;
	}


	/**
	 * Translate phrase.
	 *
	 * @param mixed $phrase phrase to translate
	 *
	 * @return translated phrase
	 */
	public static function translate($phrase)
	{
		if(is_array($phrase)){
			foreach($phrase as &$item){
				$item = __($item);
			}
			return $phrase;
		}else{
			return __($phrase);
		}
	}

	/**
	 * Convert number to pln.
	 *
	 * @param mixed $value value to convert
	 * @param bool $display_currency flag if currency should be displayed
	 *
	 * @return string converted value
	 */
	public static function pln($value, $display_currency = true)
	{
		if($value==null or $value=="") $value = '0,00';
		else{
			$value = number_format($value,2,',','.');
		}
		if($display_currency) $value .= ' zł';
		return $value;
	}
}
