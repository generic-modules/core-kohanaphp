<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * Asset class.
 *
 * The class is used to manage asset files:
 * - css
 * - js
 */
class App_Asset
{
	/**
	 * @var array $css
	 *
	 * Array of objects class Asset_Css
	 */
	private static $css = array();

	/**
	 * @var array $js
	 *
	 * Array of objects class Asset_Js
	 */
	private static $js = array();

	/**
	 * Add CSS file to assets list.
	 *
	 * @param string $module module name
	 * @param string $type css type: global | template | layout | placeholder | content
	 * @param string $path relative path to css file
	 * @param string $name name of css file
	 * @param string $template template name
	 * @param string $layout layout name
	 *
	 * @return void
	 */
	public function addCss($type, $name, $template = null, $layout = null)
	{
		$name .= '.css';
		switch($type){
			case 'global': $path = 'assets/css/';break;
			case 'lib': $path = 'assets/lib/';break;
			case 'template': $path = 'assets/css/templates/';break;
			case 'layout': $path = 'assets/css/templates/'.$template.'/layouts/';break;
			case 'placeholder':
				if($template){
					$path = 'assets/css/templates/'.$template.'/layouts/'.$layout.'/placeholders/';
				}else{
					$path = 'assets/css/placeholders/';
				}
				break;
			case 'content': $path = 'assets/css/templates/'.$template.'/layouts/'.$layout.'/placeholders/content/';break;
			default: die('Wrong type '.$type);
		}
		$modules = Kohana::$config->load('modules');
		$_modules = array();foreach($modules as $module)$_modules[] = $module;
		foreach(array_reverse($_modules) as $module){
			if(isset($module['type'])){
				if($module['type']=='magnetar'){
					if(file_exists(DOCROOT.'modules/'.$module['directory'].'/'.$path.$name)){
						self::$css[$type][] = new Asset_Css(URL::base().'modules/'.$module['directory'].'/'.$path,$name);
					}
				}
			}
		}
		if(file_exists(DOCROOT.'application/'.$path.$name)){
			$path = URL::base().'application/'.$path;
			self::$css[$type][] = new Asset_Css($path,$name);
		}
	}

	/**
	 * Add JS file to assets list.
	 *
	 * @param string $module module name
	 * @param string $type css type: global | lib | class | template | layout | placeholder | content
	 * @param string $path relative path to css file
	 * @param string $name name of css file
	 * @param string $template template name
	 * @param string $layout layout name
	 *
	 * @return void
	 */
	public function addJs($type, $name, $template = null, $layout = null)
	{
		$name .= '.js';
		switch($type){
			case 'dist': break;
			case 'global': $path = 'assets/js/';break;
			case 'lib': $path = 'assets/lib/';break;
			case 'class': $path = 'assets/js/classes/';break;
			case 'template': $path = 'assets/js/templates/';break;
			case 'layout': $path = 'assets/js/templates/'.$template.'/layouts/';break;
			case 'placeholder': $path = 'assets/js/templates/'.$template.'/layouts/'.$layout.'/placeholders/';break;
			case 'content': $path = 'assets/js/templates/'.$template.'/layouts/'.$layout.'/placeholders/content/';break;
			case 'custom': $path = 'assets/js/';break;
			default: die('Wrong type'.$type);
		}
		$modules = Kohana::$config->load('modules');
		$_modules = array();foreach($modules as $module)$_modules[] = $module;
		foreach(array_reverse($_modules) as $module){
			if(isset($module['type'])){
				if($module['type']=='magnetar'){
					if(file_exists(DOCROOT.'modules/'.$module['directory'].'/'.$path.$name)){
						self::$js[$type][] = new Asset_Js(URL::base().'modules/'.$module['directory'].'/'.$path,$name);
					}
				}
			}
		}
		if(file_exists(DOCROOT.'application/'.$path.$name)){
			$path = URL::base().'application/'.$path;
			self::$js[$type][] = new Asset_Js($path,$name);
		}
	}

	/**
	 * Clear assets
	 *
	 * Clear assets of type $type and/or name $name or all assets.
	 *
	 * @param string $type assets type
	 * @param string $name asset name
	 *
	 * @return void
	 */
	public function clear($type = null, $name = null)
	{
		if($type){
			if($name){
				unset(self::$css[$type][$name]);
				unset(self::$js[$type][$name]);
			}else{
				unset(self::$css[$type]);
				unset(self::$js[$type]);
			}
		}else{
			self::$css = null;
			self::$js = null;
		}
	}

	/**
	 * Generate assets
	 *
	 * @param string $type type of assets to generate
	 *
	 * @return array[Asset] assets array
	 */
	public static function generate($type = null)
	{
		$assets = '';
		if($type=='css' or !$type){
			if(isset(self::$css['lib'])){
				foreach(self::$css['lib'] as $css){
					$assets .= '<link rel="stylesheet" type="text/css" href="'.$css->path.$css->name.'">';
				}
			}
			if(isset(self::$css['global'])){
				foreach(self::$css['global'] as $css){
					$assets .= '<link rel="stylesheet" type="text/css" href="'.$css->path.$css->name.'">';
				}
			}
			if(isset(self::$css['template'])){
				foreach(self::$css['template'] as $css){
					$assets .= '<link rel="stylesheet" type="text/css" href="'.$css->path.$css->name.'">';
				}
			}
			if(isset(self::$css['layout'])){
				foreach(self::$css['layout'] as $css){
					$assets .= '<link rel="stylesheet" type="text/css" href="'.$css->path.$css->name.'">';
				}
			}
			if(isset(self::$css['placeholder'])){
				foreach(self::$css['placeholder'] as $css){
					$assets .= '<link rel="stylesheet" type="text/css" href="'.$css->path.$css->name.'">';
				}
			}
			if(isset(self::$css['content'])){
				foreach(self::$css['content'] as $css){
					$assets .= '<link rel="stylesheet" type="text/css" href="'.$css->path.$css->name.'">';
				}
			}

			if(Kohana::$config->load('css.dist')){
				$assets .= '<style>';
				foreach(Kohana::$config->load('css.dist') as $style){
					$selector = $style[0];
					$attribute = $style[1];
					$value = Core::replaceInitialSlashCss($style[2]);
					$assets .= $selector.' {'.$attribute.':'.$value.';} ';
				}
				$assets .= '</style>';
			}

		}
		if($type=='js' or !$type){
			if(isset(self::$js['lib'])){
				foreach(self::$js['lib'] as $js){
					$assets .= '<script src ="'.$js->path.$js->name.'"></script>';
				}
			}
			if(isset(self::$js['class'])){
				foreach(self::$js['class'] as $js){
					$assets .= '<script src ="'.$js->path.$js->name.'"></script>';
				}
			}
			if(isset(self::$js['global'])){
				foreach(self::$js['global'] as $js){
					$assets .= '<script src ="'.$js->path.$js->name.'"></script>';
				}
			}
			if(isset(self::$js['ajax'])){
				foreach(self::$js['ajax'] as $js){
					$assets .= '<script src ="'.$js->path.$js->name.'"></script>';
				}
			}
			if(isset(self::$js['template'])){
				foreach(self::$js['template'] as $js){
					$assets .= '<script src ="'.$js->path.$js->name.'"></script>';
				}
			}
			if(isset(self::$js['layout'])){
				foreach(self::$js['layout'] as $js){
					$assets .= '<script src ="'.$js->path.$js->name.'"></script>';
				}
			}
			if(isset(self::$js['placeholder'])){
				foreach(self::$js['placeholder'] as $js){
					$assets .= '<script src ="'.$js->path.$js->name.'"></script>';
				}
			}
			if(isset(self::$js['content'])){
				foreach(self::$js['content'] as $js){
					$assets .= '<script src ="'.$js->path.$js->name.'"></script>';
				}
			}

		}
		return $assets;
	}
}
