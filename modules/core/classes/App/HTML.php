<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

class App_HTML extends Kohana_HTML
{
	/**
	 * @inherit
	 */
	public static function image($file, array $attributes = NULL, $protocol = NULL, $index = FALSE)
	{
		$file = Core::findFile('assets/images/'.$file);
		if($attributes){
			if(isset($attributes['class'])){
				$attributes['class'] .= ' img-responsive';
			}else{
				$attributes['class'] = 'img-responsive';
			}
		}else{
			$attributes['class'] = 'img-responsive';
		}
		return parent::image($file,$attributes,$protocol,$index);
	}
	
	
	/**
	 * HTML LI element
	 * 
	 * @param mixed attributes
	 * 
	 * @return string
	 */
	public static function li($attributes)
	{
		if(is_array($attributes)){
			
		}else{
			$content = $attributes;
		}
		$html = '<li class="">';
		$html .= $content;
		$html .= '</li>';
		return $html;
	}
	
	
	/**
	 * Pager.
	 * 
	 * @param array $pager pager array
	 * @param string $url url for pager links
	 * @param string $separator separator for pages numbers
	 * 
	 * @return string HTML code for pager
	 */
	public static function pager($pager, $url, $separator = '&nbsp')
	{
		$html = '';
		$html .= '<div class="pager"></div>';
		if($pager['current_page'] > 1){
			$html .= self::anchor($url.'/1','<<');
			$html .= $separator;
			$html .= self::anchor($url.strval($pager['current_page']-1),'<');
		}
		for($page=1;$page<=$pager['pages_number'];$page++){
			$html .= $separator;
			if($page===$pager['current_page']){
				$html .= $page;
			}else{
				$html .= HTML::anchor($url.$page,$page);
			}
		}
		$html .= $separator;
		if($pager['current_page'] < $pager['pages_number']){
			$html .= HTML::anchor($url.strval($pager['current_page']+1),'>');
			$html .= $separator;
			$html .= HTML::anchor($url.strval($pager['pages_number']),'>>');
		}
		$html .= '</div>';
		return $html;
	}

	/**
	 * Create tree.
	 * 
	 * Tree is created by using unordered list.
	 * 
	 * @param object $objects tree elements objects
	 * @param array $properties object properties
	 * @param array $options additional options for the list:
	 * - urls: urls array
	 * 	- url: url
	 * 	- parameters: url parameters
	 * 	- label: label
	 */
	public static function tree($objects, $properties, $options = array())
	{
		if(is_object($objects)){
			$html = "";
			if(count($objects)){
				$html .= '<ul>';
				foreach($objects as $object){
					$html .= '<li>';
					$properties_count = count($properties);
					$counter = 0;
					if(is_array($properties)){
						foreach($properties as $property){
							$counter++;
							if(isset($object->lang(Lang::get())->$property)){
								$html .= $object->lang(Lang::get())->$property;
							}else{
								$html .= $object->$property;
							}
							if($counter < $properties_count){
								$html .= ' ';
							}
						}
					}else{
						$property = $properties;
						if(isset($object->lang(Lang::get())->$property)){
							$html .= $object->lang(Lang::get())->$property;
						}else{
							$html .= $object->$property;
						}
					}
					if(isset($options['urls'])){
						foreach($options['urls'] as $url){
							if(isset($url['parameters'])){
								$parameters = '';
								foreach($url['parameters'] as $parameter) $parameters .= $parameter.'/';
							}else{
								$parameters = '';
							}
							$html .= ' ';
							$html .= self::anchor($url['url'].$parameters.$object->id,__($url['label']));
							$html .= ' ';
						}
					}
					$html .='</li>';
					if(isset($object->children)){
						if($object->children->count_all()){
							$html .= self::tree($object->children->getList($object->id),$properties,$options,false);
						}
					}
				}
				$html .= '</ul>';
			}
			return $html;
		}else die('Object should be object type');
	}
	
	
	public static function ul_open()
	{
		return '<ul class="">';
	}
	
	
	public static function ul_close()
	{
		return '</ul>';
	}
}
