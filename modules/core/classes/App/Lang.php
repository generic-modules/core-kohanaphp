<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

class App_Lang
{
	/**
	 * Get lang
	 * 
	 * @return string
	 */
	public static function get()
	{
		if(Kohana::$config->load('global.languages.multilanguage')){
			
		}else{
			$lang = Kohana::$config->load('global.languages.default');
		}
		return $lang;
	}
}
