<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * Templates management class
 * 
 * It defines application's appearence
 * If not specified, there is one default template.
 * 
 * Every template has defined it's own layouts.
 * They're stored in directory views/templates/template_name.
 */
class App_Template
{
	/**
	 * @var string $name 
	 *
	 * Template name. If not specified it's "default".
	 */
	public $name;

	/**
	 * @var string $layout 
	 * 
	 * Active layout.
	 */
	public $layout;
	
	/**
	 * @var array $layouts 
	 * 
	 * Array of template's layouts.
	 * If not specified, there is one layout named "default".
	 */
	public $layouts;
		
	/**
	 * @var string $path
	 * 
	 * Template path.
	 */
	public $path;

	/**
	 * Class constructor.
	 * 
	 * There are set name, layouts and path to template directory
	 * If not specified, there are set default values:
	 * name - Default
	 * layouts - one layout named default
	 * path - views/templates/default/
	 *
	 * @param string $name nazwa szablonu
	 * @param array[Template_Layout] $layouts layouty szablonu
	 * @param string $path ścieżka do szablonu
	 *
	 * @return void
	 */
	public function __construct($name = 'default', $layouts = ['default'], $path = null)
	{
		$this->name = $name;
		if($path){
			$this->path = $path;
		}else{
			$this->path = 'templates/'.$name;
		}
		foreach($layouts as $name){
			$this->addLayout($name);
		}
		$this->layout = reset($this->layouts);
	}

	/**
	 * Ustawia nazwę szablonu.
	 * 
	 * @param string $name Nazwa szablonu
	 *
	 * @return void
	 */
	public function setName($name)
	{
		$this->name = $name;
	}
	
	/**
	 * Pobiera nazwę szablonu.
	 *
	 * @return string nazwa szablonu
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * Dodaje layout do szablonu.
	 *
	 * Jeżeli w parametrach zostanie podana nazwa już istniejącego layoutu
	 * to layout ten zostanie nadpisany.
	 *
	 * @param $name nazwa layoutu
	 *
	 * @return void
	 */
	public function addLayout($name = 'default')
	{
		$this->layouts[$name] = new Template_Layout($this->name,$name);
		$this->layout = $this->layouts[$name];
	}
	
	/**
	 * Pobiera określony layout.
	 *
	 * @param string $name nazwa layoutu
	 *
	 * @return Template_Layout wskazany w parametrach layout
	 */
	public function getLayout($name)
	{
		return $this->layouts[$name];
	}
	
	/**
	 * Activate layout
	 *
	 * Make's specified layout active, witch means that layout
	 * will be set to property layout and will be used.
	 *
	 * @param string $name layout name
	 */
	public function activateLayout($name)
	{
		$this->layout = $this->layouts[$name];
	}
	
	/**
	 * Get active layout.
	 *
	 * Simply get reference to active layout.
	 *
	 * @return reference to property layout
	 */
	public function getActiveLayout()
	{
		return $this->layout;
	}
	
	/**
	 * Ustawia ścieżkę do szablonu.
	 *
	 * @param string $path ścieżka do szablonu
	 *
	 * @return void
	 */
	public function setPath($path)
	{
		$this->path = $path;
	}
	
	/**
	 * Pobiera ścieżkę do szablonu.
	 *
	 * @return string ścieżka do szablonu
	 */
	public function getPath()
	{
		return $this->path;
	}
}
