<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * Api wrapper
 *
 * Implements singleton.
 */
class App_Api
{
	
	/**
	 * @var array[object]
	 */
	protected static $instances;
	
	/**
	 * Check api instance and create if not created.
	 * 
	 * @param string $name
	 * 
	 * return object api class
	 */
	public static function instance($name)
	{
		if(!isset(self::$instances[$name])){
			$config = Kohana::$config->load('api.'.$name);
			if(!isset($config)) die('No api '.$name);
			$class = $config['class_name'];
			self::$instances[$name] = new $class();
		}
		return self::$instances[$name];
	}
}
