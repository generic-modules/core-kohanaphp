<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Base controller for user interface.
 *
 * Methods:
 * - before
 */
class Controller_App_User_Base extends Controller_Base
{
	/**
	 * Before every action.
	 *
	 * Create template for user interface.
	 * Create default layout with its placeholders: doctype, head, header, left menu, footer.
	 * Register assets for the whole user interface: js and css.
	 *
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->template = new Template('user');
		$this->layout = $this->template->getActiveLayout();
		$this->layout->addPlaceholder('doctype');
		$this->layout->addPlaceholder('head');
		$this->layout->addPlaceholder('header');
		$this->layout->addPlaceholder('footer');
		$this->layout->addPlaceholder('menu');
		$this->layout->addPlaceholder('context_menu');
		$this->layout->addPlaceholder('user_menu');
		$this->layout->addPlaceholder('contact');
		$this->layout->placeholders['header']->addPlaceholder('logo');
		$this->layout->placeholders['header']->addPlaceholder('contact_phone');
		if(Auth::instance()->logged_in()){
			$this->layout->placeholders['header']->addPlaceholder('user_panel');
		}else{
			$this->layout->placeholders['header']->addPlaceholder('login');
		}
// 		$this->layout->placeholders['footer']->addPlaceholder('regulations');
// 		$this->layout->placeholders['footer']->addPlaceholder('cookies_policy');
		$this->layout->placeholders['contact']->prefix = 'user.contact.';
		$this->assets = new Asset();
		$this->assets->addCss('template',$this->template->name);
		$this->assets->addCss('layout','default',$this->template->name);
		$this->assets->addCss('placeholder','header',$this->template->name,$this->layout->name);
		$this->assets->addCss('placeholder','footer',$this->template->name,$this->layout->name);
		$this->assets->addCss('placeholder','menu',$this->template->name,$this->layout->name);
		$this->assets->addCss('placeholder','context_menu',$this->template->name,$this->layout->name);
		$this->assets->addCss('placeholder','user_menu',$this->template->name,$this->layout->name);
		$this->assets->addCss('placeholder','contact',$this->template->name,$this->layout->name);
		$this->assets->addJs('class','user');
		$this->assets->addJs('template',$this->template->name);
		$this->assets->addJs('layout','default',$this->template->name);
		$this->assets->addJs('placeholder','header',$this->template->name,$this->layout->name);
		$this->assets->addJs('placeholder','footer',$this->template->name,$this->layout->name);
		$this->assets->addJs('placeholder','menu',$this->template->name,$this->layout->name);
		$this->assets->addJs('placeholder','context_menu',$this->template->name,$this->layout->name);
		$this->assets->addJs('placeholder','contact',$this->template->name,$this->layout->name);
		$menu = new Menu('main',['menu_css'=>['list-inline'],'rows_css'=>[''],'rows_anchor_css'=>['']]);
		$menu->add('admin_panel',[URL::base().'admin/',__('user.menu.admin_panel.url'),'admin']);
		$menu->add('users',[URL::base().'users/management/edit',__('user.menu.users.url'),'login']);
		$menu->add('create_user',[URL::base().'users/management/create',__('user.menu.create_user.url')]);
		$menu->add('contact_form',[URL::base().'index/contact',__('user.menu.contact.url')]);
		$menu->add('news',[URL::base().'news/list',__('user.menu.news.url')]);
		$this->menu = $menu;
		$menu = new Menu('context',['menu_css'=>['list-inline'],'rows_css'=>[''],'rows_anchor_css'=>['']]);
		$menu->add(['users','edit_user'],[URL::base().'users/management/edit',__('user.menu.edit_user.url'),'login']);
		$menu->add(['users','remove_user'],[URL::base().'users/management/remove',__('user.menu.remove_user.url'),'login']);
		$menu->add(['users','change_password'],[URL::base().'users/management/changepassword',__('user.menu.change_password.url'),'login']);
		$menu->add(['news','list'],[URL::base().'news/list',__('user.menu.news_list.url')]);
		$menu->add(['news','article'],[URL::base().'news/article',__('user.menu.news_article.url')]);
		$this->context_menu = $menu;
		$menu = new Menu('user',['menu_css'=>['list-inline'],'rows_css'=>[''],'rows_anchor_css'=>['']]);
		$menu->add('edit_user',[URL::base().'users/management/edit',__('user.user_menu.edit_user.url'),'login']);
		$this->user_menu = $menu;
	}
	
	/**
	 * @inherit
	 */
	public function after()
	{
		$this->layout->placeholders['menu']->menu = $this->menu;
		if(isset($this->layout->placeholders['context_menu'])) $this->layout->placeholders['context_menu']->menu = $this->context_menu;
		if(isset($this->layout->placeholders['user_menu'])) $this->layout->placeholders['user_menu']->menu = $this->user_menu;
		parent::after();
	}
}
