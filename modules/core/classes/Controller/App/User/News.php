<?php defined('SYSPATH') or die('No direct script access.');

class Controller_App_User_News extends Controller_User_Base
{
	public function before()
	{
		parent::before();
		$this->path = 'news/';
		$this->prefix = 'news.';
		$this->menu->activate('news');
		$this->context_menu->selectGroup('news');
		
	}
	
	/**
	 * Get list action.
	 * 
	 * @return void
	 */
	public function action_list()
	{
		$news = ORM::factory('Content')->getNews();
		$this->init('list');
		$this->context_menu->activate(['news','list']);
		$this->content->news = $news;
	}
	
	/**
	 * Get article action.
	 * 
	 * @return void
	 */
	public function action_article()
	{
		$article = ORM::factory('Content')->getOne($this->request->param('id'));
		$this->init('article');
		$this->context_menu->activate(['news','article']);
		$this->content->article = $article;
	}
}
