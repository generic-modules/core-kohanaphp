<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Index controller.
 * 
 * Actions:
 * - index
 * 
 * Methods:
 * - before
 */
class Controller_App_User_Index extends Controller_User_Base {

	/**
	 * Before every action.
	 * 
	 * Set path for this controller.
	 * 
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->path = '';
		$this->prefix = 'user.index.';
	}

	/**
	 * Index action.
	 *
	 * Usually it's application's main site.
	 *
	 * @return void
	 */
	public function action_index()
	{
		$this->init('index');
	}
	
	/**
	 * Contact action.
	 *
	 * Displays contact form to enable user send emails.
	 *
	 * @return void
	 */
	public function action_contact()
	{
		$this->init('contact');
		if($data = $this->request->post()){
			if(ORM::factory('Email')->send($data,$_FILES)){
				Message::set('email_sent');
			}
		}
	}
}
