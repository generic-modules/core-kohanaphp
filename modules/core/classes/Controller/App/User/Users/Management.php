<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Controller for users management.
 *
 * Actions:
 * - changePassword
 * - create
 * - edit
 * - remove
 * - resetPassword
 *
 * Methods:
 * - before
 * - initView
 */
class Controller_App_User_Users_Management extends Controller_User_Base
{
	/**
	 * Before every action.
	 *
	 * Set path for this controller and protect actions.
	 *
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->path = 'users/management/';
		$this->prefix = 'user.users.management.';
		$this->protect(Kohana::$config->load('controllers.user.users.management.actions.protected'),'login','/login');
		$this->menu->activate('users');
		$this->context_menu->selectGroup('users');
	}

	/**
	 * Change password for own user account.
	 *
	 * First change password form is displayed.
	 * User fills form, submit it and if no errors, data are saved and form is displayed with message about successful save.
	 * If there are errors, form is displayed again with errors messages.
	 *
	 * @param array post data
	 *
	 * @return view users/management/change_password
	 */
	public function action_changePassword()
	{
		if($this->request->is_ajax()){

		}else{
			$this->init('change_password');
			$this->context_menu->activate(['users','change_password']);
			$user = ORM::factory('User')->getOne(Auth::instance()->get_user()->id);
			if($data = $this->request->post()){
				if($user->changePassword($data)){
					Message::set('change_password_success');
					$this->data['success'] = true;
				}
			}
			$this->content->user = $user;
		}
	}

	/**
	 * Create user account.
	 *
	 * First create account form is displayed.
	 * Next user fills form and if there are no errors
	 * data are saved and form with successful save message is displayed.
	 * If there are errors, form is displayed with errors messages.
	 *
	 * @param array post data
	 *
	 * @return view users/management/create
	 */
	public function action_create()
	{
		if($this->request->is_ajax()){
			if($data = $this->request->post()){
				$user = ORM::factory('User');
				if($user->createOne($this->request->post())){
					Message::set('create_user_success');
				}
			}
		}else{
			$this->init('create',['title','registerform','registerform/contactperson','registerform/livingaddress']);
			$this->menu->activate('create_user');
			$user = ORM::factory('User');
			if($post = $this->request->post()){
				if($user->createOne($post)){
					Message::set('create_user_success');
					$this->redirect('/');
				}else{
// 					$this->content->user = $post;
				}
			}else{
// 				$this->content->user = $this->initView('create_user');
			}
			$this->content->user = $user;
			
			
		}
	}

	/**
	 * Display users list.
	 *
	 * Form displays paginated users list.
	 * At each page 10 rows are displayed.
	 *
	 * @return users/management/display_list
	 */
	public function action_displayList()
	{
		if($this->request->is_ajax()){
			if($this->request->post()){
				$this->result['data'] = ORM::factory('User')->getList(array('array'=>true,'filters'=>$this->request->post('filters')));
			}else{
				$this->result['data'] = ORM::factory('User')->getList(array('array'=>true));
			}
		}else{
			$this->init('display_list');
			$this->request->param('page') ? $page = intval($this->request->param('page')) : $page = 1;
			$users_list_records_per_page = Kohana::$config->load('controllers.admin.users_management.users_list.records_per_page');
			Session::instance()->set('uri_users_list',$this->request->uri());
			if($this->request->post()){

			}else{
				$users = ORM::factory('User')->getList($page,$users_list_records_per_page);
			}
			$pages_number = ORM::factory('User')->getList($page,$users_list_records_per_page,true);
			if($pages_number > 1){
				$users_pager['pages_number'] = $pages_number;
				$users_pager['current_page'] = $page;
			}
			$this->content->users = $users;
			if(isset($users_pager)) $this->content->users_pager = $users_pager;
			$message = Session::instance()->get_once('message');
			if($message) $this->content->message = $message;
		}
	}

	/**
	 * Edit own user account.
	 *
	 * First edit form is displayed and logged in user's data are loaded to form.
	 * Next user changes his data and if no errors, data are saved and form with new data and successfull save message is displayed.
	 * If there are errors, form with changed data and errors messages is displayed.
	 *
	 * @param array post data
	 *
	 * @return view users/management/edit
	 */
	public function action_edit()
	{
		if($this->request->is_ajax()){
			$id = $this->request->param('id');
			if(!$id){
				if($data = $this->request->post()){
					$id = $data['data']['id'];
				}
			}
			$user = ORM::factory('User')->getOne($id);
			if($data = $this->request->post()){
				if($user->edit($data)){
					Message::set('save_success');
				}
			}else{
				$this->result['data'] = $user->toArray();
			}
		}else{
			$this->init('edit');
			$this->context_menu->activate(['users','edit_user']);
			$id = $this->request->param('id');
			if(!$id) $id = Auth::instance()->get_user()->id;
			$user = ORM::factory('User')->getOne($id);
			if($data = $this->request->post()){
				if($user->edit($data)){
					Message::set('save_success');
				}
			}
			$this->content->user = $user;
		}
	}

	/**
	 * Remove user account.
	 *
	 * First warning message is displayed.
	 * If confirmed, logged in user's account is removed
	 * and browser is redirected to main page.
	 *
	 * @return void
	 */
	public function action_remove()
	{
		if($this->request->is_ajax()){
			$id = $this->request->param('id');
			if(!$id) $id = Auth::instance()->get_user()->id;
			if(ORM::factory('User')->removeOne($id)){
				Message::set('remove_user_success');
			}
		}else{
			if($this->request->post('remove')){
				$user = ORM::factory('User');
				$user->removeOne(Auth::instance()->get_user()->id);
				Message::set('remove_user_success');
				$user->logout();
				$this->redirect('/');
			}else{
				$this->init('remove');
				$this->context_menu->activate(['users','remove_user']);
				$this->content->referrer = $this->request->referrer();
			}
		}
	}

	/**
	 * Reset own password.
	 *
	 * First reset password form is displayed.
	 * Next user fills his email address.
	 * If it's correct address, password is reset and a new password is sent to user's email address.
	 * If provided email address is not valid, form with error message is displayed.
	 *
	 * @param array post data
	 *
	 * @return users/management/reset_password
	 */
	public function action_resetPassword()
	{
		if($this->request->is_ajax()){

		}else{

		}

		$this->init('reset_password');
		$user = ORM::factory('User');
		if($this->request->post()){
			if($user->checkEmail($this->request->post())){
				$password = $user->resetPassword();
				$user->sendPassword($user->email,$password);
				unset($password);
				Message::set('reset_password_success');
			}
		}
	}
}
