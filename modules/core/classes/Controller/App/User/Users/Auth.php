<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Auth controller.
 *
 * Actions for user authentication.
 *
 * Actions:
 * - login
 * - logout
 * - reset password
 */
class Controller_App_User_Users_Auth extends Controller_User_Base
{
	/**
	 * Before any action.
	 *
	 * Set path for this controller.
	 *
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->path = 'users/auth/';
		$this->prefix = 'user.users.auth.';
	}

	/**
	 * Logs user in.
	 *
	 * In first run displays login form.
	 *
	 * If user inserts correct credentials, logs user in and redirects to main page,
	 * or to current page if user did not choose this action directly (for example session expired
	 * and user was redirected to this action).
	 *
	 * If credentials are incorrect it displays login form again with errors messages.
	 *
	 * @param array post data
	 *
	 * @return view users/login if not redirected
	 */
	public function action_login()
	{
		if($post = $this->request->post()){
			if(ORM::factory('User')->login($post)){
				if($this->ignoreUri(Session::instance()->get('uri'))){
					$this->redirect('/');
				}else{
					$this->redirect(Session::instance()->get_once('uri'));
				}
			}
		}
		$this->init('login');
		$this->content->addPlaceholder('login');
	}

	/**
	 * Log user out.
	 *
	 * Logs user out and redirects to main page.
	 *
	 * @return void
	 */
	public function action_logout()
	{
		ORM::factory('User')->logout();
		$this->redirect('/');
	}

	/**
	 * Reset password
	 *
	 * Redirects to reset_password_email page
	 */
	public function action_resetLink()
	{
		$this->init('reset_password_email');
	}
	public function action_resetPassword()
	{
		$this->init('reset_password');
	}
}
