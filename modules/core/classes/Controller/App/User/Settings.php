<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Index controller.
 *
 * Actions:
 * - index
 *
 * Methods:
 * - before
 */
class Controller_App_User_Settings extends Controller_User_Base
{
	/**
	 * @inherit
	 */
	public function before()
	{
		parent::before();
		$this->path = '/';
		$this->prefix = 'user.';
	}

	/**
	 * Edit action.
	 *
	 * @return void
	 */
	public function action_edit()
	{
		if($this->request->is_ajax()){
			if($post = $this->request->post()){
				if(ORM::factory('Setting')->saveAll($post)){
					Message::set('save_success');
				}else{
					Error::set('form','save_failed');
				}
			}
		}else{
			$this->init('settings');
			if($post = $this->request->post()){
				if(ORM::factory('Setting')->saveAll($post)){
					Message::set('save_success');
				}else{
					Error::set('form','save_failed');
				}
			}
			$this->content->settings = ORM::factory('Setting')->getAll();
		}
	}

	/**
	 * Load action.
	 *
	 * @return void
	 */
	public function action_load()
	{
		if($this->request->is_ajax()){
			$this->result['data'] = ORM::factory('Setting')->getList(array('array'=>true));
		}


	}
}
