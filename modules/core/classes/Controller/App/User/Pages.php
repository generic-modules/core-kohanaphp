<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Index controller.
 * 
 * Actions:
 * - index
 * 
 * Methods:
 * - before
 */
class Controller_App_User_Pages extends Controller_User_Base {

	/**
	 * Before every action.
	 * 
	 * Set path for this controller.
	 * 
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->path = 'pages/';
		$this->prefix = 'user.pages.';
	}

	/**
	 * Display regulations action.
	 * 
	 * @return void
	 */
	public function action_regulations()
	{
		$regulations = ORM::factory('Content')->getOne('regulations');
		$this->init('regulations');
		$this->content->regulations = $regulations;
	}

	/**
	 * Display coockies policy action.
	 *
	 * @return void
	 */
	public function action_coockiesPolicy()
	{
		$coockies_policy= ORM::factory('Content')->getOne('coockies_policy');
		$this->init('coockies_policy');
		$this->content->coockies_policy= $coockies_policy;
	}
}
