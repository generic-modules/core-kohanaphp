<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Users management controller.
 *
 * Actions:
 * - assignRoles
 * - changePassword
 * - create
 * - displayList
 * - edit
 * - remove
 * - resetPassword
 *
 * Methods:
 * - before
 */
class Controller_App_Admin_Users_Management extends Controller_Admin_Base
{
	/**
	 * Before every action.
	 *
	 * Set path for this controller.
	 *
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->path = 'users/management/';
		$this->prefix = 'admin.users.management.';
		$this->menu->activate('users');
		$this->context_menu->selectGroup('users');
	}

	/**
	 * Assign roles for particular user account.
	 *
	 * First assign roles form is displayed and assigned for user roles are checked.
	 * Next user changes roles and save changes and message about this is displayed.
	 *
	 * @param int $id user id
	 * @param array $ids roles id numbers
	 *
	 * @return users/management/assign_roles
	 */
	public function action_assignRoles()
	{
		$this->init('assign_roles');
		$user = ORM::factory('User')->getOne($this->request->param('id'));
		if($this->request->post()){
			$ids = $this->request->post('roles_list');
			$roles = ORM::factory('Role')->getListByIds($ids);
			$user->assignRoles($roles);
			Message::set('assign_roles_success');
		}
		$this->content->user = $user;
		$this->content->roles = ORM::factory('Role')->getList();
	}

	/**
	 * Create user account.
	 *
	 * First create account form is displayed.
	 * Next user fill data and submits the form.
	 * If data are correct, they are saved and message about successfull save is displayed.
	 * If there are errors, form with submitted data is displayed and with errors messages.
	 *
	 * @param array post data
	 *
	 * @return users/management/create
	 */
	public function action_create()
	{
		$this->init('create');
		$this->context_menu->activate(['users','create_user']);
		$user = ORM::factory('User');
		if($post = $this->request->post()){
			if($user->createOne($post)){
				Message::set('create_user_success');
				$this->redirect(Session::instance()->get_once('uri_users_list'));
			}
		}
		$this->content->user = $user;
		$this->content->roles = ORM::factory('Role')->getList(['array'=>true,'columns'=>'name','where'=>[['is_visible',true]]]);
	}

	/**
	 * Display users list.
	 *
	 * Form displays paginated users list.
	 * At each page 10 rows are displayed.
	 *
	 * @return users/management/display_list
	 */
	public function action_displayList()
	{
		$this->init('display_list');
		$this->context_menu->activate(['users','users_list']);
		$this->request->param('page') ? $page = intval($this->request->param('page')) : $page = 1;
		$users_list_records_per_page = Kohana::$config->load('controllers.admin.users_management.users_list.records_per_page');
		Session::instance()->set('uri_users_list',$this->request->uri());
		if($this->request->post()){

		}else{
			$users = ORM::factory('User')->getList($page,$users_list_records_per_page);
		}
		
		
		
		
// 		echo 'test'; 
		
// 		var_dump(ORM::factory('User')->count_all());
		
// 		die;
		
		
		
		
		
		if(ORM::factory('User')->count_all() > 0){
			$pages_number = ORM::factory('User')->countPages();
			$users_pager['pages_number'] = $pages_number;
			if($page> 1){
				$users_pager['current_page'] = $page;
			}else{
				$users_pager['current_page'] = 1;
			}
		}else{
			$pages_number = 1;
		}
		$this->content->users = $users;
		if(isset($users_pager)) $this->content->users_pager = $users_pager;
		$message = Session::instance()->get_once('message');
		if($message) $this->content->message = $message;
	}

	/**
	 * Edit particular user account.
	 *
	 * First form with selected user's data is displayed.
	 * Next user feels the form and submit it.
	 * If data are correct, they are saved and message about successfull save is displayed.
	 * If there are errors, form with submitted data and errors messages is displayed.
	 *
	 * @param int $id user id
	 *
	 * @return users/management/edit
	 */
	public function action_edit()
	{
		$this->init('edit');
		$id = $this->request->param('id');
		$user = ORM::factory('User')->getOne($id);
		if($data = $this->request->post()){
			if($user->edit($data)){
				Message::set('save_success');
			}
		}
		$this->content->user = $user;
	}

	/**
	 * Remove particular user account.
	 *
	 * First display warning.
	 * Next if user accpts operation, user's account is deleted
	 * and browser is returned to referring page.
	 * Otherwise browser returns to referring page.
	 *
	 * @param int $id user id
	 *
	 * @return view users/management/remove in the first run.
	 */
	public function action_remove()
	{
		$id = $this->request->param('id');
		if($this->request->post('remove')){
			ORM::factory('User')->removeOne($id);
			Message::set('remove_user_success');
			$this->redirect(Session::instance()->get_once('uri_users_list'));
		}else{
			$this->init('remove');
			$this->content->user = ORM::factory('User')->getOne($id);
			$this->content->referrer = $this->request->referrer();
		}
	}

	/**
	 * Reset password for particular user.
	 *
	 * For selected user password is reset.
	 * end email with the new one is sent to his main email address.
	 * After that, browser is redirected to referrer uri.
	 *
	 * @param int $id user id
	 *
	 * @return void
	 */
	public function action_resetPassword()
	{
		$id = $this->request->param('id');
		$user = ORM::factory('User')->getOne($id);
		$password = $user->resetPassword();
		$user->sendPassword($user->email,$password);
		unset($password);
		Message::set('admin_reset_password_success',array(':username'=>$user->username));
		$this->redirect($this->request->referrer());
	}
}