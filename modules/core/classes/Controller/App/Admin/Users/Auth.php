<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Auth controller.
 * 
 * Actions for user authentication.
 * 
 * Actions:
 * - login
 * - logout
 */
class Controller_App_Admin_Users_Auth extends Controller_Admin_Base 
{
	/**
	 * Before any action.
	 * 
	 * Set path for this controller.
	 * 
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->path = 'users/auth/';
		$this->prefix = 'admin.users.auth.';
	}	
	
	/**
	 * Logs user in.
	 * 
	 * In first run displays login form.
	 * 
	 * If user inserts correct credentials, logs user in and redirects to main page,
	 * or to current page if user did not choose this action directly (for example session expired 
	 * and user was redirected to this action).
	 * 
	 * If credentials are incorrect it displays login form again with errors messages.
	 * 
	 * @param array post data
	 * 
	 * @return view users/login if not redirected
	 */
	public function action_login()
	{
		if($data = $this->request->post()){
			if(ORM::factory('User')->login($data)){
				if($this->ignoreUri(Session::instance()->get('uri'))){
					$this->redirect('/admin');
				}else{
					$this->redirect(Session::instance()->get_once('uri'));
				}
			}
		}
		$this->init('login');		
	}

	/**
	 * Log user out.
	 * 
	 * Logs user out and redirects to main page. 
	 * 
	 * @return void
	 */
	public function action_logout()
	{
		ORM::factory('User')->logout();
		$this->redirect('/admin');
	}		
}