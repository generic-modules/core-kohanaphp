<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Content management.
 * 
 * Actions:
 * - create
 * - displayList
 * - edit
 * - remove
 * 
 * Methods:
 * - before
 */
class Controller_App_Admin_Contents_Management extends Controller_Admin_Base
{
	/**
	 * @inherit
	 */
	public function before()
	{
		parent::before();
		$content_type = $this->request->param('content_type');
		if(!$content_type) die('No content type set in routing');
		$this->contentType = $content_type;
		$views_directory = $this->request->param('views_directory');
		if(!$views_directory) die('No views directory set in routing');
		$directory = str_replace('_', '', $views_directory);
		if($directory==='contents') $directory = 'cms';
		$this->directory = $directory;
		$this->path = $views_directory.'/management/';
		$this->prefix = 'admin.'.$views_directory.'.management.';
	}
	
	/**
	 * @inherit
	 */
	public function after()
	{
		$this->content->directory = $this->directory;
		parent::after();
	}
	
	/**
	 * Create content
	 * 
	 * @param int $category_id content category id
	 * 
	 * @return void
	 */
	public function action_create()
	{
		$this->init('create');
		$category = ORM::factory('Content_Category')->getOne($this->request->param('id'));
		$content = ORM::factory('Content');
		if($data = $this->request->post()){
			if($content->createOne($data,$category)){
				Message::set('create_'.$this->contentType.'_success');
				$this->redirect(Session::instance()->get('contents_categories_uri'));
			}
		}
		$this->content->content = $content;
	}
	
	/**
	 * Display content list.
	 * 
	 * @param int $category_id content category id 
	 * 
	 * @return void
	 */
	public function action_displayList()
	{
		$this->init('display_list');
		Session::instance()->set('contents_categories_uri',$this->request->uri());
		$category = ORM::factory('Content_Category')->getOne($this->request->param('id'));
		$categories_ids = Kohana::$config->load('controllers.contents.displaylist.merge.'.$category->id);
		if($categories_ids){
			//$contents = ORM::factory('Content')->getList($categories_ids);
		}else{
			$contents = ORM::factory('Content')->getList(['where'=>[['category_id',$category->id]]]);
		}
		Session::instance()->set('uri_contents_list',$this->request->uri());
		$this->content->contents = $contents;
		$this->content->category = $category;
	}
	
	/**
	 * Edit content.
	 * 
	 * @param int $content_id content id
	 * 
	 * @return void
	 */
	public function action_edit()
	{
		$this->init('edit');
		$content = ORM::factory('Content')->getOne($this->request->param('id'));
		if($data = $this->request->post()){
			if($content->editOne($data)){
				Message::set('save_success');
			}
		}
		$this->content->content = $content;
	}
	
	/**
	 * Remove content.
	 * 
	 * @param int $id content id
	 */
	public function action_remove()
	{
		$id = $this->request->param('id');
		if($this->request->post('remove')){
			ORM::factory('Content')->removeOne($id);
			Message::set('remove_'.$this->contentType.'_success');
			$this->redirect(Session::instance()->get_once('uri_contents_list'));
		}else{
			$this->init('remove');
			$this->content->content = ORM::factory('Content')->getOne($id);
			$this->content->referrer = $this->request->referrer();
		}
	}
}
