<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Content categories management.
 * 
 * Actions:
 * - displayList
 * 
 * Methods:
 * - before
 */
class Controller_App_Admin_Contents_Categories_Management extends Controller_Admin_Base
{
	/**
	 * @inherit
	 */
	public function before()
	{
		parent::before();
		$content_type = $this->request->param('content_type');
		if(!$content_type) die('No content type set in routing');
		$this->contentType = $content_type;
		$views_directory = $this->request->param('views_directory');
		if(!$views_directory) die('No views directory set in routing');
		$this->path = $views_directory.'/categories/management/';
		$this->prefix = 'admin.'.$views_directory.'.categories.management.';
		$this->menu->activate('cms');
		$this->context_menu->selectGroup('cms');
	}

	/**
	 * Display categories list.
	 * 
	 * @param int $parent_type_id parent category id
	 * 
	 * @return void
	 */
	public function action_displayList()
	{
		$this->init('display_list');
		$this->context_menu->activate(['cms','categories_list']);
		Session::instance()->set('contents_categories_uri',$this->request->uri());
		$this->content->categories = ORM::factory('Content_Category')->getList();
	}


	public function action_edit()
	{
		$this->init('edit');
		$category = ORM::factory('Content_Category')->getOne($this->request->param('id'));
	
		if($data = $this->request->post()){
			if($category->editOne($data)){
				Message::set('save_success');
			}
		}
		$this->content->category = $category;
	
	}

	public function action_create()
	{
		$this->init('create');
	}

}
