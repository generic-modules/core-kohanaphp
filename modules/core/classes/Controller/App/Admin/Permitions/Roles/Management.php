<?php defined('SYSPATH') or die('No direct script access.');

class Controller_App_Admin_Permitions_Roles_Management extends Controller_Admin_Base 
{
	/**
	 * @inherit
	 */
	public function before()
	{
		parent::before();
	}	

	/**
	 * Assign user accounts for particular role.
	 * 
	 * 
	 */
	public function action_assignAccounts()
	{
		
	}
	
	/**
	 * Create role.
	 * 
	 * 
	 */
	public function action_create()
	{
		
	}
	
	/**
	 * Display roles list.
	 * 
	 * 
	 */
	public function action_displayList()
	{
		
	}
	
	/**
	 * Edit particular role.
	 * 
	 * 
	 */
	public function action_edit()
	{
		
	}
	
	/**
	 * Remove particular role.
	 * 
	 * 
	 */
	public function action_remove()
	{
		
	}
}