<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Index controller.
 * 
 * Actions:
 * - index
 * 
 * Methods:
 * - before
 */
class Controller_App_Admin_Settings extends Controller_Admin_Base
{
	/**
	 * Before every action.
	 * 
	 * Set path for this controller.
	 */
	public function before()
	{
		parent::before();
		$this->path = '/';
		$this->prefix = 'admin.';
	}

	/**
	 * Index action.
	 * 
	 * Display main page.
	 * 
	 * @return view index
	 */
	public function action_edit()
	{
		$this->init('settings');
		$this->menu->activate('settings');
		if($post = $this->request->post()){
			if(ORM::factory('Setting')->saveAll($post)){
				Message::set('save_success');
			}else{
				Error::set('form','save_failed');
			}
		}
		$this->content->settings = ORM::factory('Setting')->getActive();
	}
}
