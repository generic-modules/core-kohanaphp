<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Index controller.
 * 
 * Actions:
 * - index
 * 
 * Methods:
 * - before
 */
class Controller_App_Admin_Index extends Controller_Admin_Base
{
	/**
	 * Before every action.
	 * 
	 * Set path for this controller.
	 */
	public function before()
	{
		parent::before();
		$this->path = '';
		$this->prefix = 'admin.index.';
	}

	/**
	 * Index action.
	 * 
	 * Display main page.
	 * 
	 * @return view index
	 */
	public function action_index()
	{
		$this->init('index');
	}
}
