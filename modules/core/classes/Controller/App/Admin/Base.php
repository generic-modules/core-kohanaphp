<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Base controller for admin interface.
 *
 * Methods:
 * - before
 */
class Controller_App_Admin_Base extends Controller_Base
{
	/**
	 * Before every action.
	 *
	 * Creates template for admin interface.
	 * Creates default layout with its placeholders: doctype, head, header, menu, footer.
	 * Register assets for every controller.
	 *
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->protect('*','admin','/admin/login');
		$this->template = new Template('admin');
		$this->layout = $this->template->getActiveLayout();
		$this->layout->addPlaceholder('doctype');
		$this->layout->addPlaceholder('head');
		$this->layout->addPlaceholder('header');
		$this->layout->addPlaceholder('menu');
		$this->layout->addPlaceholder('footer');
		$this->assets->addCss('template',$this->template->name);
		$this->assets->addCss('layout','default',$this->template->name);
		$this->assets->addCss('placeholder','header',$this->template->name,$this->layout->name);
		$this->assets->addCss('placeholder','footer',$this->template->name,$this->layout->name);
		$this->assets->addCss('placeholder','menu',$this->template->name,$this->layout->name);
		$this->assets->addJs('template',$this->template->name);
		$this->assets->addJs('layout','default',$this->template->name);
		$this->assets->addJs('placeholder','header',$this->template->name,$this->layout->name);
		$this->assets->addJs('placeholder','footer',$this->template->name,$this->layout->name);
		$this->assets->addJs('placeholder','menu',$this->template->name,$this->layout->name);
		$menu = new Menu('admin_main',['menu_css'=>[''],'rows_css'=>[''],'rows_anchor_css'=>['']]);
		$menu->add('users',[URL::base().'admin/users/management/displaylist',__('admin.menu.users.url'),'admin']);
		$menu->add('cms',[URL::base().'admin/cms/contents/categories/management/displaylist',__('admin.menu.cms.url'),'admin']);
		$menu->add('settings',[URL::base().'admin/settings/edit',__('admin.menu.settings.url'),'admin']);
		$menu->add('logout',[URL::base().'logout',__('admin.menu.logout.url'),'admin']);
		$this->menu = $menu;
		$menu = new Menu('admin_context',['menu_css'=>[''],'rows_css'=>[''],'rows_anchor_css'=>['']]);
		$menu->add(['users','users_list'],[URL::base().'admin/users/management/displaylist',__('admin.menu.users_list.url'),'admin']);
		$menu->add(['users','create_user'],[URL::base().'admin/users/management/create',__('admin.menu.create_user.url'),'admin']);
		$menu->add(['cms','categories_list'],[URL::base().'admin/cms/contents/categories/management/displaylist',__('admin.menu.display_categories_list.url'),'admin']);
		$this->context_menu = $menu;
	}
	
	/**
	 * @inherit
	 */
	public function after()
	{
		$this->layout->placeholders['menu']->menu= $this->menu;
		$this->layout->placeholders['menu']->context_menu= $this->context_menu;
		parent::after();
	}
}
