<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Base controller for application.
 *
 * Methods:
 * - before
 * - protect
 */
class Controller_App_Base extends Controller
{
	/**
	 * @var array $assets
	 *
	 * Array of objects class Assets.
	 */
	protected $assets;

	/**
	 * @var App_Placeholder $content
	 *
	 * Content set for view.
	 */
	protected $content;

	/**
	 * @var array $ignoredUri
	 *
	 * Uri strings that should be ignored for redirections
	 */
	protected $ignoredUri = array();

	/**
	 * @var App_Layout $layout
	 *
	 * Layout used by controller.
	 */
	protected $layout;

	/**
	 * @var string $module
	 *
	 * Module name
	 */
	protected $module;

	/**
	 * @var string $path
	 *
	 * Relative path to controller.
	 * It also refers to views as directories structure is the same.
	 */
	protected $path;

	/**
	 * @var string $prefix
	 *
	 * Prefix used in content
	 */
	protected $prefix;

	/**
	 * @var App_Template $template
	 *
	 * Name of template used by controller.
	 * Defaults to 'default'.
	 */
	protected $template;

	/**
	 * Before every action.
	 *
	 * @return void
	 */
	public function before()
	{
		Form::open(); // solution for framework error in view after request
		$this->assets = new Asset();
		$this->assets->addCss('lib','jquery/jquery.datetimepicker');
		$this->assets->addCss('lib','jquery/jquery-ui.min');
		$this->assets->addCss('lib','bootstrap/css/bootstrap.min');
		$this->assets->addCss('global','global');
		$this->assets->addJs('global','global');
		$this->assets->addJs('global','config');
		$this->assets->addJs('lib','moment');
		$this->assets->addJs('lib','jquery-1.10.2');
		$this->assets->addJs('lib','jquery/jquery.formatDateTime');
		$this->assets->addJs('lib','jquery/jquery-ui.min');
		$this->assets->addJs('lib','jquery/jquery.datetimepicker');
		$this->assets->addJs('lib','jquery/jquery.confirm');
		$this->assets->addJs('lib','jquery/jquery.easytabs.min');
		$this->assets->addJs('lib','jquery/jquery.hashchange.min');
		$this->assets->addJs('lib','bootstrap/js/bootstrap.min');
		$this->assets->addJs('lib','tinymce/tinymce.min');
		$this->assets->addJs('lib','slider/jssor.slider-23.1.1.min');
		$this->assets->addJs('class','message');
		$this->assets->addJs('class','list');
// 		if(!file_exists(DOCROOT.'application/assets/images/cms/content/')) die('Directory /application/assets/images/cms/content is needed for CMS module to work properly.');
		if(strtolower($this->request->action())!=='login') Session::instance()->set('uri',$this->request->uri());
		if($this->request->post('remove_reject_form')) $this->request->post(array());
		$this->ignoredUri = Kohana::$config->load('auth.ignored_uris');
		if($this->request->is_ajax()){
			$this->result = array();
		}
	}

	/**
	 * @inherit
	 */
	public function after()
	{
		if($this->request->is_ajax()){
			if(!isset($this->result['status'])) $this->result['status'] = 'success';
			if(isset($this->result['pdf'])){
				$this->response->body($this->result['pdf']);
			}else{
				$errors = Session::instance()->get_once('errors');
				if(isset($errors)){
					foreach($errors as $field => $errors){
						if(isset($field)){
							foreach($errors as $error){
								$this->result['errors'][] = __('error_message.'.$field.'.'.$error);
							}
						}
					}
				}
				$messages = Session::instance()->get_once('messages');
				if(isset($messages)){
					foreach($messages as $message){
						$this->result['messages'][] = __('message.'.$message['message'],$message['parameters']);
					}
				}
				$this->response->body(json_encode($this->result));
			}
			if(isset($this->result['calendar'])){
				$this->response->body(json_encode($this->result['calendar']));
			}else{
				if(isset($this->result['view'])){
					$this->response->body((string)($this->result['view']));
				}else{
					$this->response->body(json_encode($this->result));
				}
			}
		}else{
			$this->content->prefix = $this->prefix;
			$this->response->body($this->layout);
		}
	}

	/**
	 * Init action.
	 *
	 * @param string $name action name in dashed style (for example action_name)
	 * @param array $views paths to connected views, related to view path with directory named as view name
	 *
	 * @return void
	 */
	public function init($name,$views = [])
	{
		$path = $this->layout->path.'placeholders/content/'.$this->path.$name;
		$this->content = $this->layout->setContent($path);
		if($views){
			foreach($views as $view){
				$name = Core::getFilenameFromPath($view);
				$placeholder = $this->findPlaceholderByPath($view);
				$placeholder->addPlaceholder($name);
			}
		}
		$this->layout->placeholders['head']->title = __($this->prefix.'title');
		$this->assets->addCss('content',$this->path.$name,$this->template->name,$this->layout->name);
		$this->assets->addJs('content',$this->path.$name,$this->template->name,$this->layout->name);
	}
	
	/**
	 * Find placeholder by path.
	 * 
	 * @param string $path path to view file
	 * 
	 * @return Template_Layout_Placeholder
	 */
	protected function findPlaceholderByPath($path)
	{
		$components = explode('/',$path);
		$placeholder = $this->layout->content;
		if(count($components) > 1){
			for($i=1;$i<count($components);$i++){
				$placeholder = $placeholder->placeholders[$components[$i-1]];
			}
		}
		return $placeholder;
		
		
	}
	
	/**
	 * Check if uri should be ignored.
	 *
	 * Used for redirections to check if in some occasion uri should
	 * or should not be ignored.
	 *
	 * @param string $uri uri to check
	 *
	 * @return true if uri should be ignored, otherwise false
	 */
	protected function ignoreUri($uri){
		if(isset($this->ignoredUri)){
			if(is_array($this->ignoredUri)){
				foreach($this->ignoredUri as $ignored){
					if(strtolower($uri)===$ignored) return true;
				}
			}else die('Property ignoredUri must be array.');
		}
		if(!$uri) return true;
		return false;
	}

	/**
	 * Init view with empty data.
	 *
	 * This method is used for views where no object is used.
	 * When such view is used for the first time, there is no post data set.
	 * For the sake of code readability it's better to set empty data similar to post
	 * instead of making if statements in view itself.
	 */
	public function initView($view){}

	/**
	 * Protect controller actions
	 *
	 * @param array[string] | * $actions actions to protect
	 * @param string $role role needed to access actions
	 * @param string $redirect_url redirect to login page
	 *
	 * @return void
	 */
	protected function protect($actions,$role,$redirect_url)
	{
		if(strtolower($this->request->action())!=='login'){
			$action = $this->request->action();
			if(strtoupper($action) == 'RESETLINK' or strtoupper($action) == 'RESETPASSWORD'){
				return false;
			}
			if(is_array($actions)){
				foreach($actions as $action){
					if(strtolower($this->request->action())===strtolower($action)){
						if(!Auth::instance()->logged_in($role)){
							$this->redirect($redirect_url);
						}
					}
				}
			}else if($actions==='*'){
				if(!Auth::instance()->logged_in($role)){
					$this->redirect($redirect_url);
				}
			}
		}
	}
}
