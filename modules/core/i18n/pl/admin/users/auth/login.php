<?php defined('SYSPATH') OR die('No direct script access.');

$translation['admin.users.auth.login.title'] = 'Logowanie';
$translation['admin.users.auth.login.username.label'] = 'Login';
$translation['admin.users.auth.login.password.label'] = 'Hasło';
$translation['admin.users.auth.login.submit'] = 'Zaloguj';
