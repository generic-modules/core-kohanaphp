<?php defined('SYSPATH') OR die('No direct script access.');

$translation['admin.users.management.assign_roles.title'] = 'Przypisanie ról dla użytkownika :username';
$translation['admin.users.management.assign_roles.submit'] = 'Zapisz zmiany';
