<?php defined('SYSPATH') OR die('No direct script access.');

$translation['admin.users.management.remove.title'] = 'Usunięcie konta';
$translation['admin.users.management.remove.warning'] = 'Czy na pewno chcesz usunąć konto :username?';
$translation['admin.users.management.remove.reject.submit'] = 'Powrót';
$translation['admin.users.management.remove.accept.submit'] = 'Usuń konto';
