<?php defined('SYSPATH') OR die('No direct script access.');

$translation['admin.users.management.edit.company_name.label'] = 'Nazwa firmy';
$translation['admin.users.management.edit.contact_person.fieldset.legend'] = 'Osoba kontaktowa';
$translation['admin.users.management.edit.contact_person.name.label'] = 'Imię';
$translation['admin.users.management.edit.contact_person.name.info_helper'] = 'Imię może składać się wyłącznie z liter i znaków przestankowych';
$translation['admin.users.management.edit.contact_person.surname.label'] = 'Nazwisko';
$translation['admin.users.management.edit.contact_person.surname.info_helper'] = 'Nazwisko może składać się wyłącznie z liter i znaków przestankowych';
$translation['admin.users.management.edit.contact_person.phone_number.label'] = 'Numer telefonu';
$translation['admin.users.management.edit.contact_person.phone_number.info_helper'] = 'Numer telefonu musi być poprawnym numerem telefonu i może składać się z cyfr i rozdzielających myślników';
$translation['admin.users.management.edit.email.label'] = 'Email';
$translation['admin.users.management.edit.email.info_helper'] = 'Wartość pola musi być poprawnym adresem email';
$translation['admin.users.management.edit.firstname.label'] = 'Imię';
$translation['admin.users.management.edit.firstname.info_helper'] = 'Imię może składać się wyłącznie z liter i znaków przestankowych';
$translation['admin.users.management.edit.is_company.label'] = 'Firma';
$translation['admin.users.management.edit.lastname.label'] = 'Nazwisko';
$translation['admin.users.management.edit.lastname.info_helper'] = 'Nazwisko może składać się wyłącznie z liter i znaków przestankowych';
$translation['admin.users.management.edit.living_address.fieldset.legend'] = 'Adres zamieszkania';
$translation['admin.users.management.edit.living_address.street_name.label'] = 'Nazwa ulicy';
$translation['admin.users.management.edit.living_address.street_name.info_helper'] = 'Nazwa ulicy może składać się wyłącznie z liter i znaków przestankowych';
$translation['admin.users.management.edit.living_address.street_number.label'] = 'Numer ulicy';
$translation['admin.users.management.edit.living_address.street_number.info_helper'] = 'Numer ulicy może składać się wyłącznie z cyfr na początku oraz liter rozdzielonych znakiem /';
$translation['admin.users.management.edit.living_address.flat_number.label'] = 'Numer mieszkania';
$translation['admin.users.management.edit.living_address.flat_number.info_helper'] = 'Numer mieszkania może składać się wyłącznie z cyfr na początku i liter na końcu';
$translation['admin.users.management.edit.living_address.place.label'] = 'Miejscowość';
$translation['admin.users.management.edit.living_address.place.info_helper'] = 'Miejscowość może składać się wyłącznie ze znaków alfanumerycznych';
$translation['admin.users.management.edit.living_address.zip_code.label'] = 'Kod pocztowy';
$translation['admin.users.management.edit.living_address.zip_code.info_helper'] = 'Wartość pola musi być poprawnym kodem pocztowym';
$translation['admin.users.management.edit.password.label'] = 'Hasło';
$translation['admin.users.management.edit.password_repeat.label'] = 'Powtórz hasło';
$translation['admin.users.management.edit.phone_number.label'] = 'Numer telefonu';
$translation['admin.users.management.edit.phone_number.info_helper'] = 'Numer telefonu musi być poprawnym numerem telefonu i może składać się z cyfr i rozdzielających myślników';
$translation['admin.users.management.edit.submit'] = 'Zapisz zmiany';
$translation['admin.users.management.edit.title'] = 'Edycja danych użytkownika :username';








