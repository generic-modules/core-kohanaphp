<?php defined('SYSPATH') OR die('No direct script access.');

$translation['admin.users.management.display_list.title'] = 'Lista użytkowników';
$translation['admin.users.management.display_list.table.header.username'] = 'Nazwa użytkownika';
$translation['admin.users.management.display_list.table.header.firstname'] = 'Imię';
$translation['admin.users.management.display_list.table.header.lastname'] = 'Nazwisko';
$translation['admin.users.management.display_list.table.header.phone_number'] = 'Numer telefonu';
$translation['admin.users.management.display_list.table.row.edit'] = 'Edytuj';
$translation['admin.users.management.display_list.table.row.remove'] = 'Usuń';
$translation['admin.users.management.display_list.table.row.reset_password'] = 'Reset hasła';
$translation['admin.users.management.display_list.table.row.assign_roles'] = 'Przypisz role';
$translation['admin.users.management.display_list.table.row.charge'] = 'Doładuj konto';
