<?php defined('SYSPATH') OR die('No direct script access.');

$translation['admin.menu.users.url'] = 'Użytkownicy';
$translation['admin.menu.users_list.url'] = 'Lista użytkowników';
$translation['admin.menu.create_user.url'] = 'Utwórz konto';
$translation['admin.menu.settings.url'] = 'Ustawienia aplikacji';
$translation['admin.menu.logout.url'] = 'Wyloguj';

$translation['admin.menu.cms.url'] = 'CMS';
$translation['admin.menu.display_categories_list.url'] = 'Lista kategorii';
