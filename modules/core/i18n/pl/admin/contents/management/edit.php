<?php defined('SYSPATH') OR die('No direct script access.');

$translation['admin.contents.management.edit.title'] = 'Edycja treści';
$translation['admin.contents.management.edit.title.label'] = 'Tytuł';
$translation['admin.contents.management.edit.lang.pl.title.label'] = 'Tytuł (PL)';
$translation['admin.contents.management.edit.lang.en.title.label'] = 'Tytuł (EN)';
$translation['admin.contents.management.edit.content.label'] = 'Zawartość';
$translation['admin.contents.management.edit.is_published.label'] = 'Opublikuj';
$translation['admin.contents.management.edit.is_published.info_helper'] = 'Zaznaczenie tej opcji spowoduje udostępnienie użytkownikom tej zawartości';
$translation['admin.contents.management.edit.editor.label'] = 'Treść';
$translation['admin.contents.management.edit.lang.pl.editor.label'] = 'Treść (PL)';
$translation['admin.contents.management.edit.lang.en.editor.label'] = 'Treść (EN)';
$translation['admin.contents.management.edit.submit'] = 'Zapisz zmiany';
