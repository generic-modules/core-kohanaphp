<?php defined('SYSPATH') OR die('No direct script access.');

$translation['admin.contents.management.display_list.add_content'] = 'Dodaj';
$translation['admin.contents.management.display_list.list.content_title.header'] = 'Zawartość';
$translation['admin.contents.management.display_list.list.edit.row'] = 'Edytuj';
$translation['admin.contents.management.display_list.list.remove.row'] = 'Usuń';
$translation['admin.contents.management.display_list.title'] = 'Lista zawartości dla kategorii :category_name';
