<?php defined('SYSPATH') OR die('No direct script access.');

$translation['admin.contents.management.create.title'] = 'Tworzenie treści';
$translation['admin.contents.management.create.is_published.label'] = 'Opublikuj';
$translation['admin.contents.management.create.is_published.info_helper'] = 'Zaznaczenie tej opcji spowoduje udostępnienie użytkownikom tej zawartości';
$translation['admin.contents.management.create.content.label'] = 'Treść';
$translation['admin.contents.management.create.title.label'] = 'Tytuł';
$translation['admin.contents.management.create.title.info_helper'] = 'Tytuł może składać się wyłącznie ze znaków alfanumerycznych';
$translation['admin.contents.management.create.submit'] = 'Utwórz treść';
