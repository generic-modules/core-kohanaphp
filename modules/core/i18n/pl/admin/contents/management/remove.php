<?php defined('SYSPATH') OR die('No direct script access.');

$translation['admin.contents.management.remove.title'] = 'Usunięcie zawartości';
$translation['admin.contents.management.remove.warning'] = 'Czy na pewno usunąć zawartość :title?';
$translation['admin.contents.management.remove.reject.submit'] = 'Powrót';
$translation['admin.contents.management.remove.accept.submit'] = "Usuń";
