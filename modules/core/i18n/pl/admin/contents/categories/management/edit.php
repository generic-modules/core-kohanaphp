<?php defined('SYSPATH') OR die('No direct script access.');

$translation['admin.contents.categories.management.edit.title'] = "Edycja kategorii";
$translation['admin.contents.categories.management.edit.data.category.name.label'] = "Nazwa kategorii";
$translation['admin.contents.categories.management.edit.data.category.lang.pl.name.label'] = "Nazwa kategorii (PL)";
$translation['admin.contents.categories.management.edit.data.category.lang.en.name.label'] = "Nazwa kategorii (EN)";
$translation['admin.contents.categories.management.edit.submit'] = "Zapisz zmiany";
