<?php defined('SYSPATH') OR die('No direct script access.');

$translation['admin.contents.categories.management.display_list.title'] = 'Kategorie';
$translation['admin.contents.categories.management.display_list.edit_category.url'] = 'Edytuj';
$translation['admin.contents.categories.management.display_list.display_contents_list.url'] = 'Wyświetl listę zawartości';
$translation['admin.contents.categories.management.display_list.create_content.url'] = 'Dodaj zawartość';
$translation['admin.contents.categories.management.display_list.create_category.url'] = "Utwórz kategorię";
