<?php defined('SYSPATH') OR die('No direct script access.');

$translation['error_message.company_address.regex'] = 'Adres firmy może składać się wyłącznie ze znaków alfanumerycznych';
$translation['error_message.company_bank_account_bank_name.max_length'] = 'Nazwa banku może składać się maksymalnie ze 100 znaków';
$translation['error_message.company_bank_account_bank_name.not_empty'] = 'Nazwa banku nie może być pusta';
$translation['error_message.company_bank_account_bank_name.regex'] = 'Nazwa banku może składać się wyłącznie ze znaków alfanumerycznych';
$translation['error_message.company_bank_account_number.exact_length'] = "Numer konta musi składać się z 26 znaków";
$translation['error_message.company_bank_account_number.not_empty'] = "Numer konta nie może być pusty";
$translation['error_message.company_bank_account_number.regex'] = 'Numer konta musi składać się z 26 cyfr';
$translation['error_message.company_email.email'] = 'Wprowadzono nieprawidłowy adres email';
$translation['error_message.company_email.not_empty'] = 'Email nie może być pusty';
$translation['error_message.company_fax.not_empty'] = 'Faks nie może być pusty';
$translation['error_message.company_fax.phone'] = 'Wprowadzony numer faks jest nieprawidłowy';
$translation['error_message.company_name.max_length'] = 'Nazwa firmy może składać się maksymalnie ze 100 znaków';
$translation['error_message.company_name.regex'] = 'Nazwa firmy może składać się wyłącznie ze znaków alfanumerycznych';
$translation['error_message.company_nip.max_length'] = 'Nip może składać się maksymalnie z 10 znaków';
$translation['error_message.company_nip.regex'] = 'Nip może składać się wyłącznie z maksymalnie 10 cyfr';
$translation['error_message.company_phone_number.not_empty'] = 'Numer telefonu nie może być pusty';
$translation['error_message.company_phone_number.phone'] = 'Wprowadzono nieprawidłowy numer telefonu';
$translation['error_message.company_regon.max_length'] = 'Pole regon może składać się maksymalnie z 9 znaków';
$translation['error_message.company_regon.not_empty'] = 'Pole regon nie może być puste';
$translation['error_message.company_regon.regex'] = 'Pole regon może składać się wyłącznie z maksymalnie 9 cyfr';
$translation['error_message.company_www.not_empty'] = 'Adres www nie może być pusty';
$translation['error_message.company_www.url'] = 'Wprowadzono nieprawidłowy adres www';
$translation['error_message.email.not_match'] = 'Podany email jest nieprawidłowy';
$translation['error_message.email.not_valid'] = 'Wprowadzono nieprawidłowy adres email';
$translation['error_message.email.not_empty'] = 'Pole Email nie może być puste';
$translation['error_message.email.unique'] = 'Pole Email musi być unikalne w całej aplikacji';
$translation['error_message.flat_number.max_length'] = 'Numer mieszkania może mieć maksymalnie 10 znaków';
$translation['error_message.flat_number.regex'] = 'Numer mieszkania może składać się kolejno z cyfr i liter';
$translation['error_message.form.save_failed'] = 'Zapis się nie powiódł';
$translation['error_message.name.max_length'] = 'Pole Imię może mieć maksymalnie 40 znaków';
$translation['error_message.name.min_length'] = 'Pole Imię może mieć minimalnie 3 znaki';
$translation['error_message.name.regex'] = 'Pole Imię może składać się wyłącznie ze znaków alfanumerycznych';
$translation['error_message.nip.max_length'] = 'Nip może składać się maksymalnie z 20 znaków';
$translation['error_message.nip.numeric'] = 'Nip może składać się wyłącznie z cyfr';
$translation['error_message.password.max_length'] = 'Pole Hasło może mieć co najwyżej 40 znaków';
$translation['error_message.password.min_length'] = 'Pole Hasło może mieć co najmniej 8 znaków';
$translation['error_message.password.not_empty'] = 'Pole Hasło nie może być puste';
$translation['error_message.password_repeat.matches'] = 'Pola Hasło i Powtórz Hasło muszą być takie same';
$translation['error_message.phone_number.phone'] = 'Wprowadzono nieprawidłowy numer telefonu';
$translation['error_message.phone_number.digit'] = 'Wprowadzono nieprawidłowy numer telefonu';
$translation['error_message.surname.max_length'] = 'Pole Nazwisko może mieć co najwyżej 40 znaków';
$translation['error_message.surname.min_length'] = 'Pole Nazwisko może mieć co najmniej 3 znaki';
$translation['error_message.surname.regex'] = 'Pole Nazwisko może składać się wyłącznie ze znaków alfanumerycznych';
$translation['error_message.street_name.regex'] = 'Nazwa ulicy może zawierać wyłącznie litery i znaki przestankowe';
$translation['error_message.street_number.max_length'] = 'Nazwa ulicy może mieć maksymalnie 40 znaków';
$translation['error_message.street_number.regex'] = 'Numer ulicy może składać się kolejno z cyfr, znaku / i liter';
$translation['error_message.username.alpha_numeric'] = 'Pole Login może składać się wyłącznie ze znaków alfanumerycznych';
$translation['error_message.username.max_length'] = 'Pole Login może składać się z maksymalnie 20 znaków';
$translation['error_message.username.not_empty'] = 'Pole Login nie może być puste';
$translation['error_message.username.unique'] = 'Pole Login musi być unikalne w całej aplikacji';
$translation['error_message.username_password.not_valid'] = 'Nieprawidłowa nazwa użytkownika lub hasło';
$translation['error_message.place.regex'] = 'Pole Miejscowość może zawierać wyłącznie litery i znaki przestankowe';
$translation['error_message.pesel.digit'] = 'Pole PESEL może zawierać wyłącznie cyfry';
$translation['error_message.pesel.max_length'] = 'Pole PESEL może zawierać do 11 znaków';
$translation['error_message.zip_code.regex'] = 'Wprowadzono nieprawidłowy kod pocztowy';
$translation['message.admin_reset_password_success'] = 'Hasło dla użytkownika :username zostało zresetowane i wysłane na jego główny adres email';
$translation['message.assign_roles_success'] = 'Przypisanie ról zostało zmienione';
$translation['message.change_password_success'] = 'Hasło zostało zmienione';
$translation['message.reset_password_success'] = 'Hasło zostało zresetowane i wysłane na główny adres email';
$translation['message.create_user_success'] = 'Konto zostało utworzone';
$translation['message.remove_user_success'] = 'Konto zostało usunięte';
$translation['message.save_success'] = 'Zmiany zostały zapisane';
$translation['message.email_sent'] = 'Wiadomość została wysłana';
$translation['message.remove_failed_relations'] = 'Usunięcie nie jest możliwe ze względu na istniejące powiązania';

$translation['error_message.title.max_length'] = 'Pole tytuł może mieć maksymalnie 40 znaków';
$translation['error_message.title.not_empty'] = 'Pole tytuł nie może być puste';
$translation['message.create_content_success'] = 'Treść została dodana';
$translation['message.remove_content_success'] = 'Treść została usunięta';

$translation['error_message.content.activation_link_placeholder_required'] = 'Placeholder linku aktywacyjnego jest wymagany';




