<?php defined('SYSPATH') OR die('No direct script access.');

$translation['user.users.management.create.registerform.contactperson.fieldset.legend'] = 'Osoba kontaktowa';
$translation['user.users.management.create.registerform.contactperson.firstname.label'] = 'Imię';
$translation['user.users.management.create.registerform.contactperson.firstname.info_helper'] = 'Imię może składać się wyłącznie z liter i znaków przestankowych';
$translation['user.users.management.create.registerform.contactperson.lastname.label'] = 'Nazwisko';
$translation['user.users.management.create.registerform.contactperson.lastname.info_helper'] = 'Nazwisko może składać się wyłącznie z liter i znaków przestankowych';
$translation['user.users.management.create.registerform.contactperson.phone_number.label'] = 'Numer telefonu';
$translation['user.users.management.create.registerform.contactperson.phone_number.info_helper'] = 'Należy wprowadzić poprawny numer telefonu - same cyfry lub rozdzielany znakami -';
