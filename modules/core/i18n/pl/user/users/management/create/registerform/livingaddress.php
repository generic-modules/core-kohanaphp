<?php defined('SYSPATH') OR die('No direct script access.');

$translation['user.users.management.create.registerform.livingaddress.fieldset.legend'] = 'Adres zamieszkania';
$translation['user.users.management.create.registerform.livingaddress.street_name.label'] = 'Nazwa ulicy';
$translation['user.users.management.create.registerform.livingaddress.street_name.info_helper'] = 'Nazwa ulicy może składać się wyłącznie z liter i znaków przestankowych';
$translation['user.users.management.create.registerform.livingaddress.street_number.label'] = 'Numer ulicy';
$translation['user.users.management.create.registerform.livingaddress.street_number.info_helper'] = 'Numer ulicy może składać się kolejno z cyfr, znaku / i liter';
$translation['user.users.management.create.registerform.livingaddress.flat_number.label'] = 'Numer mieszkania';
$translation['user.users.management.create.registerform.livingaddress.flat_number.info_helper'] = 'Numer mieszkania może składać się kolejno z cyfr i liter';
$translation['user.users.management.create.registerform.livingaddress.place.label'] = 'Miejscowość';
$translation['user.users.management.create.registerform.livingaddress.place.info_helper'] = 'Nazwa miejscowości może składać się wyłącznie z liter i znaków przestankowych';
$translation['user.users.management.create.registerform.livingaddress.zip_code.label'] = 'Kod pocztowy';
$translation['user.users.management.create.registerform.livingaddress.zip_code.info_helper'] = 'Należy wprowadzić poprawny kod pocztowy';
