<?php defined('SYSPATH') OR die('No direct script access.');

$translation['user.users.management.create.registerform.email.label'] = 'Email';
$translation['user.users.management.create.registerform.email.info_helper'] = 'Należy wprowadzić poprawny adres email';
$translation['user.users.management.create.registerform.name.label'] = 'Imię';
$translation['user.users.management.create.registerform.name.info_helper'] = 'Imię może składać się wyłącznie z liter i znaków przestankowych';
$translation['user.users.management.create.registerform.password.label'] = 'Hasło';
$translation['user.users.management.create.registerform.password.info_helper'] = 'Hasło nie może być puste może mieć od 8 do 40 znaków i może składać się ze znaków alfanumerycznych';
$translation['user.users.management.create.registerform.password_repeat.label'] = 'Powtórz Hasło';
$translation['user.users.management.create.registerform.password_repeat.info_helper'] = 'Powtórz Hasło';
$translation['user.users.management.create.registerform.phone_number.label'] = 'Numer telefonu';
$translation['user.users.management.create.registerform.phone_number.info_helper'] = 'Należy wprowadzić poprawny numer telefonu - same cyfry lub rozdzielany znakami -';
$translation['user.users.management.create.registerform.surname.label'] = 'Nazwisko';
$translation['user.users.management.create.registerform.surname.info_helper'] = 'Nazwisko może składać się wyłącznie z liter i znaków przestankowych';
$translation['user.users.management.create.registerform.username.label'] = 'Login';
$translation['user.users.management.create.registerform.username.info_helper'] = 'Login musi być unikalny w całej aplikacji oraz może składać się wyłącznie ze znaków alfanumerycznych';
$translation['user.users.management.create.registerform.firstname.label'] = 'Imię';
$translation['user.users.management.create.registerform.lastname.label'] = 'Nazwisko';
$translation['user.users.management.create.registerform.submit'] = 'Zarejestruj konto';
