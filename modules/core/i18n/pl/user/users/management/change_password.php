<?php defined('SYSPATH') OR die('No direct script access.');

$translation['user.users.management.change_password.title'] = 'Zmiana hasła';
$translation['user.users.management.change_password.data.user.password.label'] = 'Hasło';
$translation['user.users.management.change_password.password.info_helper'] = 'Nowe hasło';
$translation['user.users.management.change_password.data.user.password_repeat.label'] = 'Powtórz Hasło';
$translation['user.users.management.change_password.password_repeat.info_helper'] = 'Musi być takie same jak wprowadzone w polu Hasło';
$translation['user.users.management.change_password.submit'] = 'Zapisz zmiany';
$translation['user.users.management.change.password.password.label'] = 'Hasło';
$translation['user.users.management.change.password.password_repeat.label'] = 'Nowe hasło';
