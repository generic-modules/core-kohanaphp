<?php defined('SYSPATH') OR die('No direct script access.');

$translation['user.users.management.reset_password.title'] = 'Reset hasła';
$translation['user.users.management.reset_password.email.subject'] = 'Reset hasła';
$translation['user.users.management.reset_password.email.content'] = 'Nowe hasło: :password';
$translation['user.users.management.reset_password.email.label'] = 'Email wprowadzony przy rejestracji konta';
$translation['user.users.management.reset_password.email.info_helper'] = 'Email wprowadzony przy rejestracji konta';
$translation['user.users.management.reset_password.email.error_message'] = 'Wprowadzono błędny adres email';
$translation['user.users.management.reset_password.submit'] = 'Zresetuj hasło';

$translation['user.users.auth.reset.password.new_password.label'] = 'Hasło';
$translation['user.users.auth.reset.password.new_password_confirm.label'] = 'Powtórz hasło';
