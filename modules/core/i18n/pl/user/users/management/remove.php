<?php defined('SYSPATH') OR die('No direct script access.');

$translation['user.users.management.remove.title'] = 'Usunięcie konta';
$translation['user.users.management.remove.warning'] = 'Czy na pewno chcesz usunąć swoje konto?';
$translation['user.users.management.remove.reject.submit'] = 'Powrót';
$translation['user.users.management.remove.accept.submit'] = 'Usuń konto';
