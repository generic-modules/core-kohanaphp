<?php defined('SYSPATH') OR die('No direct script access.');

$translation['user.users.auth.reset_password_email.title'] = 'Wpisz adres email';
$translation['user.users.auth.reset.password.email.email.label'] = 'Wpisz adres email';
$translation['user.users.auth.reset.password.email.email_confirm.label'] = 'Wpisz ponownie adres email';
$translation['user.users.auth.reset_password_email.submit'] = 'Wyślij';
