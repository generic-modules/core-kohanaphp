<?php defined('SYSPATH') OR die('No direct script access.');

$translation['user.users.auth.reset_password.title'] = 'Zresetuj hasło';
$translation['user.users.auth.reset_password.enter.new_password.label'] = 'Wpisz nowe hasło';
$translation['user.users.auth.reset_password.enter.new_password_confirm.label'] = 'Powtórz nowe hasło';
$translation['user.users.auth.reset_password.submit'] = 'Zresetuj hasło';
