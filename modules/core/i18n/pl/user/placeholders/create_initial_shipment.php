<?php defined('SYSPATH') OR die('No direct script access.');

$translation['create.initial.shipment.weight.label'] = 'Waga';
$translation['create.initial.shipment.width.label'] = 'Szerokość';
$translation['create.initial.shipment.height.label'] = 'Wysokość';
$translation['create.initial.shipment.length.label'] = 'Długość';
