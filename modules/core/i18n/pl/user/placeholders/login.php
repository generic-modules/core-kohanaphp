<?php defined('SYSPATH') OR die('No direct script access.');

$translation['login.form.create'] = 'Rejestracja';
$translation['login.form.email.label'] = 'Email';
$translation['login.form.email.info_helper'] = 'Email użytkownika podany podczas rejestracji konta';
$translation['login.form.password.label'] = 'Hasło';
$translation['login.form.password.info_helper'] = 'Hasło podane podczas rejestracji konta';
$translation['login.form.reset_password'] = 'Reset hasła';
$translation['login.form.submit'] = 'Zaloguj';
$translation['login.form.username.label'] = 'Login';
$translation['login.form.username.info_helper'] = 'Nazwa użytkownika podana podczas rejestracji konta';
