<?php defined('SYSPATH') OR die('No direct script access.');

$translation['user_panel.edit_user.url'] = 'Edycja konta';
$translation['user_panel.logout.url'] = 'Wyloguj';
$translation['user_panel.user_panel.url'] = 'Panel użytkownika';
