<?php defined('SYSPATH') OR die('No direct script access.');

$translation['contact.form.subject.label'] = 'Napisz do nas';
$translation['contact.form.name_lastname.label'] = 'Imię i nazwisko';
$translation['contact.form.email_adress.label'] = 'Adres e-mail';
$translation['contact.form.message.label'] = 'Wpisz treść';
$translation['contact.form.attachement.label'] = 'Dodaj załącznik';
$translation['contact.form.attachement.submit'] = 'Dodaj załącznik';
$translation['user.index.contact.submit'] = 'Wyślij';
