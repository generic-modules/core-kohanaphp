<?php defined('SYSPATH') OR die('No direct script access.');

$translation['news.article.article.title'] = 'Tytuł';
$translation['news.article.article.update_date'] = 'Data aktualizacji';
$translation['news.article.news.article.list.url'] = 'Lista artykułów';
