<?php defined('SYSPATH') OR die('No direct script access.');

$translation['user.global.app.title'] = 'Generyczna Aplikacja';
$translation['user.global.select.choose'] = 'Wybierz';
$translation['user.global.footer.regulations'] = 'Regulamin';
$translation['user.global.footer.cookies_policy'] = 'Polityka cookies';
$translation['user.global.footer.payment_security'] = 'Bezpieczeństwo płatności';
