<?php defined('SYSPATH') OR die('No direct script access.');

$translation['user.index.contact.title'] = 'Strona kontaktowa';
$translation['user.index.index.article.title.label'] = 'Tytuł';
$translation['user.index.index.article.update_date.label'] = 'Aktualizacja';
$translation['user.index.index.title'] = 'Strona główna';
