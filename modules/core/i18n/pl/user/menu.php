<?php defined('SYSPATH') OR die('No direct script access.');

$translation['user.menu.admin_panel.url'] = 'Panel administracyjny';
$translation['user.menu.change_password.url'] = 'Zmiana hasła';
$translation['user.menu.create_user.url'] = 'Rejestracja';
$translation['user.menu.users.url'] = 'Użytkownicy';
$translation['user.menu.edit_user.url'] = 'Edycja danych użytkownika';
$translation['user.menu.login.url'] = 'Zaloguj';
$translation['user.menu.logout.url'] = 'Wyloguj';
$translation['user.menu.remove_user.url'] = 'Usunięcie konta';
$translation['user.menu.reset_password.url'] = 'Reset hasła';
$translation['user.menu.contact.url'] = 'Kontakt';
$translation['user.menu.news.url'] = 'Aktualności';
$translation['user.user_menu.edit_user.url'] = 'Edycja danych użytkownika';
