<?php defined('SYSPATH') OR die('No direct script access.');

$translation['user.contact.data.contact.subject.label'] = 'Tytuł';
$translation['user.contact.data.contact.subject.info_helper'] = 'Tytuł wiadomości';
$translation['user.contact.data.contact.message.label'] = 'Wiadomość';
$translation['user.contact.data.contact.message.info_helper'] = 'Wiadomość';
$translation['user.contact.attachement.label'] = "Załącznik";
$translation['user.contact.attachement.info_helper'] = "Załącznik";
$translation['user.contact.submit'] = 'Wyślij';
