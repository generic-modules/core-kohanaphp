<div class="panel">
	<div id="contents-management-display_list-title" class="panel-header"><h3 class="panel-title"><?=__($prefix.'title',[':category_name'=>$category->lang(Lang::get())->name])?></h3></div>
	<?if(isset($contents)):?>
		<div id="contents-management-display_list-list" class="tbmargin">
			<table class="">
				<thead>
					<tr>
						<th><?=__($prefix.'list.content_title.header')?></th>
						<th></th>
						<?foreach($contents as $content):?>
							<?if($content->is_deletable):?><th></th><?break;endif?>
						<?endforeach?>
					</tr>
				</thead>
				<tbody>
				<?foreach($contents as $content):?>
					<tr>
						<td><?=$content->lang(Lang::get())->title?></td>
						<td><?=HTML::anchor('/admin/'.$directory.'/contents/management/edit/'.$content->id,__($prefix.'list.edit.row'),[''])?></td>
						<?if($content->is_deletable):?><td><?=HTML::anchor('/admin/'.$directory.'/contents/management/remove/'.$content->id,__($prefix.'list.remove.row'))?></td><?endif?>
					</tr>
				<?endforeach?>
				</tbody>
			</table>
		</div>
	<?endif?>
	<?if($category->is_addable):?>
		<div>
			<?=HTML::anchor('/admin/'.$directory.'/contents/management/create/'.$category->id,__($prefix.'add_content'))?>
		</div>
	<?endif?>
</div>
