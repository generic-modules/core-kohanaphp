<div class="col-xs-1">
	<button type="button" id="menuToggle" class="btn btn-info navbar-btn">
		<?=Bootstrap::glyphicon('menu-hamburger')?>
	</button>
</div>
<div class="col-xs-11">
	<h2>
		<?=__('admin.global.app_title')?>
	</h2>
</div>
