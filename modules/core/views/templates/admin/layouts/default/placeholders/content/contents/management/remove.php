<div class="">
<div class="">
	<div id="contents-management-remove-title" class=""><h3 class=""><?=__($prefix.'title')?></h3></div>
	<div class="">
		<div class="">
			<div id="contents-management-remove-warning" class=""><?=__($prefix.'warning',array(':title'=>$content->title))?></div>
			<div class="">
				<div class="">
					<div id="contents-management-remove-reject-form" class="">
						<?=Form::open($referrer)?>
						<?=Form::submit('remove_reject_form',__($prefix.'reject.submit'),array('class'=>''))?>
						<?=Form::close()?>
					</div>
					<div id="contents-management-remove-accept-form" class="">
						<?=Form::open(null)?>
						<?=Form::hidden('remove','remove')?>
						<?=Form::submit('remove_accept_form',__($prefix.'accept.submit'),array('class'=>''))?>
						<?=Form::close()?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
