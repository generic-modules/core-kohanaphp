<div id="users-management-assign_roles-title"><h2><?=__($prefix.'title',array(':username'=>$user->username))?></h2></div>
<?if($roles):?>
	<div id="users-management-assign_roles-roles_list">
		<?=Form::open()?>
		<?foreach($roles as $role):?>
			<div class="users-management-assign_roles-roles_list_row">
				<?=Form::checkbox('roles_list[]',$role->id,$user->has('roles',$role))?>
				<?=Form::label('role_label',$role->name)?>
			</div>
		<?endforeach?>
		<?=Form::submit('user_roles_submit',__($prefix.'submit'))?>
	</div>
<?endif?>