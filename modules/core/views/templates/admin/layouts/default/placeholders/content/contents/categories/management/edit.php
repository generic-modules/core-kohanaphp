<div class="panel">
	<div id="contents-categories-management-edit-title" class="panel-header"><h3 class="panel-title"><?=__($prefix.'title')?></h3></div>
	<div id="contents-categories-management-edit-form" class="form">
		<?=Form::open()?>
			<?if(Kohana::$config->load('global.languages.multilanguage')):?>
				<?foreach(Kohana::$config->load('global.languages.languages') as $lang):?>
					<?=Form::row('data[category][lang]['.$lang.'][name]',$category->lang($lang)->name,$prefix)?>
				<?endforeach?>
			<?else:?>
				<?=Form::row('data[category][name]',$category->name,$prefix)?>
			<?endif?>
			<div class="tbmargin">
				<?=Form::submit('parameters[edit_category_submit]',__($prefix.'submit'),array('class'=>''))?>
			</div>
		<?=Form::close()?>
	</div>
</div>
