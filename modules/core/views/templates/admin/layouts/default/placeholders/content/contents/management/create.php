<div class="panel">
	<div id="contents-management-create-title" class="panel-header"><h3 class="panel-title"><?=__($prefix.'title')?></h3></div>
	<div id="contents-management-create-form" class="">
		<?=Form::open()?>
			<?=Form::row([$prefix,'title'])?>
			<?=Form::row([$prefix,'is_published'])?>
			<div id="contents-management-create-editor" class="row">
				<?=Form::row([$prefix,'content'])?>
			</div>
			<div class="tbmargin"><?=Form::submit('parameters[create_content_submit]',__($prefix.'submit'),array('class'=>''))?></div>
		<?=Form::close()?>
	</div>
</div>
