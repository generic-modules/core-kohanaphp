<div id="users-auth-login"><h2><?=__($prefix.'title')?></h2></div>
<div id="users-auth-login-form" class="form">
	<?=Form::open()?>
		<?=Form::row('username',null,$prefix)?>
		<?=Form::row('password',null,$prefix)?>
		<div class="submit"><?=Form::submit('login_submit',__($prefix.'submit'))?></div>
	<?=Form::close()?>
</div>
