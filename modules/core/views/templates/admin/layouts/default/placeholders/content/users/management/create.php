<div id="users-management-create-title"><h2><?=__($prefix.'title')?></h2></div>
<div id="users-management-create-form" class="form">
	<?=Form::open()?>
		<?=Form::row([$prefix,'username'],$user->username)?>
		<?if(Kohana::$config->load('models.email.multiple')):?>

		<?else:?>
			<?=Form::row([$prefix,'email'],$user->email)?>
		<?endif?>
		<?if(Kohana::$config->load('models.user.single_role')):?>
			<?=Form::row([$prefix,'role'],[$roles,null])?>
		<?endif?>
		<?=Form::row([$prefix,'password'])?>
		<?=Form::row([$prefix,'password_repeat'])?>
		<?=Form::row([$prefix,'firstname'],$user->firstname)?>
		<?=Form::row([$prefix,'lastname'],$user->lastname)?>
		<?=Form::row([$prefix,'phone_number'],$user->phone_number)?>
		<div class="submit"><?=Form::submit('create_user_submit',__($prefix.'submit'))?></div>
	<?=Form::close()?>
</div>
