<div class="panel">
	<div id="cms-categories-display_list-title" class="panel-header"><h3 class="panel-title"><?=__($prefix.'title')?></h3></div>
		<div id="cms-categories-display_list-list">
			<?=HTML::tree($categories,'name',[
				'urls' => [
					//['url'=>'/admin/cms/contents/categories/management/edit/','label'=>$prefix.'edit_category.url'],
					['url'=>'/admin/cms/contents/management/displaylist/','label'=>$prefix.'display_contents_list.url'],
					//['url'=>'/admin/cms/contents/management/create/','label'=>$prefix.'create_content.url'],
				],
			])?>
		</div>
	<div><?//HTML::anchor('/admin/cms/contents/categories/management/create/', __($prefix.'create_category.url'))?></div>
</div>
