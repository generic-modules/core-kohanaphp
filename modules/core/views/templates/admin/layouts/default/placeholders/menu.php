
<div class="header">
	Menu
</div>


<ul class="list-unstyled components">
	<li class="active">
		<a href="#users" data-toggle="collapse" aria-expanded="false"><?=__('admin.menu.users.url')?></a>
		<ul class="collapse list-unstyled" id="users">
			<li><?=HTML::anchor('/admin/users/management/displaylist',__('admin.menu.users.url'))?></li>
			<li><?=HTML::anchor('/admin/users/management/create',__('admin.menu.create_user.url'))?></li>
		</ul>
	</li>
	<li class="">
		<a href="#cms" data-toggle="collapse" aria-expanded="false"><?=__('admin.menu.cms.url')?></a>
		<ul class="collapse list-unstyled" id="cms">
			<li><?=HTML::anchor('/admin/cms/contents/categories/management/displaylist',__('admin.menu.cms.url'))?></li>
			<li><?=HTML::anchor('/admin/users/management/create',__('admin.menu.create_user.url'))?></li>
		</ul>
	</li>
	<li class=""><?=HTML::anchor('/admin/settings/edit',__('admin.menu.settings.url'))?></li>
	<li class=""><?=HTML::anchor('/logout',__('admin.menu.logout.url'))?></li>
</ul>




<?//=$menu->generate()?>
<?//=$context_menu->generate()?>
