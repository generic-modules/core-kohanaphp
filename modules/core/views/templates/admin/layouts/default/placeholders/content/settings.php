<div id="settings-title"><h2><?=__($prefix.'title')?></h2></div>
<?if(isset($settings)):?>
	<div id="settings-edit-form" class="form">
		<?=Form::open()?>
			<?foreach($settings as $setting):?>
				<?=Form::row([$prefix,$setting->system_name],$setting->value)?>
			<?endforeach?>
		<?=Form::submit('edit_settings_submit',__($prefix.'submit'))?>	
	</div>
<?endif?>
