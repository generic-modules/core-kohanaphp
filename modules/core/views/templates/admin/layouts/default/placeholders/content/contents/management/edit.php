<div class="panel">
	<div id="contents-management-edit-title" class="panel-header"><h3 class="panel-title"><?=__($prefix.'title')?></h3></div>
	<div id="contents-management-edit-form" class="">
		<?if($content->category->id == Kohana::$config->load('models.content.email_templates_category')):?>
			<div class="">
				<fieldset>
					<legend>Placehlodery:</legend><br>
					{{ activation_link }} - link aktywacyjny
				</fieldset>
			</div>
		<?endif?>
		<?=Form::open()?>
			<?if(Kohana::$config->load('global.languages.multilanguage')):?>
				<div class="">
					<?foreach(Kohana::$config->load('global.languages.languages') as $lang):?>
						<?=Form::row([$prefix,'lang.'.$lang.'.title'],$content->lang($lang)->title)?>
					<?endforeach?>
				</div>
			<?else:?>
				<div class="">
					<?=Form::row([$prefix,'title'],$content->lang(Lang::get())->title)?>
				</div>
			<?endif?>
			<?if($content->is_deletable):?>
				<div class="">
					<?=Form::row([$prefix,'is_published'],$content->is_published)?>
				</div>
			<?endif?>
			<?if(Kohana::$config->load('global.languages.multilanguage')):?>
				<div class="">
					<?foreach(Kohana::$config->load('global.languages.languages') as $lang):?>
						<?=Form::row([$prefix,'lang.'.$lang.'content'],$content->lang($lang)->content)?>
					<?endforeach?>
				</div>
			<?else:?>
				<div id="edit-content" class="">
					<?=Form::row([$prefix,'content'],$content->lang(Lang::get())->content)?>
				</div>
			<?endif?>
			<div class="tbmargin"><?=Form::submit('parameters[edit_content_submit]',__($prefix.'submit'),array('class'=>''))?></div>
		<?=Form::close()?>
	</div>
</div>
