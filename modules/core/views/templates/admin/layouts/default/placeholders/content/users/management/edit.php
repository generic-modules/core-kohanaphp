<div id="users-management-edit-title" class="panel-header"><h3 class="panel-title"><?=__($prefix.'title',array(':username'=>$user->username))?></h3></div>
<?=Form::open()?>
	<?=Form::row([$prefix,'is_company'],$user->is_company)?>
	<?=Form::row([$prefix,'company_name'],$user->company_name)?>
	<?=Form::row([$prefix,'email'],$user->email)?>
	<?=Form::row([$prefix,'firstname'],$user->firstname)?>
	<?=Form::row([$prefix,'lastname'],$user->lastname)?>
	<?=Form::row([$prefix,'phone_number'],$user->phone_number)?>
	<?=Form::row([$prefix,'password'])?>
	<?=Form::row([$prefix,'password_repeat'])?>
	<div class="row col-sm-offset-1">
		<?=Form::submit('parameters[edit_user_submit]',__($prefix.'submit'))?>
	</div>
<?=Form::close()?>
