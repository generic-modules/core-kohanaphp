<div class="panel">
	<div id="users-management-display_list-title" class="panel-header"><h3 class="panel-title"><?=__($prefix.'title')?></h3></div>
	<div class="trblmargin">
		<table id="users-management-display_list-users_table" class="table">
			<thead>
				<tr>
					<th><?=__($prefix.'table.header.username')?></th>
					<th><?=__($prefix.'table.header.firstname')?></th>
					<th><?=__($prefix.'table.header.lastname')?></th>
					<th><?=__($prefix.'table.header.phone_number')?></th>
					<th></th>
					<th></th>
					<?if(!Kohana::$config->load('models.user.single_role')):?><th></th><?endif?>
				</tr>
			</thead>
			<tbody>
			<?if(isset($users)):?>
				<?foreach($users as $user):?>
					<tr>
						<td><?=$user->username?></td>
						<td><?=$user->firstname?></td>
						<td><?=$user->lastname?></td>
						<td><?=$user->phone_number?></td>
						<td><?=HTML::anchor('admin/users/management/edit/'.$user->id,Bootstrap::glyphicon('pencil'),['title'=>'edit'])?></td>
						<td><?if($user->is_deletable):?><?=HTML::anchor('admin/users/management/remove/'.$user->id,Bootstrap::glyphicon('trash'),['title'=>'remove'])?><?endif?></td>
						<?if(!Kohana::$config->load('models.user.single_role')):?><td><?=HTML::anchor('admin/users/management/assignroles/'.$user->id,__($prefix.'table.row.assign_roles'))?></td><?endif?>
					</tr>
				<?endforeach?>
			<?endif?>
			</tbody>
		</table>
		<input value="Nowy" class="btn" type="submit">
		<?if(isset($users_pager)): echo HTML::pager($users_pager,'admin/users/management/displaylist/null/'); endif?>
	</div>
</div>
