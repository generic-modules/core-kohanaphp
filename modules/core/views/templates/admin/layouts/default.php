<?=$placeholders['doctype']?>
<html>
	<head><?=$placeholders['head']?></head>
	<body>
		<div id="parameters"><?=View::factory('parameters')?></div>
		<div class="wrapper">
			<nav id="menu"><?=$placeholders['menu']?></nav>
			<div class="container">
				<header id="header" class="row"><?=$placeholders['header']?></header>
				<div class="row">
					<div class="row">
						<div id="messages" class="col-xs-12"><?=View::factory('messages')?></div>
						<div id="content" class="col-xs-12"><?=$content?></div>
					</div>
				</div>
				<footer class="col-xs-12"><?=$placeholders['footer']?></footer>
			</div>
		</div>
	</body>
</html>
<?=Asset::generate('js')?>
