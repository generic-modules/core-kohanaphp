<div id="users-management-change_password-title"><h2><?=__($prefix.'title')?></h2></div>
<div id="users-management-change_password-form" class="">
	<?=Form::open()?>
		<?=Form::row([$prefix,'password'])?>
		<?=Form::row([$prefix,'password_repeat'])?>
		<div class="submit"><?=Form::submit('change_password_submit',__($prefix.'submit'))?></div>
	<?=Form::close()?>
</div>


