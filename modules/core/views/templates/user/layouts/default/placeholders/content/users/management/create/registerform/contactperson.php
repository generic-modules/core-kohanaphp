<fieldset>
	<legend><?=__($prefix.'fieldset.legend')?></legend>
	<?=Form::row([$prefix,'firstname'],$user->contact_person->firstname)?>
	<?=Form::row([$prefix,'lastname'],$user->contact_person->lastname)?>
	<?=Form::row([$prefix,'phone_number'],$user->contact_person->phone_number)?>
</fieldset>
