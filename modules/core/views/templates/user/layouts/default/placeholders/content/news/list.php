<div class="">
	<?=HTML::ul_open()?>
		<?foreach($news as $article):?>
			<?=HTML::li(HTML::anchor('/news/article/'.$article->id,$article->lang(Lang::get())->title))?>
		<?endforeach?>
	<?=HTML::ul_close()?>
</div>
