<?=Form::open()?>
	<?=Form::row([$prefix,'username'],$user->username)?>
	<? if(Kohana::$config->load('forms.components.user_users_management_create_registerform_contactperson.enabled')) echo $placeholders['contactperson']?>
	<? if(Kohana::$config->load('forms.components.user_users_management_create_registerform_livingaddress.enabled')) echo $placeholders['livingaddress']?>
	<?=Form::row([$prefix,'email'],$user->email)?>
	<?=Form::row([$prefix,'password'])?>
	<?=Form::row([$prefix,'password_repeat'])?>
	<?=Form::row([$prefix,'firstname'],$user->firstname)?>
	<?=Form::row([$prefix,'lastname'],$user->lastname)?>
	<?=Form::row([$prefix,'phone_number'],$user->phone_number)?>
	<?=Form::submit('parameters[create_user_submit]',__($prefix.'submit'))?>
<?=Form::close()?>
