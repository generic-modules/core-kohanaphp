<div class="col-sm-12">
	<div class="col-sm-1"><?=$placeholders['logo']?></div>
	<div class="col-sm-4"><h2><a><?=HTML::anchor('/', __('user.global.app.title'))?></a></h2></div>
	<div class="col-sm-2"><?=$placeholders['contact_phone']?></div>
	<div class="col-sm-5">
		<?if(Auth::instance()->logged_in()):?>
			<?=$placeholders['user_panel']?>
		<?else:?>
			<?=$placeholders['login']?>
		<?endif?>
	</div>
</div>
