<?=Form::open(['login_form','/login'])?>
	<div class="col-sm-8">
		<?=Form::row(['login_form','username'])?>
		<?=Form::row(['login_form','email'])?>
		<?=Form::row(['login_form','password'])?>
	</div>
	<div class="col-sm-4">
		<?=Form::submit('login_submit',__('login.form.submit'))?>
		<div class="col-sm-12"><?=HTML::anchor('/users/auth/resetLink', __('login.form.reset_password'))?></div>
		<div class="col-sm-12"><?=HTML::anchor('/users/management/create', __('login.form.create'))?></div>
	</div>
<?=Form::close()?>
