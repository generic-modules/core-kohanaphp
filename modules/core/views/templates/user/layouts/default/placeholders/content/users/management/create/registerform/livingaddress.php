<fieldset>
	<legend><?=__($prefix.'fieldset.legend')?></legend>
	<?=Form::row([$prefix,'street_name'],$user->living_address->street_name)?>
	<?=Form::row([$prefix,'street_number'],$user->living_address->street_number)?>
	<?=Form::row([$prefix,'flat_number'],$user->living_address->flat_number)?>
	<?=Form::row([$prefix,'zip_code'],$user->living_address->zip_code)?>
	<?=Form::row([$prefix,'place'],$user->living_address->place)?>
</fieldset>
