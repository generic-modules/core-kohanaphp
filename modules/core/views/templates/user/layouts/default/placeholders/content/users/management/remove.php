<div id="users-management-remove-title"><h2><?=__($prefix.'title')?></h2></div>
<div id="users-management-remove-warning"><?=__($prefix.'warning')?></div>
<div id="users-management-remove-reject-form">
	<?=Form::open($referrer)?>
	<?=Form::submit('remove_reject_form',__($prefix.'reject.submit'))?>
	<?=Form::close()?>
</div>
<div id="users-management-remove-accept-form">
	<?=Form::open(null)?>
	<?=Form::hidden('remove','remove')?>
	<?=Form::submit('remove_accept_form',__($prefix.'accept.submit'))?>
	<?=Form::close()?>
</div>
