<div>
	<?=__($prefix.'article.title')?>: <?=$article->lang(Lang::get())->title?>
</div>
<div>
	<?=__($prefix.'article.update_date')?>: <?=$article->date('update_date')?>
</div>
<div>
	<?=$article->lang(Lang::get())->content?>
</div>
<div>
	<?=HTML::anchor('news/list',__($prefix.'news.article.list.url'))?>
</div>
