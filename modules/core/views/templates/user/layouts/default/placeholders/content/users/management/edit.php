<h3 class="row"><?=__($prefix.'title')?></h3>
<?=Form::open()?>
	<?if(Kohana::$config->load('models.email.multiple')):?>
		<?=Form::row([$prefix,'email'],$user->emails->getDefault())?>
	<?else:?>
		<?=Form::row([$prefix,'email'],$user->email)?>
	<?endif?>
	<?=Form::row([$prefix,'firstname'],$user->firstname)?>
	<?=Form::row([$prefix,'lastname'],$user->lastname)?>
	<?=Form::row([$prefix,'phone_number'],$user->phone_number)?>
	<?=Form::row([$prefix,'password'])?>
	<?=Form::row([$prefix,'password_repeat'])?>
	<?=Form::submit('parameters[edit_user_submit]',__($prefix.'submit'))?>
<?=Form::close()?>
