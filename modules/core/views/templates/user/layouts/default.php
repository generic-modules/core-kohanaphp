<?=$placeholders['doctype']?>
<html>
	<head><?=$placeholders['head']?></head>
	<body>
		<div class="container">
			<div id="parameters"><?=View::factory('parameters')?></div>
			<header id="header" class="row"><?=$placeholders['header']?></header>
			<div id="content" class="row">
				<div class="col-xs-12"><?=$placeholders['menu']?></div>
				<div class="col-xs-12"><?=$placeholders['context_menu']?></div>
				<div class="col-xs-12"><?=$placeholders['user_menu']?></div>
				<div class="col-xs-12">
					<div id="messages" class="col-xs-12"><?=View::factory('messages')?></div>
					<div id="content" class="col-xs-12"><?=$content?></div>
				</div>
			</div>
			<footer class="col-xs-12"><?=$placeholders['footer']?></footer>
		</div>
	</body>
</html>
<?=Asset::generate('js')?>
