<?$errors = Session::instance()->get_once('errors')?>
<?if(isset($errors)):?>
	<div id="error_messages" class="col-sm-12">
		<?foreach($errors as $field => $errors):?>
			<?if(isset($field)):?>
				<?foreach($errors as $error):?>
					<div class="error message" id="error_message"><a href="#error_message"></a><?=__('error_message.'.$field.'.'.$error)?></div>
				<?endforeach?>
			<?endif?>
		<?endforeach?>
	</div>
<?endif?>
<?$messages = Session::instance()->get_once('messages')?>
<?if(isset($messages)):?>
	<div id="messages" class="col-sm-12">
		<?foreach($messages as $message):?>
			<div class="warning message"><?=__('message.'.$message['message'],$message['parameters'])?></div>
		<?endforeach?>
	</div>
<?endif?>
<div id="ajax-error-messages" class="col-sm-12"></div>
<div id="ajax-messages" class="col-sm-12"></div>
