--PHP-mixed-<?=$random_hash?>  
Content-Type: multipart/alternative; boundary="PHP-alt-<?=$random_hash?>" 

--PHP-alt-<?=$random_hash?>  
Content-Type: text/plain; charset="<?=Kohana::$config->load('emails.default.charset')?>" 
Content-Transfer-Encoding: 7bit

<?=$message?>

--PHP-alt-<?=$random_hash?>  
Content-Type: text/html; charset="<?=Kohana::$config->load('emails.default.charset')?>" 
Content-Transfer-Encoding: 7bit

<?=$message?>

--PHP-alt-<?=$random_hash?>-- 

--PHP-mixed-<?=$random_hash?>
<?if(isset($attachements)):?>
	<?foreach($attachements as $attachement):?>
		Content-Type: <?=$file['type']?>; name=<?=$file['name']?>  
		Content-Transfer-Encoding: base64  
		Content-Disposition: attachment
	
		<?=$file['file']?> 
		--PHP-mixed-<?=$random_hash?>--
	<?endforeach?>
<?endif?>
