<input id="url_base" name="url_base" type="hidden" value="<?=URL::base()?>"/>
<input id="string_translated_choose" name="string_translated_choose" type="hidden" value="<?=__('user.global.select.choose')?>"/>
<?if(Asset_Parameter::count()):?>
	<?foreach(Asset_Parameter::get() as $parameter):?>
		<input id="<?=$parameter['name']?>" name="<?=$parameter['name']?>" type="hidden" value="<?=$parameter['value']?>"/>
	<?endforeach?>
<?endif?>
