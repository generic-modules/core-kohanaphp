<?php defined('SYSPATH') OR die('No direct script access.');

// Routes for Application

// login
Route::set('admin-login', 'admin/login')
	->defaults(array(
		'directory'		=> 'Admin/Users',
		'controller'	=> 'Auth',
		'action'		=> 'Login',
	));
Route::set('login', 'login')
	->defaults(array(
		'directory'		=> 'User/Users',
		'controller'	=> 'Auth',
		'action'		=> 'Login',
	));

// logout
Route::set('logout', 'logout')
	->defaults(array(
		'directory'		=> 'User/Users',
		'controller'	=> 'Auth',
		'action'		=> 'Logout',
	));


// admin/permitions/roles
Route::set('admin-permitions-roles', 'admin/permitions/roles(/<controller>(/<action>(/<id>)))')
	->defaults(array(
		'directory'		=> 'Admin/Users',
		'controller'	=> 'Index',
		'action'		=> 'Index',
	));


// admin/permitions
Route::set('admin-permitions', 'admin/permitions(/<controller>(/<action>(/<id>)))')
	->defaults(array(
		'directory'		=> 'Admin/Users',
		'controller'	=> 'Index',
		'action'		=> 'Index',
	));


// admin/users
Route::set('admin-users', 'admin/users(/<controller>(/<action>(/<id>(/<page>))))')
	->defaults(array(
		'directory'		=> 'Admin/Users',
		'controller'	=> 'Index',
		'action'		=> 'Index',
	));


// admin
Route::set('admin', 'admin(/<controller>(/<action>(/<id>)))')
	->defaults(array(
		'directory'		=> 'Admin',
		'controller'	=> 'Index',
		'action'		=> 'Index',
	));

// user/users/management
Route::set('user-users-management', 'users/management(/<action>(/<id>))')
	->defaults(array(
		'directory'		=> 'User/Users/',
		'controller'	=> 'Management',
	));

// user/users/auth
Route::set('user-users-auth', 'users/auth(/<action>(/<id>))')
	->defaults(array(
		'directory'		=> 'User/Users/',
		'controller'	=> 'Auth',
	));

// user
Route::set('user', '(<controller>(/<action>(/<id>)))')
	->defaults(array(
		'directory'		=> 'User',
		'controller'	=> 'Index',
		'action'		=> 'Index',
	));
	
	
	
	
	// Routes for CMS
	
	// admin/contents/categories
	Route::set('cms-admin-contents-categories', 'admin/cms/contents/categories(/<controller>(/<action>(/<id>(/<page>))))')
	->defaults(array(
			'directory'			=> 'Admin/Contents/Categories/',
			'controller'		=> 'Index',
			'action'			=> 'Index',
			'views_directory'	=> 'contents',
			'content_type'		=> 'content',
	));
	
	// admin/contents
	Route::set('cms-admin-contents', 'admin/cms/contents(/<controller>(/<action>(/<id>(/<page>))))')
	->defaults(array(
			'directory'		=> 'Admin/Contents/',
			'controller'	=> 'Index',
			'action'		=> 'Index',
			'views_directory'	=> 'contents',
			'content_type'		=> 'content',
	));
	
	// user
	Route::set('cms-user', 'cms(/<controller>(/<action>(/<id>)))')
	->defaults(array(
			'directory'		=> 'User',
			'controller'	=> 'Index',
			'action'		=> 'Index',
			'views_directory'	=> 'contents',
			'content_type'		=> 'content',
	));
	

if(Kohana::$config->load('global.mode')!=='prod') Debug::init();
