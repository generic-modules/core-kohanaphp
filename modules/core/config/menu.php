<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
	'main' => array(
		'options' => array(
			'users'			=> ['enabled'=>false],
			'create_user'	=> ['enabled'=>false],
			'login'			=> ['enabled'=>true],
			'admin_panel'	=> ['enabled'=>true],
			'contact_form'	=> ['enabled'=>true],
			'logout'		=> ['enabled'=>true],
		),
	),
	'context' => array(
		'options' => array(
			'users' => array(
				'edit_user'			=> ['enabled'=>false],
				'remove_user'		=> ['enabled'=>false],
				'change_password'	=> ['enabled'=>false],
			),
		),
	),
	'user' => array(
		'options' => array(
			'edit_user' => ['enabled'=>true],
		),
	),
	'admin_main' => array(
		'options' => array(
			'users'		=> ['enabled'=>true],
			'cms'		=> ['enabled'=>true],
			'settings'	=> ['enabled'=>true],
			'logout'	=> ['enabled'=>true],
		),
	),
	'admin_context' => array(
		'options' => array(
			'users' => array(
				'users_list'	=> ['enabled'=>true],
				'create_user'	=> ['enabled'=>true],
			),
			'cms' => array(
				'categories_list' => ['enabled'=>true],
			),
		),
	),	
);
