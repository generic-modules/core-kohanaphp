<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
	'frameworks' => array(
		'bootstrap'	=> ['enabled'=>true],
		'pure'		=> ['enabled'=>true],
	),
);
