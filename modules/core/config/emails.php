<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
	'default' => array(
		'to'			=> 'radoslaw.mazurek.waw@gmail.com',
		'sender_email'	=> 'no-reply@email.pl',
		'sender_name'	=> 'Sender Name',
		'type'			=> 'multipart/mixed',
		'charset'		=> 'UTF-8',
	),
);
