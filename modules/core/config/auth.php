<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'ignored_uris' => array(
		'users/management/create',
		'users/management/resetpassword',
	),
);
