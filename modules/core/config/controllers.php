<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
	'admin' => array(
		'users_management' => array(
			'users_list' => array(
				'records_per_page' => 20,
			),
		),
	),
	'user' => array(
		'users' => array(
			'management' => array(
				'actions' => array(
					'protected' => array(
						'changepassword',
						'edit',
						'remove',
					),
				),
			),
		),
	),
);
