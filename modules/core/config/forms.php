<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
	'row' => array(
		'label' => array(
			'separator' => '',
		),
		'info_helper' => array(
			'display' => false,
			'image_path' => '/modules/app/assets/images/info_helper.gif',
		),
	),
	'components' => array(
		'admin_users_management_edit' => array(
			'inputs' => array(
				'username'			=> ['enabled'=>false],
				'company_name'      => ['enabled'=>false],
				'email'				=> ['enabled'=>true],
				'is_company'        => ['enabled'=>false,'type'=>'checkbox'],
				'role'				=> ['enabled'=>true,'type'=>'select'],
				'password'			=> ['enabled'=>true,'type'=>'password'],
				'password_repeat'	=> ['enabled'=>true,'type'=>'password'],
				'firstname'			=> ['enabled'=>true],
				'lastname'			=> ['enabled'=>true],
				'phone_number'		=> ['enabled'=>false],
			),
		),
		'admin_contents_management_edit' => array(
			'inputs' => array(
				'title'			=> ['enabled'=>true],
				'is_published'	=> ['enabled'=>true,'type'=>'checkbox'],
				'content'		=> ['enabled'=>true,'type'=>'textarea'],
			),
		),
		'admin_contents_management_create' => array(
			'inputs' => array(
				'title'			=> ['enabled'=>true],
				'is_published'	=> ['enabled'=>true,'type'=>'checkbox'],
				'content'		=> ['enabled'=>true,'type'=>'textarea'],
			),
		),
		'contact_form' => array(
			'inputs' => array(
				'subject'		=> ['enabled'=>true],
				'message'		=> ['enabled'=>true,'type'=>'textarea'],
				'attachement'	=> ['enabled'=>true,'type'=>'file'],
			),
		),
		'login_form' => array(
			'inputs' => array(
				'username'	=> ['enabled'=>false,'label'=>'placeholder'],
				'email'		=> ['enabled'=>true,'label'=>'placeholder'],
				'password'	=> ['enabled'=>true,'type'=>'password','label'=>'placeholder'],
			),
		),
		'user_users_auth_reset_password_email' => array(
			'inputs' => array(
				'email' => ['enabled'=>true],
			),
		),
		'user_users_management_create_registerform' => [
			'inputs' => [
				'username'						=> ['enabled'=>false],
				'email'							=> ['enabled'=>true],
				'password'						=> ['enabled'=>true,'type'=>'password'],
				'password_repeat'				=> ['enabled'=>true,'type'=>'password'],
				'firstname'						=> ['enabled'=>true],
				'lastname'						=> ['enabled'=>true],
				'companyname'					=> ['enabled'=>true],
				'phone_number'					=> ['enabled'=>false],
				'street'						=> ['enabled'=>false],
				'house_number'					=> ['enabled'=>false],
				'nopremises'					=> ['enabled'=>false],
				'postalcode'					=> ['enabled'=>false],
				'living_address_street_name'	=> ['enabled'=>false],
				'living_address_street_number'	=> ['enabled'=>false],
				'living_address_flat_number'	=> ['enabled'=>false],
				'living_address_zip_code'		=> ['enabled'=>false],
				'living_address_place'			=> ['enabled'=>false],
			],
		],
		'user_users_management_create_registerform_contactperson' => [
			'enabled' => false,
			'inputs' => [
				'firstname'		=> ['enabled'=>true],
				'lastname'		=> ['enabled'=>true],
				'phone_number'	=> ['enabled'=>true],
			],			
		],
		'user_users_management_create_registerform_livingaddress' => [
			'enabled' => false,
			'inputs' => [
				'street_name'	=> ['enabled'=>true],
				'street_number'	=> ['enabled'=>true],
				'flat_number'	=> ['enabled'=>true],
				'zip_code'		=> ['enabled'=>true],
				'place'			=> ['enabled'=>true],
			],
		],
		'user_users_management_edit' => array(
			'inputs' => array(
				'email'				=> ['enabled'=>true],
				'firstname'			=> ['enabled'=>true],
				'lastname'			=> ['enabled'=>true],
				'phone_number'		=> ['enabled'=>true],
				'edit_user_submit'	=> ['enabled'=>true],
				'password'			=> ['enabled'=>true,'type'=>'password'],
				'password_repeat'	=> ['enabled'=>true,'type'=>'password'],
			),
		),
		'user_users_management_change_password' => array(
			'inputs' => array(
				'password'			=> ['enabled'=>true,'type'=>'password'],
				'password_repeat'	=> ['enabled'=>true,'type'=>'password'],
			),
		),
		'user_shipments_create' => array(
			'inputs' => array(
				'weight'		=>['enabled'=>true],
				'height'		=>['enabled'=>true],
				'width'			=>['enabled'=>true],
				'length'		=>['enabled'=>true],
			),
		),
	),
	'inputs' => array(
		'attachement'		=> array('type' => 'file'),
		'email'				=> array('type' => 'input'),
		'flat_number'		=> array('type' => 'input'),
		'message'			=> array('type' => 'textarea'),
		'name'				=> array('type' => 'input'),
		'password'			=> array('type' => 'password'),
		'password_repeat'	=> array('type' => 'password'),
		'phone_number'		=> array('type' => 'input'),
		'place'				=> array('type' => 'input'),
		'roles'				=> array('type' => 'select'),
		'street_name'		=> array('type' => 'input'),
		'street_number'		=> array('type' => 'input'),
		'surname'			=> array('type' => 'input'),
		'username'			=> array('type' => 'input'),
		'zip_code'			=> array('type' => 'input'),
		'tax_identification_number'			=> array('type' => 'input'),
		'is_published'		=> array('type' => 'checkbox'),
	),
);
