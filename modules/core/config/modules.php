<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
	array('name'=>'app','directory'=>'app','type'=>'magnetar'),
	array('name'=>'auth','directory'=>'auth','type'=>'native'),
	array('name'=>'cache','directory'=>'cache','type'=>'native'),
	//array('name'=>'codebench','directory'=>'codebench','type'=>'native'),
	array('name'=>'database','directory'=>'database','type'=>'native'),
	//array('name'=>'image','directory'=>'image','type'=>'native'),
	//array('name'=>'minion','directory'=>'minion','type'=>'native'),
	array('name'=>'orm','directory'=>'orm','type'=>'native'),
	//array('name'=>'unittest','directory'=>'unittest','type'=>'native'),
	//array('name'=>'userguide','directory'=>'userguide','type'=>'native'),
);
