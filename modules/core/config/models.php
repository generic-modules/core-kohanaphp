<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
	'address' => array(
		'multiple' => false,
		'living_address_id' => 1,
	),
	'email' => array(
		'multiple' => false,
	),
	'role' => array(
		'login_id' => 1,
		'admin_id' => 2,
	),
	'user' => array(
		'reset_password_length' => 40,
		'username' => 'username',
		'remove' => 'remove', // flag/remove
		'single_role' => true,
	),
	'content' => array(
		'email_templates_category' => 1,
	),
);
