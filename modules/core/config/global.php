<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
	'mode' => 'dev', // dev/test/prod
	'languages' => array(
		'default'		=> 'pl',
		'multilanguage'	=> false,
		'languages'		=> array('pl','en'),
	),
);
