<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
	'alpha_numeric_pl' => '/^[,.\-_\/ 0-9a-zA-ZęóąśłżźćńĘÓĄŚŁŻŹĆŃ]+$/u',
	'alpha_pl' => '/^[,.\-_\ a-zA-ZęóąśłżźćńĘÓĄŚŁŻŹĆŃ]+$/u',
	'bank_account_number' => '/^[0-9]{26}?/',
	'flat_number' => '/^([0-9]+|[0-9]+[a-zA-Z]+)$/',
	'nip' => '/^[0-9]{10}?/',
	'regon' => '/^[0-9]{9}?/',
	'street_number' => '/^([0-9]+|[0-9]+\/[a-zA-Z]+)$/',
	'zip_code' => '/^[0-9]{2}-[0-9]{3}?/',
);
