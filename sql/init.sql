/**
 * Init database for module Application.
 * 
 * Creates tables:
 * - users
 * - emails_types
 * - emails
 * - addresses_types
 * - addresses
 * 
 * Inits records:
 * - emails types: domyślny
 * - addresses: zamieszkania, zameldowania
 */

-- requiered by framework --
create table roles(
    id int(11) unsigned not null auto_increment,
    is_active int(1) default 1,
    is_visible int(1) default 1,
    name varchar(32) not null,
    system_name varchar(40),
    hierarchy int,
    description varchar(255),
    primary key (id),
    unique key uniq_name (name)
) engine=InnoDB default charset=utf8;
insert into roles (id, is_visible, hierarchy, name, system_name, description) values(1, 1, 1000, 'login', 'login', 'Login privilages, granted after account confirmation.');
insert into roles (id, is_visible, hierarchy, name, system_name, description) values(2, 1, 2, 'Admin', 'admin', 'Administrative user, has access to everything.');

create table roles_users(
    user_id int(10) unsigned not null,
    role_id int(10) unsigned not null,
    primary key (user_id, role_id),
    key fk_role_id (role_id)
) engine=InnoDB default charset=utf8;

create table functions(
    id int(11) unsigned not null auto_increment,
    name varchar(32) not null,
    primary key (id)
) engine=InnoDB default charset=utf8;

create table roles_function(
    function_id int(10) unsigned not null,
    role_id int(10) unsigned not null,
    primary key (function_id, role_id),
    key fk_function_id (function_id)
) engine=InnoDB default charset=utf8;

create table users(
    id int(11) unsigned not null auto_increment,
    creator_id int(11) unsigned null,
    is_active int(1) default 1,
    is_company int(1) default 0,
    is_deleted int(1) default 0,
    is_deletable int(1) default 1,
    birth_date datetime null,
    birth_place varchar(255) null,
    company_name varchar(255) null,
    create_date datetime null,
    email varchar(255) null,
    firstname varchar(255) null,
    lastname varchar(255) null,
    password varchar(255) null,
    pesel varchar(255) null,
    phone_number varchar(255) null,
    secondname varchar(255) null,
    update_date datetime null,
    username varchar(255) null,
    logins int(10) unsigned not null default 0,
    last_login int(10) unsigned,
    primary key (id),
    foreign key (creator_id) references users(id), 
    unique key uniq_username(username),
    unique key uniq_email(email)
) engine=InnoDB default charset=utf8;

create table user_tokens(
    id int(11) unsigned not null auto_increment,
    user_id int(11) unsigned not null,
    user_agent varchar(40) not null,
    token varchar(40) not null,
    created int(11) unsigned not null,
    expires int(11) unsigned not null,
    primary key (id),
    unique key uniq_token (token),
    key fk_user_id (user_id),
    key expires (expires)
) engine=InnoDB default charset=utf8;

alter table roles_users
    add constraint roles_users_ibfk_1 foreign key (user_id) references users(id) on delete cascade,
    add constraint roles_users_ibfk_2 foreign key (role_id) references roles(id) on delete cascade
;

alter table user_tokens add constraint user_tokens_ibfk_1 foreign key (user_id) references users(id) on delete cascade;
-- ------------------------------

-- contact persons
create table contact_persons(
    id int(11) unsigned not null auto_increment,
    is_active int(1) default 1,
    user_id int(11) unsigned not null,
    firstname varchar(40),
    lastname varchar(40),
    phone_number varchar(20),
    primary key (id),
    foreign key (user_id) references users(id) 
) engine=InnoDB default charset=utf8;

-- emails
create table email_types(
    id int(11) unsigned not null auto_increment,
    is_active int(1) default 1,
    name varchar(40) not null,
    system_name varchar(40) not null,
    primary key (id)
) engine=InnoDB default charset=utf8;
insert into email_types (id, name, system_name) values (1,'private','private');
insert into email_types (id, name, system_name) values (2,'work','work');

create table emails(
    id int(11) unsigned not null auto_increment,
    is_active int(1) default 1,
    type_id int(11) unsigned not null,
    user_id int(11) unsigned not null,
    is_default int(1) default 1,
    address varchar(255),
    primary key (id),
    foreign key (type_id) references email_types(id),
    foreign key (user_id) references users(id)
) engine=InnoDB default charset=utf8;

-- addresses
create table address_types(
    id int(11) unsigned not null auto_increment,
    is_active int(1) default 1,
    name varchar(40) not null,
    system_name varchar(40) not null,
    primary key (id)
) engine=InnoDB default charset=utf8;
insert into address_types (id, name, system_name) values (1,'general','general');
insert into address_types (id, name, system_name) values (2,'living_address','living_address');
insert into address_types (id, name, system_name) values (3,'registered_address','registered_address');

create table addresses(
    id int(11) unsigned not null auto_increment,
    is_active int(1) default 1,
    type_id int(11) unsigned not null,
    user_id int(11) unsigned,
    street varchar(255),
    street_name varchar(255),
    street_number varchar(10),
    flat_number varchar(10),
    place varchar(255),
    country varchar(255),
    zip_code varchar(6),
    primary key (id),
    foreign key (type_id) references address_types(id),
    foreign key (user_id) references users(id)
) engine=InnoDB default charset=utf8;

-- content
create table contents_categories(
    id int(11) unsigned not null auto_increment,
    is_active int(1) default 1,
    parent_id int(11) unsigned default null,
    is_deletable int(1) default 1,
    is_addable int(1) default 0,
    system_name varchar(255) null,
    primary key (id),
    foreign key (parent_id) references contents_categories (id)
) engine=InnoDB default charset=utf8;
insert into contents_categories (id,parent_id,is_deletable,system_name) values(1,null,0,'email_templates');
insert into contents_categories (id,parent_id,is_deletable,system_name) values(2,null,0,'static_content');
insert into contents_categories (id,parent_id,is_deletable,is_addable,system_name) values(3,null,0,1,'news');

create table contents_categories_lang(
    id int(11) unsigned not null auto_increment,
    object_id int(11) unsigned not null,
    lang varchar(10) null,
    name varchar(255) null,
    primary key(id),
    foreign key (object_id) references contents_categories(id)
) engine=InnoDB default charset=utf8;
insert into contents_categories_lang(id,object_id,lang,name) values(1,1,'pl','Szablony Email');
insert into contents_categories_lang(id,object_id,lang,name) values(2,1,'en','Email Templates');
insert into contents_categories_lang(id,object_id,lang,name) values(3,2,'pl','Strony statyczne');
insert into contents_categories_lang(id,object_id,lang,name) values(4,2,'en','Static content');
insert into contents_categories_lang(id,object_id,lang,name) values(5,3,'pl','Aktualności');
insert into contents_categories_lang(id,object_id,lang,name) values(6,3,'en','News');

create table contents(
    id int(11) unsigned not null auto_increment,
    is_active int(1) default 1,
    category_id int(11) unsigned not null,
    is_deletable int(1) default 1,
    is_published int(1) default 0,
    system_name varchar(255) null,
    create_date datetime null,
    update_date datetime null,
    title varchar(255),
    primary key (id),
    foreign key (category_id) references contents_categories (id)
) engine=InnoDB default charset=utf8;
insert into contents (id,category_id,is_published,is_deletable,system_name) values(1,1,1,0,'email_template_activate_user');
insert into contents (id,category_id,is_published,is_deletable,system_name) values(2,2,1,0,'helpline');
insert into contents (id,category_id,is_published,is_deletable,system_name) values(3,2,1,0,'regulations');
insert into contents (id,category_id,is_published,is_deletable,system_name) values(4,2,1,0,'coockies_policy');

create table contents_lang(
    id int(11) unsigned not null auto_increment,
    object_id int(11) unsigned not null,
    lang varchar(10) not null,
    title varchar(255) null,
    content text null,
    primary key(id),
    foreign key (object_id) references contents(id)
) engine=InnoDB default charset=utf8;
insert into contents_lang(id,object_id,lang,title,content) values(1,1,'pl','Aktywacja konta użytkownika','Aby aktywować konto należy uruchomić link: {{ activation_link }}');
insert into contents_lang(id,object_id,lang,title,content) values(2,1,'en','User account activation','In order to activate your account, you need to use this url: {{ activation_link }}');
insert into contents_lang(id,object_id,lang,title,content) values(3,2,'pl','Infolinia','Telefon kontaktowy: 121212121');
insert into contents_lang(id,object_id,lang,title,content) values(4,2,'en','Helpline','Helpline: 121212121');
insert into contents_lang(id,object_id,lang,title,content) values(5,3,'pl','Regulamin','Treść regulaminu.');
insert into contents_lang(id,object_id,lang,title,content) values(6,3,'en','Regulations','Regulations content.');
insert into contents_lang(id,object_id,lang,title,content) values(7,4,'pl','Polityka coockies','Treść polityki coockies.');
insert into contents_lang(id,object_id,lang,title,content) values(8,4,'en','Coockies policy','Coockies policy content.');

-- countries
create table countries(
    id int(11) unsigned not null auto_increment,
    is_active int(1) default 1,
    name varchar(255) null,
    code varchar(10) null,
    system_name varchar(255),
    value varchar(255),
    primary key (id)    
) engine=InnoDB default charset=utf8;
INSERT INTO `countries` (id,code,name,system_name) VALUES (1, 'AF', 'Afghanistan','af');
INSERT INTO `countries` (id,code,name,system_name) VALUES (2, 'AL', 'Albania','al');
INSERT INTO `countries` (id,code,name,system_name) VALUES (3, 'DZ', 'Algeria','dz');
INSERT INTO `countries` (id,code,name,system_name) VALUES (4, 'DS', 'American Samoa','ds');
INSERT INTO `countries` (id,code,name,system_name) VALUES (5, 'AD', 'Andorra','ad');
INSERT INTO `countries` (id,code,name,system_name) VALUES (6, 'AO', 'Angola','ao');
INSERT INTO `countries` (id,code,name,system_name) VALUES (7, 'AI', 'Anguilla','ai');
INSERT INTO `countries` (id,code,name,system_name) VALUES (8, 'AQ', 'Antarctica','aq');
INSERT INTO `countries` (id,code,name,system_name) VALUES (9, 'AG', 'Antigua and Barbuda','ag');
INSERT INTO `countries` (id,code,name,system_name) VALUES (10, 'AR', 'Argentina','ar');
INSERT INTO `countries` (id,code,name,system_name) VALUES (11, 'AM', 'Armenia','am');
INSERT INTO `countries` (id,code,name,system_name) VALUES (12, 'AW', 'Aruba','aw');
INSERT INTO `countries` (id,code,name,system_name) VALUES (13, 'AU', 'Australia','au');
INSERT INTO `countries` (id,code,name,system_name) VALUES (14, 'AT', 'Austria','at');
INSERT INTO `countries` (id,code,name,system_name) VALUES (15, 'AZ', 'Azerbaijan','az');
INSERT INTO `countries` (id,code,name,system_name) VALUES (16, 'BS', 'Bahamas','bs');
INSERT INTO `countries` (id,code,name,system_name) VALUES (17, 'BH', 'Bahrain','bh');
INSERT INTO `countries` (id,code,name,system_name) VALUES (18, 'BD', 'Bangladesh','bd');
INSERT INTO `countries` (id,code,name,system_name) VALUES (19, 'BB', 'Barbados','bb');
INSERT INTO `countries` (id,code,name,system_name) VALUES (20, 'BY', 'Belarus','by');
INSERT INTO `countries` (id,code,name,system_name) VALUES (21, 'BE', 'Belgium','be');
INSERT INTO `countries` (id,code,name,system_name) VALUES (22, 'BZ', 'Belize','bz');
INSERT INTO `countries` (id,code,name,system_name) VALUES (23, 'BJ', 'Benin','bj');
INSERT INTO `countries` (id,code,name,system_name) VALUES (24, 'BM', 'Bermuda','bm');
INSERT INTO `countries` (id,code,name,system_name) VALUES (25, 'BT', 'Bhutan','bt');
INSERT INTO `countries` (id,code,name,system_name) VALUES (26, 'BO', 'Bolivia','bo');
INSERT INTO `countries` (id,code,name,system_name) VALUES (27, 'BA', 'Bosnia and Herzegovina','ba');
INSERT INTO `countries` (id,code,name,system_name) VALUES (28, 'BW', 'Botswana','bw');
INSERT INTO `countries` (id,code,name,system_name) VALUES (29, 'BV', 'Bouvet Island','bv');
INSERT INTO `countries` (id,code,name,system_name) VALUES (30, 'BR', 'Brazil','br');
INSERT INTO `countries` (id,code,name,system_name) VALUES (31, 'IO', 'British Indian Ocean Territory','io');
INSERT INTO `countries` (id,code,name,system_name) VALUES (32, 'BN', 'Brunei Darussalam','bn');
INSERT INTO `countries` (id,code,name,system_name) VALUES (33, 'BG', 'Bulgaria','bg');
INSERT INTO `countries` (id,code,name,system_name) VALUES (34, 'BF', 'Burkina Faso','bf');
INSERT INTO `countries` (id,code,name,system_name) VALUES (35, 'BI', 'Burundi','bi');
INSERT INTO `countries` (id,code,name,system_name) VALUES (36, 'KH', 'Cambodia','kh');
INSERT INTO `countries` (id,code,name,system_name) VALUES (37, 'CM', 'Cameroon','cm');
INSERT INTO `countries` (id,code,name,system_name) VALUES (38, 'CA', 'Canada','ca');
INSERT INTO `countries` (id,code,name,system_name) VALUES (39, 'CV', 'Cape Verde','cv');
INSERT INTO `countries` (id,code,name,system_name) VALUES (40, 'KY', 'Cayman Islands','ky');
INSERT INTO `countries` (id,code,name,system_name) VALUES (41, 'CF', 'Central African Republic','cf');
INSERT INTO `countries` (id,code,name,system_name) VALUES (42, 'TD', 'Chad','td');
INSERT INTO `countries` (id,code,name,system_name) VALUES (43, 'CL', 'Chile','cl');
INSERT INTO `countries` (id,code,name,system_name) VALUES (44, 'CN', 'China','cn');
INSERT INTO `countries` (id,code,name,system_name) VALUES (45, 'CX', 'Christmas Island','cx');
INSERT INTO `countries` (id,code,name,system_name) VALUES (46, 'CC', 'Cocos (Keeling) Islands','cc');
INSERT INTO `countries` (id,code,name,system_name) VALUES (47, 'CO', 'Colombia','co');
INSERT INTO `countries` (id,code,name,system_name) VALUES (48, 'KM', 'Comoros','km');
INSERT INTO `countries` (id,code,name,system_name) VALUES (49, 'CG', 'Congo','cg');
INSERT INTO `countries` (id,code,name,system_name) VALUES (50, 'CK', 'Cook Islands','ck');
INSERT INTO `countries` (id,code,name,system_name) VALUES (51, 'CR', 'Costa Rica','cr');
INSERT INTO `countries` (id,code,name,system_name) VALUES (52, 'HR', 'Croatia (Hrvatska)','hr');
INSERT INTO `countries` (id,code,name,system_name) VALUES (53, 'CU', 'Cuba','cu');
INSERT INTO `countries` (id,code,name,system_name) VALUES (54, 'CY', 'Cyprus','cy');
INSERT INTO `countries` (id,code,name,system_name) VALUES (55, 'CZ', 'Czech Republic','cz');
INSERT INTO `countries` (id,code,name,system_name) VALUES (56, 'DK', 'Denmark','dk');
INSERT INTO `countries` (id,code,name,system_name) VALUES (57, 'DJ', 'Djibouti','dj');
INSERT INTO `countries` (id,code,name,system_name) VALUES (58, 'DM', 'Dominica','dm');
INSERT INTO `countries` (id,code,name,system_name) VALUES (59, 'DO', 'Dominican Republic','do');
INSERT INTO `countries` (id,code,name,system_name) VALUES (60, 'TP', 'East Timor','tp');
INSERT INTO `countries` (id,code,name,system_name) VALUES (61, 'EC', 'Ecuador','ec');
INSERT INTO `countries` (id,code,name,system_name) VALUES (62, 'EG', 'Egypt','eg');
INSERT INTO `countries` (id,code,name,system_name) VALUES (63, 'SV', 'El Salvador','sv');
INSERT INTO `countries` (id,code,name,system_name) VALUES (64, 'GQ', 'Equatorial Guinea','gq');
INSERT INTO `countries` (id,code,name,system_name) VALUES (65, 'ER', 'Eritrea','er');
INSERT INTO `countries` (id,code,name,system_name) VALUES (66, 'EE', 'Estonia','ee');
INSERT INTO `countries` (id,code,name,system_name) VALUES (67, 'ET', 'Ethiopia','et');
INSERT INTO `countries` (id,code,name,system_name) VALUES (68, 'FK', 'Falkland Islands (Malvinas)','fk');
INSERT INTO `countries` (id,code,name,system_name) VALUES (69, 'FO', 'Faroe Islands','fo');
INSERT INTO `countries` (id,code,name,system_name) VALUES (70, 'FJ', 'Fiji','fj');
INSERT INTO `countries` (id,code,name,system_name) VALUES (71, 'FI', 'Finland','fi');
INSERT INTO `countries` (id,code,name,system_name) VALUES (72, 'FR', 'France','fr');
INSERT INTO `countries` (id,code,name,system_name) VALUES (73, 'FX', 'France, Metropolitan','fx');
INSERT INTO `countries` (id,code,name,system_name) VALUES (74, 'GF', 'French Guiana','gf');
INSERT INTO `countries` (id,code,name,system_name) VALUES (75, 'PF', 'French Polynesia','pf');
INSERT INTO `countries` (id,code,name,system_name) VALUES (76, 'TF', 'French Southern Territories','tf');
INSERT INTO `countries` (id,code,name,system_name) VALUES (77, 'GA', 'Gabon','ga');
INSERT INTO `countries` (id,code,name,system_name) VALUES (78, 'GM', 'Gambia','gm');
INSERT INTO `countries` (id,code,name,system_name) VALUES (79, 'GE', 'Georgia','ge');
INSERT INTO `countries` (id,code,name,system_name) VALUES (80, 'DE', 'Germany','de');
INSERT INTO `countries` (id,code,name,system_name) VALUES (81, 'GH', 'Ghana','gh');
INSERT INTO `countries` (id,code,name,system_name) VALUES (82, 'GI', 'Gibraltar','gi');
INSERT INTO `countries` (id,code,name,system_name) VALUES (83, 'GK', 'Guernsey','gk');
INSERT INTO `countries` (id,code,name,system_name) VALUES (84, 'GR', 'Greece','gr');
INSERT INTO `countries` (id,code,name,system_name) VALUES (85, 'GL', 'Greenland','gl');
INSERT INTO `countries` (id,code,name,system_name) VALUES (86, 'GD', 'Grenada','gd');
INSERT INTO `countries` (id,code,name,system_name) VALUES (87, 'GP', 'Guadeloupe','gp');
INSERT INTO `countries` (id,code,name,system_name) VALUES (88, 'GU', 'Guam','gu');
INSERT INTO `countries` (id,code,name,system_name) VALUES (89, 'GT', 'Guatemala','gt');
INSERT INTO `countries` (id,code,name,system_name) VALUES (90, 'GN', 'Guinea','gn');
INSERT INTO `countries` (id,code,name,system_name) VALUES (91, 'GW', 'Guinea-Bissau','gw');
INSERT INTO `countries` (id,code,name,system_name) VALUES (92, 'GY', 'Guyana','gy');
INSERT INTO `countries` (id,code,name,system_name) VALUES (93, 'HT', 'Haiti','ht');
INSERT INTO `countries` (id,code,name,system_name) VALUES (94, 'HM', 'Heard and Mc Donald Islands','hm');
INSERT INTO `countries` (id,code,name,system_name) VALUES (95, 'HN', 'Honduras','hn');
INSERT INTO `countries` (id,code,name,system_name) VALUES (96, 'HK', 'Hong Kong','hk');
INSERT INTO `countries` (id,code,name,system_name) VALUES (97, 'HU', 'Hungary','hu');
INSERT INTO `countries` (id,code,name,system_name) VALUES (98, 'IS', 'Iceland','is');
INSERT INTO `countries` (id,code,name,system_name) VALUES (99, 'IN', 'India','in');
INSERT INTO `countries` (id,code,name,system_name) VALUES (100, 'IM', 'Isle of Man','im');
INSERT INTO `countries` (id,code,name,system_name) VALUES (101, 'ID', 'Indonesia','id');
INSERT INTO `countries` (id,code,name,system_name) VALUES (102, 'IR', 'Iran (Islamic Republic of)','ir');
INSERT INTO `countries` (id,code,name,system_name) VALUES (103, 'IQ', 'Iraq','iq');
INSERT INTO `countries` (id,code,name,system_name) VALUES (104, 'IE', 'Ireland','ie');
INSERT INTO `countries` (id,code,name,system_name) VALUES (105, 'IL', 'Israel','il');
INSERT INTO `countries` (id,code,name,system_name) VALUES (106, 'IT', 'Italy','it');
INSERT INTO `countries` (id,code,name,system_name) VALUES (107, 'CI', 'Ivory Coast','ci');
INSERT INTO `countries` (id,code,name,system_name) VALUES (108, 'JE', 'Jersey','je');
INSERT INTO `countries` (id,code,name,system_name) VALUES (109, 'JM', 'Jamaica','jm');
INSERT INTO `countries` (id,code,name,system_name) VALUES (110, 'JP', 'Japan','jp');
INSERT INTO `countries` (id,code,name,system_name) VALUES (111, 'JO', 'Jordan','jo');
INSERT INTO `countries` (id,code,name,system_name) VALUES (112, 'KZ', 'Kazakhstan','kz');
INSERT INTO `countries` (id,code,name,system_name) VALUES (113, 'KE', 'Kenya','ke');
INSERT INTO `countries` (id,code,name,system_name) VALUES (114, 'KI', 'Kiribati','ki');
INSERT INTO `countries` (id,code,name,system_name) VALUES (115, 'KP', 'Korea, Democratic People''s Republic of','kp');
INSERT INTO `countries` (id,code,name,system_name) VALUES (116, 'KR', 'Korea, Republic of','kr');
INSERT INTO `countries` (id,code,name,system_name) VALUES (117, 'XK', 'Kosovo','xk');
INSERT INTO `countries` (id,code,name,system_name) VALUES (118, 'KW', 'Kuwait','kw');
INSERT INTO `countries` (id,code,name,system_name) VALUES (119, 'KG', 'Kyrgyzstan','kg');
INSERT INTO `countries` (id,code,name,system_name) VALUES (120, 'LA', 'Lao People''s Democratic Republic','la');
INSERT INTO `countries` (id,code,name,system_name) VALUES (121, 'LV', 'Latvia','lv');
INSERT INTO `countries` (id,code,name,system_name) VALUES (122, 'LB', 'Lebanon','lb');
INSERT INTO `countries` (id,code,name,system_name) VALUES (123, 'LS', 'Lesotho','ls');
INSERT INTO `countries` (id,code,name,system_name) VALUES (124, 'LR', 'Liberia','lr');
INSERT INTO `countries` (id,code,name,system_name) VALUES (125, 'LY', 'Libyan Arab Jamahiriya','ly');
INSERT INTO `countries` (id,code,name,system_name) VALUES (126, 'LI', 'Liechtenstein','li');
INSERT INTO `countries` (id,code,name,system_name) VALUES (127, 'LT', 'Lithuania','lt');
INSERT INTO `countries` (id,code,name,system_name) VALUES (128, 'LU', 'Luxembourg','lu');
INSERT INTO `countries` (id,code,name,system_name) VALUES (129, 'MO', 'Macau','mo');
INSERT INTO `countries` (id,code,name,system_name) VALUES (130, 'MK', 'Macedonia','mk');
INSERT INTO `countries` (id,code,name,system_name) VALUES (131, 'MG', 'Madagascar','mg');
INSERT INTO `countries` (id,code,name,system_name) VALUES (132, 'MW', 'Malawi','mw');
INSERT INTO `countries` (id,code,name,system_name) VALUES (133, 'MY', 'Malaysia','my');
INSERT INTO `countries` (id,code,name,system_name) VALUES (134, 'MV', 'Maldives','mv');
INSERT INTO `countries` (id,code,name,system_name) VALUES (135, 'ML', 'Mali','ml');
INSERT INTO `countries` (id,code,name,system_name) VALUES (136, 'MT', 'Malta','mt');
INSERT INTO `countries` (id,code,name,system_name) VALUES (137, 'MH', 'Marshall Islands','mh');
INSERT INTO `countries` (id,code,name,system_name) VALUES (138, 'MQ', 'Martinique','mq');
INSERT INTO `countries` (id,code,name,system_name) VALUES (139, 'MR', 'Mauritania','mr');
INSERT INTO `countries` (id,code,name,system_name) VALUES (140, 'MU', 'Mauritius','mu');
INSERT INTO `countries` (id,code,name,system_name) VALUES (141, 'TY', 'Mayotte','ty');
INSERT INTO `countries` (id,code,name,system_name) VALUES (142, 'MX', 'Mexico','mx');
INSERT INTO `countries` (id,code,name,system_name) VALUES (143, 'FM', 'Micronesia, Federated States of','fm');
INSERT INTO `countries` (id,code,name,system_name) VALUES (144, 'MD', 'Moldova, Republic of','md');
INSERT INTO `countries` (id,code,name,system_name) VALUES (145, 'MC', 'Monaco','mc');
INSERT INTO `countries` (id,code,name,system_name) VALUES (146, 'MN', 'Mongolia','mn');
INSERT INTO `countries` (id,code,name,system_name) VALUES (147, 'ME', 'Montenegro','me');
INSERT INTO `countries` (id,code,name,system_name) VALUES (148, 'MS', 'Montserrat','ms');
INSERT INTO `countries` (id,code,name,system_name) VALUES (149, 'MA', 'Morocco','ma');
INSERT INTO `countries` (id,code,name,system_name) VALUES (150, 'MZ', 'Mozambique','mz');
INSERT INTO `countries` (id,code,name,system_name) VALUES (151, 'MM', 'Myanmar','mm');
INSERT INTO `countries` (id,code,name,system_name) VALUES (152, 'NA', 'Namibia','na');
INSERT INTO `countries` (id,code,name,system_name) VALUES (153, 'NR', 'Nauru','nr');
INSERT INTO `countries` (id,code,name,system_name) VALUES (154, 'NP', 'Nepal','np');
INSERT INTO `countries` (id,code,name,system_name) VALUES (155, 'NL', 'Netherlands','nl');
INSERT INTO `countries` (id,code,name,system_name) VALUES (156, 'AN', 'Netherlands Antilles','an');
INSERT INTO `countries` (id,code,name,system_name) VALUES (157, 'NC', 'New Caledonia','nc');
INSERT INTO `countries` (id,code,name,system_name) VALUES (158, 'NZ', 'New Zealand','nz');
INSERT INTO `countries` (id,code,name,system_name) VALUES (159, 'NI', 'Nicaragua','ni');
INSERT INTO `countries` (id,code,name,system_name) VALUES (160, 'NE', 'Niger','ne');
INSERT INTO `countries` (id,code,name,system_name) VALUES (161, 'NG', 'Nigeria','ng');
INSERT INTO `countries` (id,code,name,system_name) VALUES (162, 'NU', 'Niue','nu');
INSERT INTO `countries` (id,code,name,system_name) VALUES (163, 'NF', 'Norfolk Island','nf');
INSERT INTO `countries` (id,code,name,system_name) VALUES (164, 'MP', 'Northern Mariana Islands','mp');
INSERT INTO `countries` (id,code,name,system_name) VALUES (165, 'NO', 'Norway','no');
INSERT INTO `countries` (id,code,name,system_name) VALUES (166, 'OM', 'Oman','om');
INSERT INTO `countries` (id,code,name,system_name) VALUES (167, 'PK', 'Pakistan','pk');
INSERT INTO `countries` (id,code,name,system_name) VALUES (168, 'PW', 'Palau','pw');
INSERT INTO `countries` (id,code,name,system_name) VALUES (169, 'PS', 'Palestine','ps');
INSERT INTO `countries` (id,code,name,system_name) VALUES (170, 'PA', 'Panama','pa');
INSERT INTO `countries` (id,code,name,system_name) VALUES (171, 'PG', 'Papua New Guinea','pg');
INSERT INTO `countries` (id,code,name,system_name) VALUES (172, 'PY', 'Paraguay','py');
INSERT INTO `countries` (id,code,name,system_name) VALUES (173, 'PE', 'Peru','pe');
INSERT INTO `countries` (id,code,name,system_name) VALUES (174, 'PH', 'Philippines','ph');
INSERT INTO `countries` (id,code,name,system_name) VALUES (175, 'PN', 'Pitcairn','pn');
INSERT INTO `countries` (id,code,name,system_name) VALUES (176, 'PL', 'Poland','pl');
INSERT INTO `countries` (id,code,name,system_name) VALUES (177, 'PT', 'Portugal','pt');
INSERT INTO `countries` (id,code,name,system_name) VALUES (178, 'PR', 'Puerto Rico','pr');
INSERT INTO `countries` (id,code,name,system_name) VALUES (179, 'QA', 'Qatar','qa');
INSERT INTO `countries` (id,code,name,system_name) VALUES (180, 'RE', 'Reunion','re');
INSERT INTO `countries` (id,code,name,system_name) VALUES (181, 'RO', 'Romania','ro');
INSERT INTO `countries` (id,code,name,system_name) VALUES (182, 'RU', 'Russian Federation','ru');
INSERT INTO `countries` (id,code,name,system_name) VALUES (183, 'RW', 'Rwanda','rw');
INSERT INTO `countries` (id,code,name,system_name) VALUES (184, 'KN', 'Saint Kitts and Nevis','kn');
INSERT INTO `countries` (id,code,name,system_name) VALUES (185, 'LC', 'Saint Lucia','lc');
INSERT INTO `countries` (id,code,name,system_name) VALUES (186, 'VC', 'Saint Vincent and the Grenadines','vc');
INSERT INTO `countries` (id,code,name,system_name) VALUES (187, 'WS', 'Samoa','ws');
INSERT INTO `countries` (id,code,name,system_name) VALUES (188, 'SM', 'San Marino','sm');
INSERT INTO `countries` (id,code,name,system_name) VALUES (189, 'ST', 'Sao Tome and Principe','st');
INSERT INTO `countries` (id,code,name,system_name) VALUES (190, 'SA', 'Saudi Arabia','sa');
INSERT INTO `countries` (id,code,name,system_name) VALUES (191, 'SN', 'Senegal','sn');
INSERT INTO `countries` (id,code,name,system_name) VALUES (192, 'RS', 'Serbia','rs');
INSERT INTO `countries` (id,code,name,system_name) VALUES (193, 'SC', 'Seychelles','sc');
INSERT INTO `countries` (id,code,name,system_name) VALUES (194, 'SL', 'Sierra Leone','sl');
INSERT INTO `countries` (id,code,name,system_name) VALUES (195, 'SG', 'Singapore','sg');
INSERT INTO `countries` (id,code,name,system_name) VALUES (196, 'SK', 'Slovakia','sk');
INSERT INTO `countries` (id,code,name,system_name) VALUES (197, 'SI', 'Slovenia','si');
INSERT INTO `countries` (id,code,name,system_name) VALUES (198, 'SB', 'Solomon Islands','sb');
INSERT INTO `countries` (id,code,name,system_name) VALUES (199, 'SO', 'Somalia','so');
INSERT INTO `countries` (id,code,name,system_name) VALUES (200, 'ZA', 'South Africa','za');
INSERT INTO `countries` (id,code,name,system_name) VALUES (201, 'GS', 'South Georgia South Sandwich Islands','gs');
INSERT INTO `countries` (id,code,name,system_name) VALUES (202, 'ES', 'Spain','es');
INSERT INTO `countries` (id,code,name,system_name) VALUES (203, 'LK', 'Sri Lanka','lk');
INSERT INTO `countries` (id,code,name,system_name) VALUES (204, 'SH', 'St. Helena','sh');
INSERT INTO `countries` (id,code,name,system_name) VALUES (205, 'PM', 'St. Pierre and Miquelon','pm');
INSERT INTO `countries` (id,code,name,system_name) VALUES (206, 'SD', 'Sudan','sd');
INSERT INTO `countries` (id,code,name,system_name) VALUES (207, 'SR', 'Suriname','sr');
INSERT INTO `countries` (id,code,name,system_name) VALUES (208, 'SJ', 'Svalbard and Jan Mayen Islands','sj');
INSERT INTO `countries` (id,code,name,system_name) VALUES (209, 'SZ', 'Swaziland','sz');
INSERT INTO `countries` (id,code,name,system_name) VALUES (210, 'SE', 'Sweden','se');
INSERT INTO `countries` (id,code,name,system_name) VALUES (211, 'CH', 'Switzerland','ch');
INSERT INTO `countries` (id,code,name,system_name) VALUES (212, 'SY', 'Syrian Arab Republic','sy');
INSERT INTO `countries` (id,code,name,system_name) VALUES (213, 'TW', 'Taiwan','tw');
INSERT INTO `countries` (id,code,name,system_name) VALUES (214, 'TJ', 'Tajikistan','tj');
INSERT INTO `countries` (id,code,name,system_name) VALUES (215, 'TZ', 'Tanzania, United Republic of','tz');
INSERT INTO `countries` (id,code,name,system_name) VALUES (216, 'TH', 'Thailand','th');
INSERT INTO `countries` (id,code,name,system_name) VALUES (217, 'TG', 'Togo','tg');
INSERT INTO `countries` (id,code,name,system_name) VALUES (218, 'TK', 'Tokelau','tk');
INSERT INTO `countries` (id,code,name,system_name) VALUES (219, 'TO', 'Tonga','to');
INSERT INTO `countries` (id,code,name,system_name) VALUES (220, 'TT', 'Trinidad and Tobago','tt');
INSERT INTO `countries` (id,code,name,system_name) VALUES (221, 'TN', 'Tunisia','tn');
INSERT INTO `countries` (id,code,name,system_name) VALUES (222, 'TR', 'Turkey','tr');
INSERT INTO `countries` (id,code,name,system_name) VALUES (223, 'TM', 'Turkmenistan','tm');
INSERT INTO `countries` (id,code,name,system_name) VALUES (224, 'TC', 'Turks and Caicos Islands','tc');
INSERT INTO `countries` (id,code,name,system_name) VALUES (225, 'TV', 'Tuvalu','tv');
INSERT INTO `countries` (id,code,name,system_name) VALUES (226, 'UG', 'Uganda','ug');
INSERT INTO `countries` (id,code,name,system_name) VALUES (227, 'UA', 'Ukraine','ua');
INSERT INTO `countries` (id,code,name,system_name) VALUES (228, 'AE', 'United Arab Emirates','ae');
INSERT INTO `countries` (id,code,name,system_name) VALUES (229, 'GB', 'United Kingdom','gb');
INSERT INTO `countries` (id,code,name,system_name) VALUES (230, 'US', 'United States','us');
INSERT INTO `countries` (id,code,name,system_name) VALUES (231, 'UM', 'United States minor outlying islands','um');
INSERT INTO `countries` (id,code,name,system_name) VALUES (232, 'UY', 'Uruguay','uy');
INSERT INTO `countries` (id,code,name,system_name) VALUES (233, 'UZ', 'Uzbekistan','uz');
INSERT INTO `countries` (id,code,name,system_name) VALUES (234, 'VU', 'Vanuatu','vu');
INSERT INTO `countries` (id,code,name,system_name) VALUES (235, 'VA', 'Vatican City State','va');
INSERT INTO `countries` (id,code,name,system_name) VALUES (236, 'VE', 'Venezuela','ve');
INSERT INTO `countries` (id,code,name,system_name) VALUES (237, 'VN', 'Vietnam','vn');
INSERT INTO `countries` (id,code,name,system_name) VALUES (238, 'VG', 'Virgin Islands (British)','vg');
INSERT INTO `countries` (id,code,name,system_name) VALUES (239, 'VI', 'Virgin Islands (U.S.)','vi');
INSERT INTO `countries` (id,code,name,system_name) VALUES (240, 'WF', 'Wallis and Futuna Islands','wf');
INSERT INTO `countries` (id,code,name,system_name) VALUES (241, 'EH', 'Western Sahara','eh');
INSERT INTO `countries` (id,code,name,system_name) VALUES (242, 'YE', 'Yemen','ye');
INSERT INTO `countries` (id,code,name,system_name) VALUES (243, 'ZR', 'Zaire','zr');
INSERT INTO `countries` (id,code,name,system_name) VALUES (244, 'ZM', 'Zambia','zm');
INSERT INTO `countries` (id,code,name,system_name) VALUES (245, 'ZW', 'Zimbabwe','zw');

-- settings
create table settings(
    id int(11) unsigned not null auto_increment,
    is_active int(1) default 1,
    name varchar(255),
    system_name varchar(255),
    value varchar(255),
    primary key (id)    
) engine=InnoDB default charset=utf8;

