-- insert admin user
insert into users (id, username, firstname, lastname, password, email, is_deletable) values(1, 'admin', 'admin', 'admin','920bb9b5d9ce2c563f6d00afc3fd320bd417473d866194a98c2d6542df7845aa','admin@test.pl',0);
insert into roles_users (user_id, role_id) values(1,1);
insert into roles_users (user_id, role_id) values(1,2);

insert into users (id, username, firstname, lastname, password, email, is_deletable) values(2, 'test1', 'test1', 'test1','920bb9b5d9ce2c563f6d00afc3fd320bd417473d866194a98c2d6542df7845aa','test1@test.pl',1);
insert into roles_users (user_id, role_id) values(2,1);

insert into users (id, username, firstname, lastname, password, email, is_deletable) values(3, 'test2', 'test2', 'test2','920bb9b5d9ce2c563f6d00afc3fd320bd417473d866194a98c2d6542df7845aa','test2@test.pl',1);
insert into roles_users (user_id, role_id) values(3,1);

insert into users (id, username, firstname, lastname, password, email, is_deletable) values(4, 'test3', 'test3', 'test3','920bb9b5d9ce2c563f6d00afc3fd320bd417473d866194a98c2d6542df7845aa','test3@test.pl',1);
insert into roles_users (user_id, role_id) values(4,1);

